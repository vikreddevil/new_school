package com.snaptech.emissionew.Registration.Fragments;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.snaptech.emissionew.R;
import com.snaptech.emissionew.Utils.Singleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import static com.snaptech.emissionew.Utils.Constants.BASE_URL;

/**
 * Created by vikas on 15/02/17.
 */

public class SignupFragment extends Fragment {


    private Singleton singleton;
    private Activity mActivity;
    private EditText et_password,et_confirm_password;
    private EditText et_email_id;
    private EditText et_mobile;
    private EditText et_fname,et_lname,et_mname;
    private Button btn_sign_up;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View view;

        view=inflater.inflate(R.layout.fragment_signup_screen,null);

        getRef(view);

        singleton=Singleton.getInstance();

        btn_sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                callSignupApi();

            }
        });



        return view;
    }

    private void getRef(View view){


        et_password=(EditText)view.findViewById(R.id.et_reg_password);
        et_confirm_password=(EditText)view.findViewById(R.id.et_reg_confirm_password);
        et_email_id=(EditText)view.findViewById(R.id.et_reg_email);
        et_mobile=(EditText)view.findViewById(R.id.et_reg_email);
        et_fname=(EditText)view.findViewById(R.id.et_reg_fname);
        et_lname=(EditText)view.findViewById(R.id.et_reg_lname);
        et_mname=(EditText)view.findViewById(R.id.et_reg_mname);
        btn_sign_up=(Button)view.findViewById(R.id.btn_signup);



    }
    private void callSignupApi()
    {

        System.out.println("Api called");
        final ProgressDialog pDialog = new ProgressDialog(mActivity);
        pDialog.setMessage("Loading...");
        pDialog.show();

        Map<String, String> params= new HashMap<>();

        params.put("first_name", et_fname.getText().toString());
        params.put("middle_name", et_mname.getText().toString());
        params.put("last_name", et_lname.getText().toString());
        params.put("email_id", et_email_id.getText().toString());
        params.put("mobile", et_mobile.getText().toString());
        params.put("password",et_password.getText().toString());



        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                BASE_URL+"user/register", new JSONObject(params),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        //Log.d("Response on color", response.toString());
                        pDialog.dismiss();

                    }
                }, new Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error", "Error: " + error.getMessage());
                String message="";
                System.out.println("Error on reg "+error.getMessage());
                if (error instanceof NetworkError) {
                    message =mActivity.getString(R.string.network_error_message);
                }
//                else if (error instanceof ServerError) {
//                    message = "The server could not be found. Please try again after some time!!";
//                }


                pDialog.dismiss();
                if(!message.trim().equals(""))
                    Toast.makeText(mActivity,message,Toast.LENGTH_SHORT).show();
            }

        }) {

            /**
             * Passing some request headers
             * */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Client-Id", "1");
                //Client-Id
                return headers;
            }


            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {


                if(volleyError!=null)
                {
                    if(volleyError.networkResponse.statusCode==400)

                        try {
                            String jsonString = new String(volleyError.networkResponse.data, "UTF-8");
                            System.out.println(jsonString);
                            JSONObject jsonObject=new JSONObject(jsonString);
                            final String error=jsonObject.getString("error");


                            mActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if(error!=null)
                                        Toast.makeText(mActivity,error,Toast.LENGTH_SHORT).show();
                                }
                            });

                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                }


                return super.parseNetworkError(volleyError);
            }

            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {

                if(response!=null) {
                    try {
                        String jsonString = new String(response.data, "UTF-8");
                        System.out.println("status code is " + response.statusCode+" and response is "+jsonString);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }



                }
                return super.parseNetworkResponse(response);


            }
        };


        // Adding request to request queue


        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        jsonObjReq.setShouldCache(false);
        singleton.addToRequestQueue(jsonObjReq, "signup api calling");

    }
    @TargetApi(23)
    @Override public void onAttach(Context context) {
        //This method avoid to call super.onAttach(context) if I'm not using api 23 or more
        //if (Build.VERSION.SDK_INT >= 23) {
        super.onAttach(context);
        onAttachToContext(context);
        //}
    }

    /*
     * Deprecated on API 23
     * Use onAttachToContext instead
     */
    @SuppressWarnings("deprecation")
    @Override public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (Build.VERSION.SDK_INT < 23) {
            onAttachToContext(activity);
        }
    }
    protected void onAttachToContext(Context context){

        mActivity=(Activity)context;
    }

    private boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }
    private boolean isValidMobile(String phone)
    {
        if(phone==null){
            return false;
        }
        else
        {
            try {
                long phone_long = Long.parseLong(phone);
                if(phone.length()!=10)
                    return false;
            }catch (Exception e){

                return false;
            }
            return true;
        }
    }
}
