package com.snaptech.emissionew.Registration;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.snaptech.emissionew.R;
import com.snaptech.emissionew.Registration.Fragments.LoginFragment;
import com.snaptech.emissionew.Registration.Fragments.SignupFragment;
import com.snaptech.emissionew.Utils.Constants;
import com.snaptech.emissionew.Utils.Singleton;


public class MainActivity extends AppCompatActivity{

   private MyAdapter pagerAdapter;
    // private T tabLayout;
    private TabLayout tabLayout;
    private Singleton singleton;
    private LinearLayout header;
    private ViewPager viewPager;
    private String color_primary="#31498F";
    private String color_secondary="#31498F";
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Remove title bar
       // this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

        getDataFromSharedPreference();


        setStatusBarColor(color_secondary);
        singleton=Singleton.getInstance();

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        viewPager = (ViewPager) findViewById(R.id.viewpager);

        tabLayout.setBackgroundColor(Color.parseColor(color_primary));
        pagerAdapter = new MyAdapter(getSupportFragmentManager());

        viewPager.setAdapter(pagerAdapter);

        tabLayout.post(new Runnable() {
            @Override
            public void run() {
                tabLayout.setupWithViewPager(viewPager);
            }
        });
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int state) {
            }

            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            public void onPageSelected(int position) {
                if (position==1) {
                    //    calendarListFragment.populateDummyData();
                    //viewPager.invalidate();
                    //pagerAdapter.notifyDataSetChanged();
                }
//                Fragment fragment= pagerAdapter.getItem(position);
//
//                if(fragment instanceof CalendarListFragment ){
//                    ((CalendarListFragment)fragment).populateDummyData();
//                }
                // Check if this is the page you want.
            }
        });

        //callSideBarApi();

    }

    class MyAdapter extends FragmentPagerAdapter {

        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        /**
         * Return fragment with respect to Position .
         */

        @Override
        public Fragment getItem(int position)
        {
            switch (position){
                case 0 : return new LoginFragment();
                case 1 : return new SignupFragment();
            }
            return null;
        }

        @Override
        public int getCount() {

            return 2;

        }

        /**
         * This method returns the title of the tab according to the position.
         */

        @Override
        public CharSequence getPageTitle(int position) {

            switch (position){
                case 0 :
                    return "Login";
                case 1 :
                    return "Signup";

            }
            return null;
        }
    }


    private void getDataFromSharedPreference(){
        sharedPreferences=this.getSharedPreferences(Constants.LOGIN_PREFS_NAME,MODE_PRIVATE);
        color_primary=sharedPreferences.getString(Constants.PRIMARY_COLOR_PREFS_KEY,"#31498F");
        color_secondary=sharedPreferences.getString(Constants.SECONDARY_COLOR_PREFS_KEY,"#31498F");

    }
    private void setStatusBarColor(String color_secondary){

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.parseColor(color_secondary));
        }
    }

}



//api key: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoxMiwiaWF0IjoxNDg2NjI3MDU1LCJleHAiOjE0OTE4MTEwNTV9.xd8kX4gZP__RzVPvL3q57KbOHLwYOYFkphcBT8JWn6w