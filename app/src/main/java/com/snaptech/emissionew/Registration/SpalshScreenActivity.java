package com.snaptech.emissionew.Registration;

import android.app.Activity;


import android.content.Context;
import android.content.Intent;

import android.content.SharedPreferences;
import android.os.Bundle;

import android.os.Handler;

import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import com.snaptech.emissionew.AfterLoginModules.HomeScreenActivity;
import com.snaptech.emissionew.R;

import com.snaptech.emissionew.Utils.Constants;
import com.snaptech.emissionew.Utils.Singleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import static com.snaptech.emissionew.Utils.Constants.BASE_URL;
import static com.snaptech.emissionew.Utils.Constants.COLOR_VERSION_PREFS_KEY;
import static com.snaptech.emissionew.Utils.Constants.PRIMARY_COLOR_PREFS_KEY;
import static com.snaptech.emissionew.Utils.Constants.SECONDARY_COLOR_PREFS_KEY;

public class SpalshScreenActivity extends Activity {

    private static int SPLASH_TIME_OUT = 2500;
    private SharedPreferences prefs;
    private String api_token;
    private Intent loginIntent;
    private String current_color_version;
    private String current_side_bar_version;
    private Singleton singleton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spalsh_screen);
        singleton=Singleton.getInstance();
        prefs = SpalshScreenActivity.this.getSharedPreferences(Constants.LOGIN_PREFS_NAME, Context.MODE_PRIVATE);
        api_token=prefs.getString(Constants.ACCESS_TOKEN_PREFS_KEY,null);

        checkIfLoggedIn();
        callApi();

    }
    private void checkIfLoggedIn(){

//
////        if (accessToken != null){
////            loginIntent = new Intent(SplashActivity.this, BaseActivity.class);
////            startActivity(loginIntent);
////            finish();
//
//            //checkUserStatus();
//
//            //}
////        }else {
//
//
////            loginIntent = new Intent(this, MainActivity.class);
//

//
//        }
    }
    private void callApi()
    {

        System.out.println("Api called");
//        final ProgressDialog pDialog = new ProgressDialog(context);
//        pDialog.setMessage("Loading...");
//        pDialog.show();

        Map<String, String> params= new HashMap<>();

//                params.put("email_id", "vikas@snaptech.in");
//                params.put("mobile", "8108320362");
//                params.put("password", "1234");
//                params.put("meal_type", "veg");
//                params.put("passport_no", "abcd123");
//                params.put("passport_issue", "2010-10-05");
//                params.put("passport_expiry", "2020-10-05");
//                params.put("place_of_issue", "kalyan");



        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                BASE_URL+"user/color", new JSONObject(params),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        //Log.d("Response on color", response.toString());
                        //pDialog.hide();

                    }
                }, new Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error", "Error: " + error.getMessage());
                String message="Cannot connect to Internet...Please check your connection!";
                System.out.println("Error is color "+error.getMessage());
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                }
                Toast.makeText(SpalshScreenActivity.this,message,Toast.LENGTH_SHORT).show();

                //pDialog.hide();
            }

        }) {

            /**
             * Passing some request headers
             * */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Client-Id", "1");
                //Client-Id
                return headers;
            }


            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {


                return super.parseNetworkError(volleyError);
            }

            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {

                if(response!=null) {
                    try {
                        String jsonString = new String(response.data, "UTF-8");
                        System.out.println("status code is " + response.statusCode+" and response is "+jsonString);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }


                    if (response.statusCode == 200) {
                        prefs = SpalshScreenActivity.this.getSharedPreferences(Constants.LOGIN_PREFS_NAME, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = prefs.edit();

                        String color_primary="";
                        String side_bar_version="";
                        String color_secondary="";
                        String version="";
                        current_color_version=prefs.getString(COLOR_VERSION_PREFS_KEY,"-1");
                        current_side_bar_version=prefs.getString(Constants.SIDE_BAR_VERSION_PREFS_KEY,"-1");
                        try {
                            String jsonString = new String(response.data, "UTF-8");
                            JSONObject jsonObject=new JSONObject(jsonString);
                            JSONArray jsonArray=jsonObject.getJSONArray("data");

                            if(jsonArray.length()!=0) {
                                JSONObject dataObject = jsonArray.getJSONObject(0);
                                color_primary = dataObject.getString("android_primary_color");
                                color_secondary=dataObject.getString("android_secondary_color");
                                side_bar_version=dataObject.getInt("sidebar_version")+"";
                                version=dataObject.getInt("version")+"";

                                if(!side_bar_version.trim().equalsIgnoreCase(current_side_bar_version.trim())){

                                    Constants.flag_side_bar_updated=true;
                                }
                                else{

                                    Constants.flag_side_bar_updated=false;
                                }
                                if(!version.trim().equalsIgnoreCase(current_color_version.trim())) {

                                    editor.putString(PRIMARY_COLOR_PREFS_KEY, color_primary);
                                    editor.putString(SECONDARY_COLOR_PREFS_KEY, color_secondary);
                                    editor.putString(COLOR_VERSION_PREFS_KEY, version);
                                    editor.putString(Constants.SIDE_BAR_VERSION_PREFS_KEY,side_bar_version);

                                    editor.commit();
                                }
                            }

                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        SpalshScreenActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                new Handler().postDelayed(new Runnable() {

                                    @Override
                                    public void run() {

                                        if(api_token==null) {
                                            loginIntent = new Intent(SpalshScreenActivity.this, MainActivity.class);
                                            startActivity(loginIntent);
                                        }
                                        else{

                                            loginIntent = new Intent(SpalshScreenActivity.this, HomeScreenActivity.class);
                                            startActivity(loginIntent);
                                        }
                                        finish();
                                    }
                                }, SPLASH_TIME_OUT);
                            }
                        });


                    }
                }
                return super.parseNetworkResponse(response);


            }
        };


        // Adding request to request queue


        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        jsonObjReq.setShouldCache(false);
        singleton.addToRequestQueue(jsonObjReq, "new color theme api calling");

    }
}
