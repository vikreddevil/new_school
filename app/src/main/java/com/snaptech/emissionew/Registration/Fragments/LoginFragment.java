package com.snaptech.emissionew.Registration.Fragments;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;

import com.snaptech.emissionew.AfterLoginModules.HomeScreenActivity;
import com.snaptech.emissionew.R;
import com.snaptech.emissionew.Registration.Pojos.LoginDataResponsePojo;
import com.snaptech.emissionew.Registration.Pojos.LoginPojo;
import com.snaptech.emissionew.Utils.Constants;
import com.snaptech.emissionew.Utils.Singleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import static com.android.volley.VolleyLog.TAG;
import static com.snaptech.emissionew.Utils.Constants.BASE_URL;

/**
 * Created by vikas on 15/02/17.
 */


//device UUID make it dynamic

    //reminder
public class LoginFragment extends Fragment {


    private Button btn_login;
    private Singleton singleton;
    private Activity mActivity;
    private SharedPreferences sharedPreferences;
    private TextView tv_forgot_password;
    private String user_id="";
    private EditText et_login_email_or_number;
    private EditText et_password;
    private String password="";
    private SharedPreferences.Editor editor;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view;
        view=inflater.inflate(R.layout.fragment_login_screen,null);

        singleton=Singleton.getInstance();
        et_login_email_or_number=(EditText)view.findViewById(R.id.et_login_email_or_number);
        et_password=(EditText)view.findViewById(R.id.et_login_password);

        btn_login=(Button)view.findViewById(R.id.btn_login);
        sharedPreferences=mActivity.getSharedPreferences(Constants.LOGIN_PREFS_NAME,Context.MODE_PRIVATE);
        tv_forgot_password=(TextView)view.findViewById(R.id.tv_forgot_password);
        String color_primary=sharedPreferences.getString(Constants.PRIMARY_COLOR_PREFS_KEY,"#31498F");
        tv_forgot_password.setTextColor(Color.parseColor(color_primary));

        editor=sharedPreferences.edit();


        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                callLoginApi();

                //Intent intent=new Intent(mActivity, HomeScreenActivity.class);
                //startActivity(intent);

            }
        });
        return view;
    }

    @TargetApi(23)
    @Override public void onAttach(Context context) {
        //This method avoid to call super.onAttach(context) if I'm not using api 23 or more
        //if (Build.VERSION.SDK_INT >= 23) {
        super.onAttach(context);
        onAttachToContext(context);
        //}
    }

    /*
     * Deprecated on API 23
     * Use onAttachToContext instead
     */
    @SuppressWarnings("deprecation")
    @Override public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (Build.VERSION.SDK_INT < 23) {
            onAttachToContext(activity);
        }
    }
    protected void onAttachToContext(Context context){

        mActivity=(Activity)context;
    }
    private boolean checkReadPhoneState()
    {

        if (Build.VERSION.SDK_INT >= 23) {
            if (mActivity.checkSelfPermission(Manifest.permission.READ_PHONE_STATE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG,"Permission is granted");
                return true;
            } else {

                Log.v(TAG,"Permission is revoked");
                ActivityCompat.requestPermissions(mActivity, new String[]{Manifest.permission.READ_PHONE_STATE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG,"Permission is granted");
            return true;
        }
    }
    private boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }
    private boolean isValidMobile(String phone)
    {
        if(phone==null){
            return false;
        }
        else
        {
            try {
                long phone_long = Long.parseLong(phone);
                if(phone.length()!=10)
                    return false;
            }catch (Exception e){

                return false;
            }
            return true;
        }
    }
    private void callLoginApi()
    {

        System.out.println("Api called");
        final ProgressDialog pDialog = new ProgressDialog(mActivity);
        pDialog.setMessage("Loading...");
        pDialog.show();

        Map<String, String> params= new HashMap<>();

                params.put("email_id", et_login_email_or_number.getText().toString());
                params.put("password", et_password.getText().toString());
                params.put("device_type", "android");
                params.put("device_uuid", "2010-10-05");
                params.put("device_token", "2020-10-05");



        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                BASE_URL+"user/login", new JSONObject(params),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Response on login", response.toString());
                        pDialog.dismiss();

                    }
                }, new Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error", "Error: " + error.getMessage());
                String message="";
                System.out.println("Error is login "+error.getMessage());
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                }
//                else if (error instanceof ServerError) {
//                    message = "The server could not be found. Please try again after some time!!";
//                }


                pDialog.dismiss();
                if(!message.trim().equals(""))
                Toast.makeText(mActivity,message,Toast.LENGTH_SHORT).show();
            }

        }) {

            /**
             * Passing some request headers
             * */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Client-Id", "1");
                //Client-Id
                return headers;
            }


            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {


                if(volleyError!=null)
                {
                    if(volleyError.networkResponse.statusCode==400)

                        try {
                            String jsonString = new String(volleyError.networkResponse.data, "UTF-8");
                            System.out.println(jsonString);
                            JSONObject jsonObject=new JSONObject(jsonString);
                            final String error=jsonObject.getString("error");


                            mActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if(error!=null)
                                    Toast.makeText(mActivity,error,Toast.LENGTH_SHORT).show();
                                }
                            });

                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                }


                return super.parseNetworkError(volleyError);
            }

            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {

                if(response!=null) {
                    try {
                        String jsonString = new String(response.data, "UTF-8");
                        System.out.println("status code is " + response.statusCode+" and response is "+jsonString);

                        Gson gson=new Gson();

                        final LoginPojo loginPojo=gson.fromJson(jsonString,LoginPojo.class);
                        if(loginPojo!=null) {
                            if (loginPojo.getLoginDataResponsePojo().size() != 0) {

                                LoginDataResponsePojo loginDataResponsePojo=loginPojo.getLoginDataResponsePojo().get(0);
                                editor.putString(Constants.ACCESS_TOKEN_PREFS_KEY, loginDataResponsePojo.getApi_token());
                                editor.putString(Constants.FIRST_NAME_PREFS_KEY,loginDataResponsePojo.getFirst_name());
                                editor.putString(Constants.LAST_NAME_PREFS_KEY,loginDataResponsePojo.getLast_name());
                                //editor.putString(Constants.MIDDLE_NAME_PREFS_KEY,loginDataResponsePojo.geM)
                                editor.putString(Constants.DOB_PREFS_KEY,loginDataResponsePojo.getDob());
                                editor.putString(Constants.MOBILE_PREFS_KEY,loginDataResponsePojo.getMobile());
                                editor.putString(Constants.EMAIL_ID_PREFS_KEY,loginDataResponsePojo.getEmail_id());
                                editor.putString(Constants.ROLE_ID,loginDataResponsePojo.getRole_id()+"");
                                editor.putString(Constants.GENDER_PREFS_KEY,loginDataResponsePojo.getGender());
                                editor.putString(Constants.USER_ID_PREFS_KEY,loginDataResponsePojo.getUser_id()+"");


                                editor.commit();

                                mActivity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {


                                        Intent intent=new Intent(mActivity, HomeScreenActivity.class);
                                        startActivity(intent);
                                    }
                                });
                            }
                        }




                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    catch (Exception e){

                        e.printStackTrace();
                    }



                }
                return super.parseNetworkResponse(response);


            }
        };


        // Adding request to request queue


        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        jsonObjReq.setShouldCache(false);
        singleton.addToRequestQueue(jsonObjReq, "login api calling");

    }
}

//{"data":[{"action":"link","display_name":"LINK","sort":1},{"action":"document","display_name":"DOCUMENT","sort":2},{"action":"album","display_name":"ALBUM","sort":3},{"action":"messages","display_name":"MESSAGES","sort":4},{"action":"video","display_name":"VIDEOS","sort":5},{"action":"calendar","display_name":"CALENDAR","sort":6},{"action":"setting","display_name":"SETTING","sort":7},{"action":"institute_info","display_name":"INSTITUTE INFO","sort":8},{"action":"sign_out","display_name":"SIGN OUT","sort":9},{"action":"home","display_name":"HOME","sort":0}]}



//eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoxLCJpYXQiOjE0ODcyNDAyMTUsImV4cCI6MTQ5MjQyNDIxNX0.U04XL6iGzmoN0KnNkctNxzTyGkCjFOFrra3PS1GzrhE