package com.snaptech.emissionew.Registration.Pojos;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by vikas on 16/02/17.
 */

public class LoginPojo {

    private String message;

    @SerializedName("data")
    private ArrayList<LoginDataResponsePojo> loginDataResponsePojo;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<LoginDataResponsePojo> getLoginDataResponsePojo() {
        return loginDataResponsePojo;
    }

    public void setLoginDataResponsePojo(ArrayList<LoginDataResponsePojo> loginDataResponsePojo) {
        this.loginDataResponsePojo = loginDataResponsePojo;
    }
}
