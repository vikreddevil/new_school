package com.snaptech.emissionew.AfterLoginModules.Pojos;

import java.util.ArrayList;

/**
 * Created by vikas on 27/03/17.
 */

public class AddEventsRequestPojo {

    private String title;
    private String event_type_id;
    private String desc;
    private String start_date;
    private String end_date;
    private String start_time;
    private String end_time;
    private String location;
    private ArrayList<String> group_add_id;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEvent_type_id() {
        return event_type_id;
    }

    public void setEvent_type_id(String event_type_id) {
        this.event_type_id = event_type_id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public ArrayList<String> getGroup_add_id() {
        return group_add_id;
    }

    public void setGroup_add_id(ArrayList<String> group_add_id) {
        this.group_add_id = group_add_id;
    }
}
