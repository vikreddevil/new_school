package com.snaptech.emissionew.AfterLoginModules.Adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.snaptech.emissionew.R;
import com.snaptech.emissionew.Utils.Constants;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import uk.co.senab.photoview.PhotoViewAttacher;


/**
 * Created by vikas on 02/11/16.
 */


// This adapter is for the FullScreenViewActivity which is used to display the Image Gallery

public class AlbumImagePagerAdapter extends PagerAdapter {

    private Context context;
    private PhotoViewAttacher mAttacher;
    private Callback imageLoadedCallback;
    private LayoutInflater layoutInflater;


    public AlbumImagePagerAdapter(Context context) {
        this.context = context;
        this.layoutInflater = (LayoutInflater) this.context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        System.out.println("Size is "+ Constants.photoGridDataResponsePojos.size());
        return Constants.photoGridDataResponsePojos.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }


    @Override
    public Object instantiateItem(ViewGroup container, int position) {


        final ImageView photoImageView;

        layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = layoutInflater.inflate(R.layout.view_pager_album, container,
                false);


        photoImageView = (ImageView) viewLayout.findViewById(R.id.album_pager_image);

        ProgressBar progressBar = null;
        if (viewLayout != null) {
            progressBar = (ProgressBar) viewLayout.findViewById(R.id.progressBar);
            progressBar.setVisibility(View.VISIBLE);
        }

        //loading the image in the Gallery

         imageLoadedCallback= new ImageLoadedCallback(progressBar) {

            @Override
            public void onSuccess() {
                if(mAttacher!=null){
                    mAttacher = new PhotoViewAttacher(photoImageView);
                    mAttacher.setZoomable(true);
                    if (this.progressBar != null) {
                        this.progressBar.setVisibility(View.GONE);
                    }

                }else{
                    mAttacher = new PhotoViewAttacher(photoImageView);
                    mAttacher.setZoomable(true);
                    if (this.progressBar != null) {
                        this.progressBar.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onError() {
                // TODO Auto-generated method stub

            }
        };

        Picasso.with(context)
                .load(Constants.photoGridDataResponsePojos.get(position).getLarge_image()).fit().centerInside()
                .into(photoImageView,imageLoadedCallback);

        ((ViewPager) container).addView(viewLayout);

        return viewLayout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((RelativeLayout) object);
    }
    private class ImageLoadedCallback implements Callback {
        ProgressBar progressBar;

        public  ImageLoadedCallback(ProgressBar progBar){
            progressBar = progBar;
        }

        @Override
        public void onSuccess() {

        }

        @Override
        public void onError() {

        }
    }
}
