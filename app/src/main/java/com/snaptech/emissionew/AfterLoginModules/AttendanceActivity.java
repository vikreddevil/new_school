package com.snaptech.emissionew.AfterLoginModules;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.snaptech.emissionew.R;

public class AttendanceActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance);

        Intent intent=new Intent(AttendanceActivity.this,AttendanceChoicePopupActivity.class);
        startActivity(intent);
    }
}
