package com.snaptech.emissionew.AfterLoginModules.Pojos;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by vikas on 09/03/17.
 */

public class UserDetailsPojo {



    @SerializedName("data")
    private ArrayList<UserDetailsDataResponsePojo> userDetailsDataResponsePojoArrayList;
    private String message;

    public ArrayList<UserDetailsDataResponsePojo> getUserDetailsDataResponsePojoArrayList() {
        return userDetailsDataResponsePojoArrayList;
    }

    public void setUserDetailsDataResponsePojoArrayList(ArrayList<UserDetailsDataResponsePojo> userDetailsDataResponsePojoArrayList) {
        this.userDetailsDataResponsePojoArrayList = userDetailsDataResponsePojoArrayList;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
