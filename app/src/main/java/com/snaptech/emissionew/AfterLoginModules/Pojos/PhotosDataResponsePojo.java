package com.snaptech.emissionew.AfterLoginModules.Pojos;

/**
 * Created by vikas on 14/11/16.
 */

public class PhotosDataResponsePojo {

    private int id;
    private String album_name;
    private String album_desription;
    private String album_image;

    private String created_at;
    private int total_image;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAlbum_name() {
        return album_name;
    }

    public void setAlbum_name(String album_name) {
        this.album_name = album_name;
    }

    public String getAlbum_desription() {
        return album_desription;
    }

    public void setAlbum_desription(String album_desription) {
        this.album_desription = album_desription;
    }

    public String getAlbum_image() {
        return album_image;
    }

    public void setAlbum_image(String album_image) {
        this.album_image = album_image;
    }



    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public int getTotal_image() {
        return total_image;
    }

    public void setTotal_image(int total_image) {
        this.total_image = total_image;
    }
}
