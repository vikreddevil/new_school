package com.snaptech.emissionew.AfterLoginModules;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.snaptech.emissionew.AfterLoginModules.Pojos.AllEventTypesDataResponsePojo;
import com.snaptech.emissionew.AfterLoginModules.Pojos.AllEventTypesPojo;

import com.snaptech.emissionew.R;
import com.snaptech.emissionew.Utils.Constants;
import com.snaptech.emissionew.Utils.Singleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import es.dmoral.toasty.Toasty;

import static com.snaptech.emissionew.Utils.Constants.BASE_URL;

public class EventTypePopupActivity extends AppCompatActivity {

    private Singleton singleton;
    private SharedPreferences prefs;
    private String api_token;
    private String role_id="";
    private ListView listView;
    private ArrayList<String> str_event_type_array;
    private ArrayList<String> str_event_type_id=new ArrayList<>();
    private ArrayList<String> str_event_type_code=new ArrayList<>();
    private Button btn_confirm;
    private Button btn_cancel;
    private String type_code;
    private String type_id;
    private String type_name;
    private HashMap<Integer,Integer> hashMap=new HashMap<>();
    ArrayAdapter<String> adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_type_popup);

        prefs= EventTypePopupActivity.this.getSharedPreferences(Constants.LOGIN_PREFS_NAME, MODE_PRIVATE);
        // tour_id=prefs.getString(TOUR_ID,null);
        api_token = prefs.getString(Constants.ACCESS_TOKEN_PREFS_KEY, null);
        //checkBox=(CheckBox)findViewById(R.id.chkAll);
        btn_cancel=(Button)findViewById(R.id.btn_cancel_popup);
        btn_confirm=(Button)findViewById(R.id.btn_confirm_popup);
        singleton=Singleton.getInstance();
        role_id=prefs.getString(Constants.ROLE_ID,null);
        listView=(ListView)findViewById(R.id.list);
        str_event_type_array=new ArrayList<>();
        adapter = new ArrayAdapter<>(this, R.layout.simple_list_item_single_choice, str_event_type_array);

        listView.setAdapter(adapter);
        listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

//        str_event_type_array.clear();
//        str_event_type_array.add("Public holiday");
//        str_event_type_array.add("School holiday");
       // adapter.notifyDataSetChanged();
        DisplayMetrics dm=new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width=dm.widthPixels;
        int height=dm.heightPixels;

        getWindow().setLayout((int)(width*.9), ((int)(height*.6)));
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
                Intent intent = new Intent();

                setResult(RESULT_CANCELED, intent);
            }
        });
        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               System.out.println("Item selected position "+listView.getCheckedItemPosition()+"");
                Intent intent = new Intent();
                //Bundle bundle=new Bundle();

                int pos=listView.getCheckedItemPosition();

                for (int i=0;i<listView.getCount();i++){

                    if(i==pos){

                        type_name=str_event_type_array.get(i);
                        type_code=str_event_type_code.get(i);
                        type_id=str_event_type_id.get(i);
                    }
                }

                intent.putExtra("type_name",type_name);
                intent.putExtra("type_code",type_code);
                intent.putExtra("type_id",type_id);

//                type_name=bundle.getString("type_name");
//                type_code=bundle.getString("type_code");
//                type_id=bundle.getString("type_id");

                setResult(RESULT_OK, intent);
                finish();
            }
        });
        callEventTypeListsApi(EventTypePopupActivity.this);
    }
    private void callEventTypeListsApi(final Context context)
    {


        Map<String, String> params= new HashMap<String, String>();

        params.put("role_id",role_id);


//        ArrayList<String> arrayList=new ArrayList<String>();
//        arrayList.add(0,"5");

        Gson gson=new Gson();
//        JSONObject jsonObject=null;
//        try {
//            jsonObject=new JSONObject(gson.toJson(addLinksRequestPojo));
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                BASE_URL+"api/event/types", new JSONObject(params),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        if(response!=null)
                            Log.d("Response", response.toString());


                        //Toast.makeText(mActivity,"Login Successful",Toast.LENGTH_SHORT).show();

                    }
                }, new Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {

                //VolleyLog.d("Error", "Error: " + error.getMessage());
                String message="";
                if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }


                if (!message.equalsIgnoreCase(""))
                    Toasty.error(context, message, Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {

                if(response!=null) {
                    System.out.println("Response code from parse Network response is " + response.statusCode + " and data is " + response.toString() + " and data is " + response.data.toString());
                    try {
                        String jsonString = new String(response.data, "UTF-8");
                        System.out.println("Response from parse Network response is " + jsonString);

                        //Gson gson = new Gson();
                        //final LinksPojo linksPojo = gson.fromJson(jsonString, LinksPojo.class);

                        if (response.statusCode == 200||response.statusCode==304) {

                            //Toast.makeText(mActivity,"Login Successful",Toast.LENGTH_SHORT).show();



                            Gson gson1=new Gson();
                            final AllEventTypesPojo allEventTypesPojo=gson1.fromJson(jsonString,AllEventTypesPojo.class);


                            ((EventTypePopupActivity) context).runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    //System.out.println("Number of groups "+groupsPojo.getGroupsDataResponsePojoArrayList().size());
                                    if(allEventTypesPojo!=null) {
                                        //groupsDataResponsePojoArrayList.clear();
                                        AllEventTypesDataResponsePojo allEventTypesDataResponsePojo=new AllEventTypesDataResponsePojo();
//                                        groupsDataResponsePojo.setGroup_id(0);
//                                        groupsDataResponsePojo.setGroup_name("All Groups");
                                        //groupsDataResponsePojoArrayList.add(0,groupsDataResponsePojo);
//                                        flag_check_list.clear();
//                                        str_group_id_array.clear();
                                        hashMap.clear();
                                        str_event_type_array.clear();
                                        str_event_type_code.clear();
                                        str_event_type_id.clear();
                                        if (allEventTypesPojo.getAllEventTypesDataResponsePojoArrayList().size() != 0) {
                                            for (int i = 0; i < allEventTypesPojo.getAllEventTypesDataResponsePojoArrayList().size(); i++) {
                                                // groupsDataResponsePojoArrayList.add(i, groupsPojo.getGroupsDataResponsePojoArrayList().get(i));
                                                //flag_check_list.add(i,false);
                                                str_event_type_array.add(i,allEventTypesPojo.getAllEventTypesDataResponsePojoArrayList().get(i).getEvent_type_name());
                                                str_event_type_code.add(i,allEventTypesPojo.getAllEventTypesDataResponsePojoArrayList().get(i).getEvent_code());
                                                str_event_type_id.add(i,allEventTypesPojo.getAllEventTypesDataResponsePojoArrayList().get(i).getEvent_type_id()+"");

                                                hashMap.put(i,allEventTypesPojo.getAllEventTypesDataResponsePojoArrayList().get(i).getEvent_type_id());

                                            }
                                            adapter.notifyDataSetChanged();
                                            int itemCount = listView.getCount();
                                            for(int i=0 ; i < itemCount ; i++){
                                                listView.setItemChecked(i, false);
                                            }



                                            //adapter.notifyDataSetChanged();
                                            //groupPopUpAdapter.notifyDataSetChanged();
                                        }
                                    }
//                                    Toast.makeText(Gr.this,"Link added Successfully",Toast.LENGTH_SHORT).show();

                                }
                            });



                        }
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
                return super.parseNetworkResponse(response);
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {

                NetworkResponse networkResponse=volleyError.networkResponse;
                if(networkResponse!=null){

                    String jsonString="";
                    try {
                        jsonString = new String(networkResponse.data, "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                    System.out.println("Network response is "+networkResponse.statusCode+jsonString);
                    try {
                        JSONObject jsonObject=new JSONObject(jsonString);
                        JSONArray jsonArray=jsonObject.getJSONArray("error");
                        final String errorString=jsonArray.getString(0);
                        ((HomeScreenActivity)context).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toasty.error(context,errorString,Toast.LENGTH_SHORT).show();
                            }
                        });


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
                return super.parseNetworkError(volleyError);
            }

            /**
             * Passing some request headers
             * */

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Authorization",api_token);
                headers.put("Client-Id","1");
                return headers;
            }

        };

        singleton.addToRequestQueue(jsonObjReq, "Event Type List api calling");

    }
}
