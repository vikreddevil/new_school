package com.snaptech.emissionew.AfterLoginModules.Pojos;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by vikas on 28/10/16.
 */

public class NotificationPojo {


    @SerializedName("data")
    private ArrayList<NotificationResponsePojo> notificationResponsePojoArrayList;

    public ArrayList<NotificationResponsePojo> getNotificationResponsePojoArrayList() {
        return notificationResponsePojoArrayList;
    }

    public void setNotificationResponsePojoArrayList(ArrayList<NotificationResponsePojo> notificationResponsePojoArrayList) {
        this.notificationResponsePojoArrayList = notificationResponsePojoArrayList;
    }
}
