package com.snaptech.emissionew.AfterLoginModules.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.snaptech.emissionew.AfterLoginModules.LinksWebViewer;
import com.snaptech.emissionew.AfterLoginModules.Pojos.LinksDataResponsePojo;
import com.snaptech.emissionew.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by vikas on 30/10/16.
 */

//this adapter is to display the links list

public class LinksAdapter extends RecyclerView.Adapter<LinksAdapter.CustomViewHolder>{

    private List<LinksDataResponsePojo> linksPojoList;
    private Context mContext;

    public LinksAdapter(Context context, List<LinksDataResponsePojo> notificationPojoList) {
        this.linksPojoList = notificationPojoList;
        this.mContext = context;
    }

    @Override
    public LinksAdapter.CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.links_row, viewGroup,false);
        LinksAdapter.CustomViewHolder viewHolder = new LinksAdapter.CustomViewHolder(view);
        viewHolder.cardView.setPreventCornerOverlap(false);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(LinksAdapter.CustomViewHolder customViewHolder, final int i) {

        DateFormat fromFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        fromFormat.setLenient(false);
        DateFormat toFormat = new SimpleDateFormat("dd-MM-yyyy");
        toFormat.setLenient(false);
        String dateStr = linksPojoList.get(i).getCreated_at();
        Date date = null;
        try {
            date = fromFormat.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        customViewHolder.tv_date.setText(toFormat.format(date));
        DateFormat fromFormat2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        fromFormat2.setLenient(false);
        DateFormat toFormat2 = new SimpleDateFormat("hh:mm aa");
        toFormat2.setLenient(false);
        Date date2 = null;
        try {
            date2 = fromFormat2.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        customViewHolder.tv_time.setText(toFormat2.format(date2));
        final LinksDataResponsePojo linksDataResponsePojo=linksPojoList.get(i);
        customViewHolder.tv_title.setText(linksDataResponsePojo.getLink_name());

        //opening web view on click of the links

        customViewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(mContext, LinksWebViewer.class);
                String inputUrl = linksPojoList.get(i).getLink_url();

                //adding http if it is not in the link

                if (!inputUrl.contains("http://")&&!inputUrl.contains("https://"))
                    inputUrl = "http://" + inputUrl;


                intent.putExtra("url",inputUrl);
                intent.putExtra("urltitle",linksPojoList.get(i).getLink_name());
                mContext.startActivity(intent);
            }
        });
        SpannableString content = new SpannableString(linksDataResponsePojo.getLink_url());
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        customViewHolder.tv_url.setText(linksDataResponsePojo.getLink_desc());
    }

    @Override
    public int getItemCount() {
        return (linksPojoList.size());
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {
        protected TextView tv_title;
        protected TextView tv_url;
        protected TextView tv_date;
        protected TextView tv_time;
        private CardView cardView;

        public CustomViewHolder(View view) {
            super(view);

            this.tv_title=(TextView)view.findViewById(R.id.tv_title);
            this.tv_url=(TextView)view.findViewById(R.id.tv_url);
            this.cardView=(CardView)view.findViewById(R.id.cardview_notification_row);
            this.tv_date=(TextView)view.findViewById(R.id.tv_date_links);
            this.tv_time=(TextView)view.findViewById(R.id.tv_time_links);

        }
    }

}
