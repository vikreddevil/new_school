package com.snaptech.emissionew.AfterLoginModules.Pojos;

/**
 * Created by vikas on 03/11/16.
 */

public class LinksDataResponsePojo {

    //{"data":[{"id":2,"link_name":"eco","link_desc":"eco desc","link_url":"http://economictimes.indiatimes.com/news/science/28-hour-countdown-for-isros-record-satellite-launch-begins/articleshow/57144177.cms","created_at":"2017-02-16 10:45:47"}]}
    private int id;
    private String link_name;
    private String link_desc;
    private String link_url;
    private String created_at;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLink_name() {
        return link_name;
    }

    public void setLink_name(String link_name) {
        this.link_name = link_name;
    }


    public String getLink_desc() {
        return link_desc;
    }

    public void setLink_desc(String link_desc) {
        this.link_desc = link_desc;
    }

    public String getLink_url() {
        return link_url;
    }

    public void setLink_url(String link_url) {
        this.link_url = link_url;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }
}
