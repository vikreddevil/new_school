package com.snaptech.emissionew.AfterLoginModules.Pojos;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by vikas on 24/03/17.
 */

public class CalendarMonthwisePojo {

    @SerializedName("data")
    private ArrayList<CalendarMonthwiseDataResponsePojo> calendarMonthwiseDataResponsePojoArrayList;

    public ArrayList<CalendarMonthwiseDataResponsePojo> getCalendarMonthwiseDataResponsePojoArrayList() {
        return calendarMonthwiseDataResponsePojoArrayList;
    }

    public void setCalendarMonthwiseDataResponsePojoArrayList(ArrayList<CalendarMonthwiseDataResponsePojo> calendarMonthwiseDataResponsePojoArrayList) {
        this.calendarMonthwiseDataResponsePojoArrayList = calendarMonthwiseDataResponsePojoArrayList;
    }
}
