package com.snaptech.emissionew.AfterLoginModules.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;


import com.snaptech.emissionew.AfterLoginModules.Pojos.GroupsDataResponsePojo;
import com.snaptech.emissionew.R;

import java.util.ArrayList;

/**
 * Created by vikas on 15/03/17.
 */

public class GroupPopUpAdapter extends RecyclerView.Adapter<GroupPopUpAdapter.CustomViewHolder> {

    private Context context;
    public static ArrayList<GroupsDataResponsePojo> groupsDataResponsePojoArrayList=new ArrayList<>();

    private int check_all;


    public GroupPopUpAdapter(Context context,ArrayList<GroupsDataResponsePojo> groupsDataResponsePojoArrayList,ArrayList<Boolean> flag_checkbox){



        this.groupsDataResponsePojoArrayList=groupsDataResponsePojoArrayList;
        this.context=context;
    }
    @Override
    public GroupPopUpAdapter.CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.popup_row, parent,false);
        GroupPopUpAdapter.CustomViewHolder viewHolder = new GroupPopUpAdapter.CustomViewHolder(view);
        //viewHolder.cardView.setPreventCornerOverlap(false);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder holder, final int position) {



        holder.tv_group_popup.setText(groupsDataResponsePojoArrayList.get(position).getGroup_name());


        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {


//                if(isChecked){
//                    flag_checkbox.set(position,true);
//                }
//                else
//                    flag_checkbox.set(position,false);

            }
        });

    }


    @Override
    public int getItemCount() {
        return groupsDataResponsePojoArrayList.size() ;
    }
    class CustomViewHolder extends RecyclerView.ViewHolder {

        private CheckBox checkBox;
        private TextView tv_group_popup;

        public CustomViewHolder(View view) {
            super(view);

            checkBox= (CheckBox) view.findViewById(R.id.checkbox);
            tv_group_popup=(TextView)view.findViewById(R.id.tv_group_popup);


        }
    }


}
