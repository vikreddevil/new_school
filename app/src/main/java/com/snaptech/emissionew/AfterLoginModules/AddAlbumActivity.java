package com.snaptech.emissionew.AfterLoginModules;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.snaptech.emissionew.R;

import java.util.ArrayList;

public class AddAlbumActivity extends AppCompatActivity {


    private EditText et_title;
    private EditText et_images;
    private EditText et_groups;
    private Button btn_add_album;
    private ArrayList<String> group_id_array;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_album);

        getRef();

        et_groups.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;


                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (et_groups.getRight() - et_groups.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here

                        et_groups.setText("");
                        et_groups.requestFocus();
                        return true;
                    }
                }
                return false;
            }
        });

        et_images.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;


                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (et_images.getRight() - et_images.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here

                        et_images.setText("");
                        et_images.requestFocus();
                        return true;
                    }
                }
                return false;
            }
        });

        et_title.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;


                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (et_title.getRight() - et_title.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here

                        et_title.setText("");
                        et_title.requestFocus();
                        return true;
                    }
                }
                return false;
            }
        });

        et_groups.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(AddAlbumActivity.this,GroupsPopupActivity.class);
                startActivityForResult(intent,1);
            }
        });

        et_images.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        btn_add_album.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }
    private void getRef()
    {
        et_title=(EditText)findViewById(R.id.et_title_add_album);
        et_groups=(EditText)findViewById(R.id.et_groups_add_album);
        et_images=(EditText)findViewById(R.id.et_images_add_album);
        btn_add_album=(Button)findViewById(R.id.btn_add_album);
    }
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 3) {
            if(resultCode == RESULT_OK){
                Bundle bundle=data.getExtras();
                group_id_array=bundle.getStringArrayList("group_ids");
                // group_name_array=bundle.getStringArrayList("group_names");

                if(group_id_array!=null) {
                    String groups = group_id_array.size() + " groups selected";
//                for (int i=0;i<group_name_array.size();i++){
//
//                    if(i!=group_name_array.size()-1)
//                    groups=groups+group_name_array.get(i)+" ,";
//                    else
//                        groups=groups+group_name_array.get(i);
//                }
                    et_groups.setText(groups);
                }
            }
        }
    }
}
