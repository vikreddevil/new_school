package com.snaptech.emissionew.AfterLoginModules.Fragments;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.snaptech.emissionew.AfterLoginModules.Adapters.VideosAdapter;
import com.snaptech.emissionew.AfterLoginModules.HomeScreenActivity;
import com.snaptech.emissionew.AfterLoginModules.Pojos.VideosPojo;
import com.snaptech.emissionew.AfterLoginModules.Pojos.VideosResponsePojo;
import com.snaptech.emissionew.R;
import com.snaptech.emissionew.Utils.Constants;
import com.snaptech.emissionew.Utils.Singleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import es.dmoral.toasty.Toasty;

import static android.content.Context.MODE_PRIVATE;
import static com.snaptech.emissionew.Utils.Constants.BASE_URL;


/**
 * Created by vikas on 01/11/16.
 */

public class VideosFragment extends Fragment {

    Activity mActivity;
    private SharedPreferences prefs;
    private String api_token;
    private Singleton singleton;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView mRecyclerView;
    private VideosAdapter videosAdapter;
    private ArrayList<VideosResponsePojo> videosResponsePojoArrayList;
    //private String tour_id="1";
    private TextView tv_no_events;
    private ImageView iv_add;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view;
        view=inflater.inflate(R.layout.fragment_notification,container,false);
        getRef(view);
        Toolbar toolbar = (Toolbar) mActivity.findViewById(R.id.toolbar);
        iv_add= (ImageView) toolbar.findViewById(R.id.iv_add);
        iv_add.setVisibility(View.VISIBLE);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
            callVideosApi(mActivity);
            }
        });
        callVideosApi(mActivity);
        return view;
    }
    private void getRef(View view){
        singleton=Singleton.getInstance();
        tv_no_events=(TextView)view.findViewById(R.id.tv_comingsoon);
        swipeRefreshLayout=(SwipeRefreshLayout)view.findViewById(R.id.swiperefresh_links);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        videosResponsePojoArrayList=new ArrayList<>();
        videosAdapter=new VideosAdapter(mActivity,videosResponsePojoArrayList);
        mRecyclerView.setAdapter(videosAdapter);
        prefs= mActivity.getSharedPreferences(Constants.LOGIN_PREFS_NAME, MODE_PRIVATE);
        api_token = prefs.getString(Constants.ACCESS_TOKEN_PREFS_KEY, null);
        //tour_id=prefs.getString(TOUR_ID,null);

    }

    private void callVideosApi(final Context context)
    {
       swipeRefreshLayout.setRefreshing(true);
        Map<String, String> params= new HashMap<String, String>();
        params.put("limit", "10");
        params.put("offset","0");

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                BASE_URL+"api/videos", new JSONObject(params),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        if(response!=null)
                        Log.d("Response", response.toString());


                        //Toast.makeText(mActivity,"Login Successful",Toast.LENGTH_SHORT).show();
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }, new Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {

                mRecyclerView.setVisibility(View.GONE);
                tv_no_events.setVisibility(View.VISIBLE);
                tv_no_events.setText("Sorry, no videos found");
                if(swipeRefreshLayout.isRefreshing()){
                    swipeRefreshLayout.setRefreshing(false);
                }
                swipeRefreshLayout.setRefreshing(false);
                //VolleyLog.d("Error", "Error: " + error.getMessage());
                String message="";
                if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }


                    if (!message.equalsIgnoreCase(""))
                        Toast.makeText(mActivity, message, Toast.LENGTH_SHORT).show();


            }
        }) {

            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {

                if(response!=null) {
                    System.out.println("Response code from parse Network response is " + response.statusCode + " and data is " + response.toString() + " and data is " + response.data.toString());
                    try {
                        String jsonString = new String(response.data, "UTF-8");
                        System.out.println("Response from parse Network response is " + jsonString);

                        Gson gson = new Gson();
                        final VideosPojo videosPojo = gson.fromJson(jsonString, VideosPojo.class); // (reader, Login_Response_Pojo.class);

                        // System.out.println("Response pojo is "+videosPojo.getVideosResponsePojoArrayList().get(0).getUrl());
                        if (response.statusCode == 200) {

                            if (videosPojo.getVideosResponsePojoArrayList().size() != 0) {
                                //Toast.makeText(mActivity,"Login Successful",Toast.LENGTH_SHORT).show();
                                videosResponsePojoArrayList.clear();
                                for (int i = 0; i < videosPojo.getVideosResponsePojoArrayList().size(); i++) {
                                    videosResponsePojoArrayList.add(i, videosPojo.getVideosResponsePojoArrayList().get(i));
                                }

                                ((HomeScreenActivity) context).runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {


                                        mRecyclerView.setVisibility(View.VISIBLE);
                                        tv_no_events.setVisibility(View.GONE);
                                        videosAdapter.notifyDataSetChanged();

                                        swipeRefreshLayout.setRefreshing(false);

                                    }
                                });
                            } else {

                                ((HomeScreenActivity) context).runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {

                                        mRecyclerView.setVisibility(View.GONE);
                                        tv_no_events.setVisibility(View.VISIBLE);
                                        tv_no_events.setText("Sorry, no videos found");
                                    }
                                });

                            }

                        }
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
                return super.parseNetworkResponse(response);
            }


            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {


                NetworkResponse networkResponse=volleyError.networkResponse;
                if(networkResponse!=null){

                    String jsonString="";
                    try {
                        jsonString = new String(networkResponse.data, "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                    System.out.println("Network response is "+networkResponse.statusCode+jsonString);
                    try {
                        JSONObject jsonObject=new JSONObject(jsonString);
                        JSONArray jsonArray=jsonObject.getJSONArray("error");
                        final String errorString=jsonArray.getString(0);
                        ((HomeScreenActivity)context).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toasty.error(context,errorString,Toast.LENGTH_SHORT).show();
                            }
                        });


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
                return super.parseNetworkError(volleyError);
            }

            /**
             * Passing some request headers
             * */

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Authorization",api_token);
                headers.put("Client-Id","1");
                return headers;
            }
        };

//// Adding request to request queue
        singleton.addToRequestQueue(jsonObjReq, "Videos api calling");

    }
    @TargetApi(23)
    @Override public void onAttach(Context context) {
        //This method avoid to call super.onAttach(context) if I'm not using api 23 or more
        //if (Build.VERSION.SDK_INT >= 23) {
        super.onAttach(context);
        onAttachToContext(context);
        //}
    }

    /*
     * Deprecated on API 23
     * Use onAttachToContext instead
     */
    @SuppressWarnings("deprecation")
    @Override public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (Build.VERSION.SDK_INT < 23) {
            onAttachToContext(activity);
        }
    }
    protected void onAttachToContext(Context context){

        mActivity=(Activity)context;
    }
}
