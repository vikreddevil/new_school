package com.snaptech.emissionew.AfterLoginModules;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;

import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;

import com.snaptech.emissionew.AfterLoginModules.Pojos.GroupsDataResponsePojo;
import com.snaptech.emissionew.AfterLoginModules.Pojos.GroupsPojo;
import com.snaptech.emissionew.R;
import com.snaptech.emissionew.Utils.Constants;
import com.snaptech.emissionew.Utils.Singleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import es.dmoral.toasty.Toasty;

import static com.snaptech.emissionew.Utils.Constants.BASE_URL;

public class GroupsPopupActivity extends AppCompatActivity {

    //private RecyclerView recyclerView;
    //private GroupPopUpAdapter groupPopUpAdapter;
    private Singleton singleton;
    private SharedPreferences prefs;
    private String api_token;
    private String role_id="";
    private ListView listView;
//    private ArrayList<GroupsDataResponsePojo> groupsDataResponsePojoArrayList;
    private ArrayList<Boolean> flag_check_list;
    private Button btn_confirm;
    private Button btn_cancel;
    private ArrayList<String> str_group_id_array=new ArrayList<>();
    private ArrayList<String> str_group_name_array_checked=new ArrayList<>();
    private ArrayList<String> str_group_id_array_checked=new ArrayList<>();
    ArrayAdapter<String> adapter;
    private CheckBox chec_all;
    private HashMap<Integer,Integer> hashMap=new HashMap<>();
    private SparseBooleanArray sparseBooleanArray=new SparseBooleanArray();

   // private CheckBox checkBox;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_groups_popup);
        prefs= GroupsPopupActivity.this.getSharedPreferences(Constants.LOGIN_PREFS_NAME, MODE_PRIVATE);
        // tour_id=prefs.getString(TOUR_ID,null);
        api_token = prefs.getString(Constants.ACCESS_TOKEN_PREFS_KEY, null);
        //checkBox=(CheckBox)findViewById(R.id.chkAll);
        btn_cancel=(Button)findViewById(R.id.btn_cancel_popup);
        chec_all=(CheckBox)findViewById(R.id.chk_all);
        btn_confirm=(Button)findViewById(R.id.btn_confirm_popup);
        singleton=Singleton.getInstance();
        role_id=prefs.getString(Constants.ROLE_ID,null);
        listView=(ListView)findViewById(R.id.list);
         adapter = new ArrayAdapter<>(this, R.layout.simple_list_item_multiple_choice, str_group_id_array);
        listView.setAdapter(adapter);
//        recyclerView=(RecyclerView)findViewById(R.id.recycler_view);
//        recyclerView.setLayoutManager(new LinearLayoutManager(GroupsPopupActivity.this));
        //groupsDataResponsePojoArrayList=new ArrayList<>();
        flag_check_list=new ArrayList<>();
       // groupPopUpAdapter=new GroupPopUpAdapter(GroupsPopupActivity.this,groupsDataResponsePojoArrayList,flag_check_list);
       // recyclerView.addItemDecoration(new VerticalSpaceDecoration(2));
//        recyclerView.setAdapter(groupPopUpAdapter);


        DisplayMetrics dm=new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width=dm.widthPixels;
        int height=dm.heightPixels;

        getWindow().setLayout((int)(width*.9), ((int)(height*.6)));


        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
                Intent intent = new Intent();

                setResult(RESULT_CANCELED, intent);
            }
        });

        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sparseBooleanArray=listView.getCheckedItemPositions();


                //System.out.println("item selected is "+sparseBooleanArray.get(0));
                str_group_id_array_checked.clear();
                str_group_name_array_checked.clear();
                int j=0;
                for (int i=0;i<sparseBooleanArray.size();i++){

                    System.out.println("selected for position "+i+" is "+sparseBooleanArray.get(i));
                    if(sparseBooleanArray.get(i)){

                        str_group_name_array_checked.add(j,str_group_id_array.get(i));
                        str_group_id_array_checked.add(j,hashMap.get(i)+"");
                        j++;

                    }
                }
                Intent intent = new Intent();
                //Bundle bundle=new Bundle();

                intent.putStringArrayListExtra("group_ids",str_group_id_array_checked);
                intent.putStringArrayListExtra("group_names",str_group_name_array_checked);
                setResult(RESULT_OK, intent);
                finish();

            }
        });
        View.OnClickListener clickListener = new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                CheckBox chk = (CheckBox) v;
                int itemCount = listView.getCount();
                for(int i=0 ; i < itemCount ; i++){
                    listView.setItemChecked(i, chk.isChecked());
                }
            }
        };
        AdapterView.OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                CheckBox chk = (CheckBox) findViewById(R.id.chk_all);
                int checkedItemCount = listView.getCheckedItemCount();

                if(listView.getCount()==checkedItemCount)
                    chk.setChecked(true);
                else
                    chk.setChecked(false);
            }
        };


        /** Getting reference to checkbox available in the main.xml layout */


        /** Setting a click listener for the checkbox **/
        chec_all.setOnClickListener(clickListener);

        /** Setting a click listener for the listitem checkbox **/
        listView.setOnItemClickListener(itemClickListener);
        callGroupsListsApi(GroupsPopupActivity.this);
//        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//
//
//            }
//        });

    }


    private void callGroupsListsApi(final Context context)
    {


        Map<String, String> params= new HashMap<String, String>();

        params.put("role_id",role_id);


//        ArrayList<String> arrayList=new ArrayList<String>();
//        arrayList.add(0,"5");

        Gson gson=new Gson();
//        JSONObject jsonObject=null;
//        try {
//            jsonObject=new JSONObject(gson.toJson(addLinksRequestPojo));
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                BASE_URL+"api/group/list", new JSONObject(params),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        if(response!=null)
                            Log.d("Response", response.toString());


                        //Toast.makeText(mActivity,"Login Successful",Toast.LENGTH_SHORT).show();

                    }
                }, new Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {

                //VolleyLog.d("Error", "Error: " + error.getMessage());
                String message="";
                if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }


                if (!message.equalsIgnoreCase(""))
                    Toasty.error(context, message, Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {

                if(response!=null) {
                    System.out.println("Response code from parse Network response is " + response.statusCode + " and data is " + response.toString() + " and data is " + response.data.toString());
                    try {
                        String jsonString = new String(response.data, "UTF-8");
                        System.out.println("Response from parse Network response is " + jsonString);

                        //Gson gson = new Gson();
                        //final LinksPojo linksPojo = gson.fromJson(jsonString, LinksPojo.class);

                        if (response.statusCode == 200) {

                            //Toast.makeText(mActivity,"Login Successful",Toast.LENGTH_SHORT).show();



                            Gson gson1=new Gson();
                            final GroupsPojo groupsPojo=gson1.fromJson(jsonString,GroupsPojo.class);






                            ((GroupsPopupActivity) context).runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    //System.out.println("Number of groups "+groupsPojo.getGroupsDataResponsePojoArrayList().size());
                                    if(groupsPojo!=null) {
                                        //groupsDataResponsePojoArrayList.clear();
                                        GroupsDataResponsePojo groupsDataResponsePojo=new GroupsDataResponsePojo();
                                        groupsDataResponsePojo.setGroup_id(0);
                                        groupsDataResponsePojo.setGroup_name("All Groups");
                                        //groupsDataResponsePojoArrayList.add(0,groupsDataResponsePojo);
                                        flag_check_list.clear();
                                        str_group_id_array.clear();
                                        hashMap.clear();
                                        if (groupsPojo.getGroupsDataResponsePojoArrayList().size() != 0) {
                                            for (int i = 0; i < groupsPojo.getGroupsDataResponsePojoArrayList().size(); i++) {
                                               // groupsDataResponsePojoArrayList.add(i, groupsPojo.getGroupsDataResponsePojoArrayList().get(i));
                                                flag_check_list.add(i,false);
                                                str_group_id_array.add(i,groupsPojo.getGroupsDataResponsePojoArrayList().get(i).getGroup_name());
                                                adapter.notifyDataSetChanged();
                                                hashMap.put(i,groupsPojo.getGroupsDataResponsePojoArrayList().get(i).getGroup_id());

                                            }
                                            int itemCount = listView.getCount();
                                            for(int i=0 ; i < itemCount ; i++){
                                                listView.setItemChecked(i, false);
                                            }


                                            //groupPopUpAdapter.notifyDataSetChanged();
                                        }
                                    }
//                                    Toast.makeText(Gr.this,"Link added Successfully",Toast.LENGTH_SHORT).show();

                                }
                            });



                        }
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
                return super.parseNetworkResponse(response);
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {

                NetworkResponse networkResponse=volleyError.networkResponse;
                if(networkResponse!=null){

                    String jsonString="";
                    try {
                        jsonString = new String(networkResponse.data, "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                    System.out.println("Network response is "+networkResponse.statusCode+jsonString);
                    try {
                        JSONObject jsonObject=new JSONObject(jsonString);
                        JSONArray jsonArray=jsonObject.getJSONArray("error");
                        final String errorString=jsonArray.getString(0);
                        ((HomeScreenActivity)context).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toasty.error(context,errorString,Toast.LENGTH_SHORT).show();
                            }
                        });


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
                return super.parseNetworkError(volleyError);
            }

            /**
             * Passing some request headers
             * */

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Authorization",api_token);
                headers.put("Client-Id","1");
                return headers;
            }

        };

        singleton.addToRequestQueue(jsonObjReq, "Groups List api calling");

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
