package com.snaptech.emissionew.AfterLoginModules.Pojos;

import java.util.ArrayList;

/**
 * Created by vikas on 20/03/17.
 */

public class AddMessageRequestPojo {


    private String title;
    private String desc;
    private ArrayList<String> group_add_id;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public ArrayList<String> getGroup_add_id() {
        return group_add_id;
    }

    public void setGroup_add_id(ArrayList<String> group_add_id) {
        this.group_add_id = group_add_id;
    }
}
