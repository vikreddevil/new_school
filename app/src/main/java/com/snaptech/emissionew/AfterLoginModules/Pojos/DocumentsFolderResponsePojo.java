package com.snaptech.emissionew.AfterLoginModules.Pojos;

/**
 * Created by vikas on 14/03/17.
 */

public class DocumentsFolderResponsePojo {

    //{"data":[{"folder_name":"Test PDF Folder ","folder_created_date":"2017-03-06","data":[{"id":8,"name":"Dummy","description":"Dummy","url":"https://s3-us-west-2.amazonaws.com/communication-document%2Fnew_school%2FDummy/dummy_pdf_1_pdf","document_folder_id":3,"folder_name":"Test PDF Folder ","created_at":"2017-03-06 10:39:27"}]},{"folder_name":"test","folder_created_date":"2017-02-14","data":[{"id":4,"name":"test","description":"test","url":"https://s3-us-west-2.amazonaws.com/communication-document%2Fnew_school%2Ftest/dummy_pdf_pdf","document_folder_id":1,"folder_name":"test","created_at":"2017-02-17 08:28:01"}]},{"id":6,"name":"moses doc name","description":"moses doc desc","url":"https://s3-us-west-2.amazonaws.com/communication-document%2Fnew_school%2Fmoses%20doc%20name/Customer_pdf_pdf","document_folder_id":null,"folder_name":null,"created_at":"2017-03-03 10:03:44"},{"id":5,"name":"moses test name","description":"moses test desc","url":"https://s3-us-west-2.amazonaws.com/communication-document%2Fnew_school%2Fmoses%20test%20name/pdf_sample_pdf","document_folder_id":null,"folder_name":null,"created_at":"2017-03-03 09:59:38"}]}
    private int id;
    private String name;
    private String description;
    private String url;
    private String document_folder_id;
    private String folder_name;
    private String created_at;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDocument_folder_id() {
        return document_folder_id;
    }

    public void setDocument_folder_id(String document_folder_id) {
        this.document_folder_id = document_folder_id;
    }

    public String getFolder_name() {
        return folder_name;
    }

    public void setFolder_name(String folder_name) {
        this.folder_name = folder_name;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }
}
