package com.snaptech.emissionew.AfterLoginModules.Pojos;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by vikas on 16/03/17.
 */

public class GroupsPojo {
    public ArrayList<GroupsDataResponsePojo> getGroupsDataResponsePojoArrayList() {
        return groupsDataResponsePojoArrayList;
    }

    public void setGroupsDataResponsePojoArrayList(ArrayList<GroupsDataResponsePojo> groupsDataResponsePojoArrayList) {
        this.groupsDataResponsePojoArrayList = groupsDataResponsePojoArrayList;
    }

    @SerializedName("data")
    private ArrayList<GroupsDataResponsePojo> groupsDataResponsePojoArrayList;
}
