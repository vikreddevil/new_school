package com.snaptech.emissionew.AfterLoginModules.Adapters;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;


import com.snaptech.emissionew.AfterLoginModules.FullScreenViewActivity;
import com.snaptech.emissionew.AfterLoginModules.Pojos.PhotoGridDataResponsePojo;
import com.snaptech.emissionew.R;
import com.snaptech.emissionew.Utils.Constants;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by vikas on 01/11/16.
 */

public class ImageAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<PhotoGridDataResponsePojo> photoGridDataResponsePojoArrayList;
    public ImageAdapter(Context c, ArrayList<PhotoGridDataResponsePojo> photoGridDataResponsePojoArrayList) {
        this.photoGridDataResponsePojoArrayList=photoGridDataResponsePojoArrayList;
        mContext = c;
      //  Constants.photoGridDataResponsePojos.clear();

    }

    public int getCount() {
        return photoGridDataResponsePojoArrayList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(final int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        if (convertView == null) {

            //Calculation of ImageView Size - density independent.
            //maybe you should do this calculation not exactly in this method but put is somewhere else.
            Resources r = Resources.getSystem();
            float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 165, r.getDisplayMetrics());


            imageView = new ImageView(mContext);
            imageView.setLayoutParams(new GridView.LayoutParams((int)px, (int)px));
            imageView.setPadding(4, 4, 4, 4);
            imageView.setBackgroundColor(Color.parseColor("#FFFFFF"));

            String image_path="";
            image_path=photoGridDataResponsePojoArrayList.get(position).getThumbnail_image();
            //Constants.photoGridDataResponsePojos.add(position,photoGridDataResponsePojoArrayList.get(position));
            URL sourceUrl=null;
            String temp=photoGridDataResponsePojoArrayList.get(position).getThumbnail_image();
            temp = temp.replaceAll(" ", "%20");
            try {
                sourceUrl = new URL(temp);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            if(image_path!=null){
                if(!image_path.equalsIgnoreCase("")){

                    System.out.println("position is "+position +" ");
                    if(sourceUrl!=null) {
                        Picasso.with(mContext).load(sourceUrl.toString()).fit().centerInside()
                                .error(R.drawable.placeholder_album)
                                .placeholder(R.drawable.placeholder_album)
                                .into(imageView, new Callback() {
                                    @Override
                                    public void onSuccess() {

                                    }

                                    @Override
                                    public void onError() {

                                    }
                                });
                    }
                }
            }

            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent i = new Intent(mContext, FullScreenViewActivity.class);
                    i.putExtra("position", position);
                    i.putExtra("url",photoGridDataResponsePojoArrayList.get(position).getLarge_image());
                    i.putExtra("caption",photoGridDataResponsePojoArrayList.get(position).getImage_name());
                    Constants.album_name=photoGridDataResponsePojoArrayList.get(position).getImage_name();
                    mContext.startActivity(i);
                }
            });

        } else {
            imageView = (ImageView) convertView;
        }

        //imageView.setImageResource(mThumbIds[position]);
        return imageView;
    }

    // references to our images
//    private Integer[] mThumbIds = {
//            R.drawable.photos_temp,R.drawable.grid2,
//            R.drawable.grid3, R.drawable.grid4,
//            R.drawable.grid1, R.drawable.grid5,
//            R.drawable.grid2, R.drawable.grid3,
//            R.drawable.photos_temp, R.drawable.grid1
//    };
}