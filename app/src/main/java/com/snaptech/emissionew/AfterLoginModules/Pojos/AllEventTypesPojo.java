package com.snaptech.emissionew.AfterLoginModules.Pojos;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by vikas on 27/03/17.
 */

public class AllEventTypesPojo {

    @SerializedName("data")
    private ArrayList<AllEventTypesDataResponsePojo> allEventTypesDataResponsePojoArrayList;

    public ArrayList<AllEventTypesDataResponsePojo> getAllEventTypesDataResponsePojoArrayList() {
        return allEventTypesDataResponsePojoArrayList;
    }

    public void setAllEventTypesDataResponsePojoArrayList(ArrayList<AllEventTypesDataResponsePojo> allEventTypesDataResponsePojoArrayList) {
        this.allEventTypesDataResponsePojoArrayList = allEventTypesDataResponsePojoArrayList;
    }
}
