package com.snaptech.emissionew.AfterLoginModules.Pojos;

import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

import java.util.ArrayList;

/**
 * Created by vikas on 28/02/17.
 */

public class MobileSideBarPojo {

public MobileSideBarPojo(){


}

    @SerializedName("data")
    ArrayList<MobileSideBarDataResponsePojo> mobileSideBarDataResponsePojoArrayList;

    public ArrayList<MobileSideBarDataResponsePojo> getMobileSideBarDataResponsePojoArrayList() {
        return mobileSideBarDataResponsePojoArrayList;
    }

    public void setMobileSideBarDataResponsePojoArrayList(ArrayList<MobileSideBarDataResponsePojo> mobileSideBarDataResponsePojoArrayList) {
        this.mobileSideBarDataResponsePojoArrayList = mobileSideBarDataResponsePojoArrayList;
    }
}
