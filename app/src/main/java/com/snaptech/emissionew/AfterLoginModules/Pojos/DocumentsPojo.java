package com.snaptech.emissionew.AfterLoginModules.Pojos;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by vikas on 01/11/16.
 */

//documents list pojo

public class DocumentsPojo {

    @SerializedName("data")
    private ArrayList<DocumentsResponsePojo> documentsResponsePojoArrayList;

    public ArrayList<DocumentsResponsePojo> getDocumentsResponsePojoArrayList() {
        return documentsResponsePojoArrayList;
    }

    public void setDocumentsResponsePojoArrayList(ArrayList<DocumentsResponsePojo> documentsResponsePojoArrayList) {
        this.documentsResponsePojoArrayList = documentsResponsePojoArrayList;
    }
}
