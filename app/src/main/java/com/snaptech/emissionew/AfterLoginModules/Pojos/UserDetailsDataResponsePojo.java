package com.snaptech.emissionew.AfterLoginModules.Pojos;

/**
 * Created by vikas on 09/03/17.
 */

public class UserDetailsDataResponsePojo {



   //{"data":[{"document_type_id":1,"insurance_nominee_name":null,"meal_type":"veg","is_domestic":0,
    // "emergency_contact_no":null,"image":null,"place_of_issue":null,"
    // passport_no":null,"trip_id":"ABBO22-02","document_type_no":null,"id":8,"first_name":"AVI",
    // "insurance_nominee_mob_no":null,"itenary_no":1,"passport_expiry":null,"dob":"1994-08-07",
    // "document_type_name":"Passport","last_name":"JAIN","gender":"male","tour_id":1,"email_id":"avi@snaptech.in",
    // "passport_issue":null,"mobile":"9819749732"}],"message":"success"}


    private int user_id;
    private String student_first_name;
    private String student_last_name;
    private String student_dob;
    private String profile;
    private String user_status;
    private String student_gender;
    private String email;
    private String mobile;
    private int role_id;
    private String parent_first_name;
    private String parent_last_name;


    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getStudent_first_name() {
        return student_first_name;
    }

    public void setStudent_first_name(String student_first_name) {
        this.student_first_name = student_first_name;
    }

    public String getStudent_last_name() {
        return student_last_name;
    }

    public void setStudent_last_name(String student_last_name) {
        this.student_last_name = student_last_name;
    }

    public String getStudent_dob() {
        return student_dob;
    }

    public void setStudent_dob(String student_dob) {
        this.student_dob = student_dob;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getUser_status() {
        return user_status;
    }

    public void setUser_status(String user_status) {
        this.user_status = user_status;
    }

    public String getStudent_gender() {
        return student_gender;
    }

    public void setStudent_gender(String student_gender) {
        this.student_gender = student_gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public int getRole_id() {
        return role_id;
    }

    public void setRole_id(int role_id) {
        this.role_id = role_id;
    }

    public String getParent_first_name() {
        return parent_first_name;
    }

    public void setParent_first_name(String parent_first_name) {
        this.parent_first_name = parent_first_name;
    }

    public String getParent_last_name() {
        return parent_last_name;
    }

    public void setParent_last_name(String parent_last_name) {
        this.parent_last_name = parent_last_name;
    }
}
