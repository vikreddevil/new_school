package com.snaptech.emissionew.AfterLoginModules.Pojos;

/**
 * Created by vikas on 28/11/16.
 */

public class NotificationResponsePojo {

    private int notification_id;
    private String title;
    private String description;
    private String created_at;

    public int getNotification_id() {
        return notification_id;
    }

    public void setNotification_id(int notification_id) {
        this.notification_id = notification_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }
}
