package com.snaptech.emissionew.AfterLoginModules.Fragments;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.snaptech.emissionew.AfterLoginModules.Adapters.DocumentsAdapter;
import com.snaptech.emissionew.AfterLoginModules.DocumentsFolderActivity;
import com.snaptech.emissionew.AfterLoginModules.HomeScreenActivity;
import com.snaptech.emissionew.AfterLoginModules.PDFWebViewer;
import com.snaptech.emissionew.AfterLoginModules.Pojos.DocumentsPojo;
import com.snaptech.emissionew.AfterLoginModules.Pojos.DocumentsResponsePojo;
import com.snaptech.emissionew.R;
import com.snaptech.emissionew.Utils.Constants;
import com.snaptech.emissionew.Utils.RecyclerItemClickListener;
import com.snaptech.emissionew.Utils.Singleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import es.dmoral.toasty.Toasty;

import static android.content.Context.MODE_PRIVATE;
import static com.snaptech.emissionew.AfterLoginModules.Adapters.DocumentsAdapter.DocumentsResponsePjoList;
import static com.snaptech.emissionew.Utils.Constants.BASE_URL;


/**
 * Created by vikas on 01/11/16.
 */

//Documents Fragment shows the list of documents

public class DocumentsFragment extends Fragment {

    Activity mActivity;
    private SharedPreferences prefs;
    private String api_token;
    private Singleton singleton;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView mRecyclerView;
    private DocumentsAdapter documentsAdapter;
    private ArrayList<DocumentsResponsePojo> documentsPojoList;
    protected RecyclerView.LayoutManager mLayoutManager;
    private static final String KEY_LAYOUT_MANAGER = "layoutManager";
    private TextView tv_no_events;
    private String message="";
    private int position;
    private ImageView iv_add;
    private boolean flag_permission_ex_storage=false;
    private enum LayoutManagerType {
        GRID_LAYOUT_MANAGER,
        LINEAR_LAYOUT_MANAGER
    }
    protected LayoutManagerType mCurrentLayoutManagerType;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view;
        view=inflater.inflate(R.layout.fragment_notification,container,false);
        getRef(view);
        Toolbar toolbar = (Toolbar) mActivity.findViewById(R.id.toolbar);
        iv_add= (ImageView) toolbar.findViewById(R.id.iv_add);
        iv_add.setVisibility(View.GONE);
        mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;

        if (savedInstanceState != null) {
            // Restore saved layout manager type.
            mCurrentLayoutManagerType = (LayoutManagerType) savedInstanceState
                    .getSerializable(KEY_LAYOUT_MANAGER);
        }
       setRecyclerViewLayoutManager(mCurrentLayoutManagerType);

        //pull to refresh
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                //api call

                callDocumentsApi(mActivity);
            }
        });
        //api call
        callDocumentsApi(mActivity);
        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(mActivity, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int i) {

                position=i;

                //check permission


                if(!checkExternalStoragePermission()) {
                    flag_permission_ex_storage=false;
                    ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
                }
                else
                    flag_permission_ex_storage=true;

                if(flag_permission_ex_storage) {

                    if(DocumentsResponsePjoList.get(position).getFolder_name()==null) {
                        Intent intent = new Intent(mActivity, PDFWebViewer.class);
                        intent.putExtra("url", DocumentsResponsePjoList.get(position).getUrl());
                        intent.putExtra("urltitle", DocumentsResponsePjoList.get(position).getName());
                        intent.putExtra("id", DocumentsResponsePjoList.get(position).getId() + "");
                        startActivity(intent);
                    }
                    else{

                        DocumentsAdapter.documentsFolderResponsePojoArrayList.clear();
                        DocumentsAdapter.documentsFolderResponsePojoArrayList=DocumentsResponsePjoList.get(position).getDocumentsFolderResponsePojoArrayList();
                        Intent intent = new Intent(mActivity, DocumentsFolderActivity.class);
                        intent.putExtra("folder_name", DocumentsResponsePjoList.get(position).getFolder_name());

                        startActivity(intent);
                    }
                }
            }
        }));
        return view;
    }
    private void getRef(View view){
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        tv_no_events=(TextView)view.findViewById(R.id.tv_comingsoon);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        documentsPojoList=new ArrayList<>();
        documentsAdapter=new DocumentsAdapter(mActivity,documentsPojoList);
        mRecyclerView.setAdapter(documentsAdapter);
        singleton=Singleton.getInstance();
        swipeRefreshLayout=(SwipeRefreshLayout)view.findViewById(R.id.swiperefresh_links);
        prefs= mActivity.getSharedPreferences(Constants.LOGIN_PREFS_NAME, MODE_PRIVATE);
        api_token = prefs.getString(Constants.ACCESS_TOKEN_PREFS_KEY, null);
    }
    public void setRecyclerViewLayoutManager(LayoutManagerType layoutManagerType) {
        int scrollPosition = 0;

        // If a layout manager has already been set, get current scroll position.
        if (mRecyclerView.getLayoutManager() != null) {
            scrollPosition = ((LinearLayoutManager) mRecyclerView.getLayoutManager())
                    .findFirstCompletelyVisibleItemPosition();
        }

        switch (layoutManagerType) {
            case GRID_LAYOUT_MANAGER:
                mLayoutManager = new GridLayoutManager(mActivity, 2);
                mCurrentLayoutManagerType = LayoutManagerType.GRID_LAYOUT_MANAGER;
                break;
            case LINEAR_LAYOUT_MANAGER:
                mLayoutManager = new LinearLayoutManager(mActivity);
                mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
                break;
            default:
                mLayoutManager = new LinearLayoutManager(mActivity);
                mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
        }

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.scrollToPosition(scrollPosition);
    }

    //api call for fetching list of documents

    private void callDocumentsApi(final Context context)
    {

        swipeRefreshLayout.setRefreshing(true);

        Map<String, String> params= new HashMap<>();

        params.put("user_id", "1");

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                BASE_URL+"api/documents", new JSONObject(params),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        if(response!=null)
                        Log.d("Response", response.toString());



                        //Toast.makeText(mActivity,"Login Successful",Toast.LENGTH_SHORT).show();
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }, new Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {

                //if error response showing no documents found

                mRecyclerView.setVisibility(View.GONE);
                tv_no_events.setVisibility(View.VISIBLE);
                tv_no_events.setText("Sorry, no documents found");
                    swipeRefreshLayout.setRefreshing(false);

                VolleyLog.d("Error", "Error: " + error.getMessage());



                if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }

                ((HomeScreenActivity)context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(message!=null) {
                            if (!message.equalsIgnoreCase(""))
                                Toasty.error(context, message, Toast.LENGTH_SHORT).show();
                        }
                    }
                });

            }
        }) {

            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {

                if(response!=null) {
                    System.out.println("Response code from parse Network response is " + response.statusCode + " and data is " + response + " and data is " + response.data);
                    try {
                        String jsonString = new String(response.data, "UTF-8");
                        System.out.println("Response from parse Network response is " + jsonString);

                        Gson gson = new Gson();
                        final DocumentsPojo documentsPojo = gson.fromJson(jsonString, DocumentsPojo.class);

                        if (response.statusCode == 200) {

                            if (documentsPojo.getDocumentsResponsePojoArrayList().size() != 0) {


                                documentsPojoList.clear();
                                for (int i = 0; i < documentsPojo.getDocumentsResponsePojoArrayList().size(); i++) {
                                    documentsPojoList.add(i, documentsPojo.getDocumentsResponsePojoArrayList().get(i));
                                }

                                ((HomeScreenActivity) context).runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        mRecyclerView.setVisibility(View.VISIBLE);
                                        tv_no_events.setVisibility(View.GONE);
                                        documentsAdapter.notifyDataSetChanged();
                                        swipeRefreshLayout.setRefreshing(false);
                                    }
                                });


                            } else {

                                ((HomeScreenActivity) context).runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {


                                        mRecyclerView.setVisibility(View.GONE);
                                        tv_no_events.setVisibility(View.VISIBLE);
                                        tv_no_events.setText("Sorry, no documents found");
                                    }
                                });

                            }
                        }
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
                return super.parseNetworkResponse(response);
            }


            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {

                NetworkResponse networkResponse=volleyError.networkResponse;
                if(networkResponse!=null){

                    String jsonString="";
                    try {
                        jsonString = new String(networkResponse.data, "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                    System.out.println("Network response is "+networkResponse.statusCode+jsonString);
                    try {
                        JSONObject jsonObject=new JSONObject(jsonString);
                        JSONArray jsonArray=jsonObject.getJSONArray("error");
                        final String errorString=jsonArray.getString(0);
                        ((HomeScreenActivity)context).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toasty.error(context,errorString,Toast.LENGTH_SHORT).show();
                            }
                        });


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
                return super.parseNetworkError(volleyError);
            }

            /**
             * Passing some request headers
             * */

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Authorization",api_token);
                headers.put("Client-Id","1");
                return headers;
            }



        };

        singleton.addToRequestQueue(jsonObjReq, "Documents new api calling");

    }
    @TargetApi(23)
    @Override public void onAttach(Context context) {
        //This method avoid to call super.onAttach(context) if I'm not using api 23 or more
        //if (Build.VERSION.SDK_INT >= 23) {
        super.onAttach(context);
        onAttachToContext(context);
        //}
    }

    /*
     * Deprecated on API 23
     * Use onAttachToContext instead
     */
    @SuppressWarnings("deprecation")
    @Override public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (Build.VERSION.SDK_INT < 23) {
            onAttachToContext(activity);
        }
    }
    protected void onAttachToContext(Context context){

        mActivity=(Activity)context;
    }
    private boolean checkExternalStoragePermission()
    {

        if (Build.VERSION.SDK_INT >= 23) {
            if (getActivity().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
//                Log.v(TAG,"Permission is granted");
                System.out.println("First condition");
                return true;
            } else {

                System.out.println("Second condition");
                //Log.v(TAG,"Permission is revoked");
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            // Log.v(TAG,"Permission is granted");
            System.out.println("Third condition");
            return true;
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                    if(DocumentsResponsePjoList.size()!=0) {

                        if(DocumentsResponsePjoList.get(position).getFolder_name()==null) {
                            Intent intent = new Intent(mActivity, PDFWebViewer.class);
                            intent.putExtra("url", DocumentsResponsePjoList.get(position).getUrl());
                            intent.putExtra("urltitle", DocumentsResponsePjoList.get(position).getName());
                            intent.putExtra("id", DocumentsResponsePjoList.get(position).getId() + "");
                            startActivity(intent);
                        }
                        else{

                            DocumentsAdapter.documentsFolderResponsePojoArrayList.clear();
                            DocumentsAdapter.documentsFolderResponsePojoArrayList=DocumentsResponsePjoList.get(position).getDocumentsFolderResponsePojoArrayList();
                            Intent intent = new Intent(mActivity, DocumentsFolderActivity.class);
                            intent.putExtra("folder_name", DocumentsResponsePjoList.get(position).getFolder_name());

                            startActivity(intent);
                        }

                    }
                    // permission was granted, yay! do the
                    // calendar task you need to do.

                } else {

                    Toasty.error(mActivity,"Please give Storage permission to view pdf",Toast.LENGTH_SHORT).show();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'switch' lines to check for other
            // permissions this app might request
        }
    }
}
