package com.snaptech.emissionew.AfterLoginModules;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;

import com.snaptech.emissionew.R;

public class AttendanceChoicePopupActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance_choice_popup);

        DisplayMetrics dm=new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width=dm.widthPixels;
        //int height=dm.heightPixels;

        getWindow().setLayout((int)(width*.8), ActionBar.LayoutParams.WRAP_CONTENT);

    }
}
