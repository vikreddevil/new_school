package com.snaptech.emissionew.AfterLoginModules;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.snaptech.emissionew.AfterLoginModules.Adapters.DocumentsAdapter;
import com.snaptech.emissionew.AfterLoginModules.Adapters.DocumentsFolderAdapter;
import com.snaptech.emissionew.AfterLoginModules.Fragments.DocumentsFragment;
import com.snaptech.emissionew.AfterLoginModules.Pojos.DocumentsFolderResponsePojo;
import com.snaptech.emissionew.AfterLoginModules.Pojos.DocumentsResponsePojo;
import com.snaptech.emissionew.R;
import com.snaptech.emissionew.Utils.Constants;
import com.snaptech.emissionew.Utils.Singleton;

import java.util.ArrayList;

public class DocumentsFolderActivity extends AppCompatActivity {
    private SharedPreferences prefs;
    private String api_token;
    private Singleton singleton;
    private Toolbar toolbar;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView mRecyclerView;
    private DocumentsFolderAdapter documentsAdapter;
    private ArrayList<DocumentsFolderResponsePojo> documentsPojoList;
    protected RecyclerView.LayoutManager mLayoutManager;
    private static final String KEY_LAYOUT_MANAGER = "layoutManager";
    private TextView tv_no_events;
    private String message="";
    private int position;
    private ImageView iv_add;
    private boolean flag_permission_ex_storage=false;
    protected DocumentsFolderActivity.LayoutManagerType mCurrentLayoutManagerType;
    private enum LayoutManagerType {
        GRID_LAYOUT_MANAGER,
        LINEAR_LAYOUT_MANAGER
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_documents_folder);
        getRef();
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        Intent intent=getIntent();
        if(intent!=null){

            if(intent.getStringExtra("folder_name")!=null){
                toolbar.setTitle(intent.getStringExtra("folder_name"));
            }

        }

        //singleton=Singleton.getInstance();
        setSupportActionBar(toolbar);
        mCurrentLayoutManagerType = DocumentsFolderActivity.LayoutManagerType.LINEAR_LAYOUT_MANAGER;

        if (savedInstanceState != null) {
            // Restore saved layout manager type.
            mCurrentLayoutManagerType = (DocumentsFolderActivity.LayoutManagerType) savedInstanceState
                    .getSerializable(KEY_LAYOUT_MANAGER);
        }
        setRecyclerViewLayoutManager(mCurrentLayoutManagerType);

    }
    private void getRef(){
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        tv_no_events=(TextView)findViewById(R.id.tv_comingsoon);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(DocumentsFolderActivity.this));
        documentsPojoList=new ArrayList<>();
        documentsAdapter=new DocumentsFolderAdapter(DocumentsFolderActivity.this,DocumentsAdapter.documentsFolderResponsePojoArrayList);
        mRecyclerView.setAdapter(documentsAdapter);
        singleton= Singleton.getInstance();
        swipeRefreshLayout=(SwipeRefreshLayout)findViewById(R.id.swiperefresh_links);
        prefs= this.getSharedPreferences(Constants.LOGIN_PREFS_NAME, MODE_PRIVATE);
        api_token = prefs.getString(Constants.ACCESS_TOKEN_PREFS_KEY, null);
    }
    public void setRecyclerViewLayoutManager(DocumentsFolderActivity.LayoutManagerType layoutManagerType) {
        int scrollPosition = 0;

        // If a layout manager has already been set, get current scroll position.
        if (mRecyclerView.getLayoutManager() != null) {
            scrollPosition = ((LinearLayoutManager) mRecyclerView.getLayoutManager())
                    .findFirstCompletelyVisibleItemPosition();
        }

        switch (layoutManagerType) {
            case GRID_LAYOUT_MANAGER:
                mLayoutManager = new GridLayoutManager(DocumentsFolderActivity.this, 2);
                mCurrentLayoutManagerType = DocumentsFolderActivity.LayoutManagerType.GRID_LAYOUT_MANAGER;
                break;
            case LINEAR_LAYOUT_MANAGER:
                mLayoutManager = new LinearLayoutManager(DocumentsFolderActivity.this);
                mCurrentLayoutManagerType = DocumentsFolderActivity.LayoutManagerType.LINEAR_LAYOUT_MANAGER;
                break;
            default:
                mLayoutManager = new LinearLayoutManager(DocumentsFolderActivity.this);
                mCurrentLayoutManagerType = DocumentsFolderActivity.LayoutManagerType.LINEAR_LAYOUT_MANAGER;
        }

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.scrollToPosition(scrollPosition);
    }
}
