package com.snaptech.emissionew.AfterLoginModules;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.orm.query.Select;
import com.snaptech.emissionew.AfterLoginModules.Fragments.CalendarFragment;
import com.snaptech.emissionew.AfterLoginModules.Fragments.DocumentsFragment;
import com.snaptech.emissionew.AfterLoginModules.Fragments.InstituteInfoFragment;
import com.snaptech.emissionew.AfterLoginModules.Fragments.LinksFragment;
import com.snaptech.emissionew.AfterLoginModules.Fragments.NotificationFragment;
import com.snaptech.emissionew.AfterLoginModules.Fragments.PhotosFragment;
import com.snaptech.emissionew.AfterLoginModules.Fragments.SettingsFragment;
import com.snaptech.emissionew.AfterLoginModules.Fragments.VideosFragment;
import com.snaptech.emissionew.AfterLoginModules.Pojos.MobileSideBarDataResponsePojo;
import com.snaptech.emissionew.AfterLoginModules.Pojos.MobileSideBarPojo;
import com.snaptech.emissionew.R;
import com.snaptech.emissionew.Utils.Constants;
import com.snaptech.emissionew.Utils.Singleton;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.snaptech.emissionew.Utils.Constants.BASE_URL;

public class HomeScreenActivity extends AppCompatActivity {


    private NavigationView navView;
    private SharedPreferences sharedPreferences;
    private String color_primary;
    private String color_secondary;
    private LinearLayout ll_header;
    private DrawerLayout drawerLayout;
    private Toolbar toolbar;
    private Singleton singleton;
    private boolean signout_flag=false;
    private Fragment fragment;
    private FragmentManager fragmentManager;
    private boolean flag_nav_view=false;
    private  List<MobileSideBarDataResponsePojo> finalMobileSideBarDataResponsePojoList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(this.getString(R.string.home_screen_title));
        //singleton=Singleton.getInstance();
        setSupportActionBar(toolbar);
        singleton=Singleton.getInstance();

        finalMobileSideBarDataResponsePojoList=new ArrayList<>();
        drawerLayout = (DrawerLayout)findViewById(R.id.drawer);


        getDataFromSharedPreference();
        navView = (NavigationView) findViewById(R.id.navigation_view);
        ll_header= (LinearLayout) navView.getHeaderView(0).findViewById(R.id.ll_header);

        ll_header.setBackgroundColor(Color.parseColor(color_secondary));

        drawerLayout.addDrawerListener(new DrawerLayout.SimpleDrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();

            }

            @Override
            public void onDrawerStateChanged(int newState) {
                super.onDrawerStateChanged(newState);
            }
        });

//        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.drawer_open,R.string.drawer_close){
//
//            @Override
//            public void onDrawerClosed(View v){
//                super.onDrawerClosed(v);
//                flag_nav_view=false;
////                fragmentManager = getSupportFragmentManager();
//                System.out.println("drawer closed");
//                fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
//            }
//
//            @Override
//            public void onDrawerOpened(View v) {
//                super.onDrawerOpened(v);
//            }
//        };
        initNavigationDrawer();
        fragmentManager = getSupportFragmentManager();
        fragment = new LinksFragment();
        fragmentManager.beginTransaction().add(R.id.container, fragment).commit();




if(!Constants.flag_side_bar_updated) {
    if (MobileSideBarDataResponsePojo.count(MobileSideBarDataResponsePojo.class) != 0) {

        for (int i = 0; i < MobileSideBarDataResponsePojo.count(MobileSideBarDataResponsePojo.class); i++) {


            int position = Select.from(MobileSideBarDataResponsePojo.class).list().get(i).getSort();
            navView.getMenu().add(0, position, i, Select.from(MobileSideBarDataResponsePojo.class).list().get(i).getDisplay_name()).setIcon(R.drawable.home);


        }
    }
}
        if(Constants.flag_side_bar_updated)
        callSideBarApi();

    }
    private void getDataFromSharedPreference(){
        sharedPreferences=this.getSharedPreferences(Constants.LOGIN_PREFS_NAME,MODE_PRIVATE);
        color_primary=sharedPreferences.getString(Constants.PRIMARY_COLOR_PREFS_KEY,"#31498F");
        color_secondary=sharedPreferences.getString(Constants.SECONDARY_COLOR_PREFS_KEY,"#31498F");

    }
    private void initNavigationDrawer() {

        NavigationView navigationView = (NavigationView)findViewById(R.id.navigation_view);
        drawerLayout = (DrawerLayout)findViewById(R.id.drawer);
        List<MobileSideBarDataResponsePojo> mobileSideBarDataResponsePojoList=new ArrayList<MobileSideBarDataResponsePojo>();
        mobileSideBarDataResponsePojoList=Select.from(MobileSideBarDataResponsePojo.class).list();

        finalMobileSideBarDataResponsePojoList.clear();;
          finalMobileSideBarDataResponsePojoList = mobileSideBarDataResponsePojoList;
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                int position = menuItem.getItemId();

                System.out.println("item selected is "+position);
                // For AppCompat use getSupportFragmentManager

                // getting action for Sort value

//{"data":[{"action":"link","display_name":"LINK","sort":1},{"action":"document","display_name":"DOCUMENT","sort":2},{"action":"album","display_name":"ALBUM","sort":3},{"action":"messages","display_name":"MESSAGES","sort":4},{"action":"video","display_name":"VIDEOS","sort":5},{"action":"calendar","display_name":"CALENDAR","sort":6},{"action":"setting","display_name":"SETTING","sort":7},{"action":"institute_info","display_name":"INSTITUTE INFO","sort":8},{"action":"sign_out","display_name":"SIGN OUT","sort":9},{"action":"home","display_name":"HOME","sort":0}]}




                String action="";
                String title="";

                for(int i = 0; i< finalMobileSideBarDataResponsePojoList.size(); i++){

                    if(position== finalMobileSideBarDataResponsePojoList.get(i).getSort()) {
                        action = finalMobileSideBarDataResponsePojoList.get(i).getAction();
                        title=finalMobileSideBarDataResponsePojoList.get(i).getDisplay_name();
                    }
                }
                toolbar.setTitle(title);
                switch(action) {

//
                    case "link":

                        System.out.println("Link clicked");

                        fragment = new LinksFragment();

                        Constants.flag_close_app=false;
                        signout_flag=false;
                        drawerLayout.closeDrawers();
                        break;

                    case "document":

                        System.out.println("Document clicked");
                        fragment = new DocumentsFragment();
                        Constants.flag_close_app=false;
                        signout_flag=false;
                        drawerLayout.closeDrawers();

                        break;
                    case "album":

                        System.out.println("Album/photos clicked");
                        fragment = new PhotosFragment();
                        Constants.flag_close_app=false;
                        signout_flag=false;
                        drawerLayout.closeDrawers();
                        break;

                    case "messages":

                        System.out.println("Messages/notification clicked");
                        fragment = new NotificationFragment();

                        Constants.flag_close_app=false;
                        signout_flag=false;
                        drawerLayout.closeDrawers();
                        break;

                    case "video":

                        System.out.println("Videos clicked");
                        fragment = new VideosFragment();

                        Constants.flag_close_app=false;
                        signout_flag=false;
                        drawerLayout.closeDrawers();
                        break;

                    case "calendar":

                        System.out.println("Calendar clicked");
                        fragment = new CalendarFragment();

                        Constants.flag_close_app=false;
                        signout_flag=false;
                        drawerLayout.closeDrawers();
                        break;

                    case "setting":

                        System.out.println("Settings clicked");
                        fragment = new SettingsFragment();
                        Constants.flag_close_app=false;
                        signout_flag=false;
                        drawerLayout.closeDrawers();
                        break;
                    case "institute_info":

                        fragment = new InstituteInfoFragment();
                        Constants.flag_close_app=false;
                        signout_flag=false;
                        System.out.println("Institute info clicked");
                        drawerLayout.closeDrawers();
                        break;

                    case "attendance":

                        Intent intent=new Intent(HomeScreenActivity.this,AttendanceActivity.class);
                        startActivity(intent);
                        Constants.flag_close_app=false;
                        signout_flag=false;
                        System.out.println("Attendance clicked");
                        drawerLayout.closeDrawers();
                        break;

                    case "sign_out":

                        System.out.println("Signout clicked");
                        drawerLayout.closeDrawers();
                        break;
                    case "home":

                        System.out.println("Home clicked");
                        drawerLayout.closeDrawers();
                        break;

                }

                return true;
            }
        });


        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.drawer_open,R.string.drawer_close){

            @Override
            public void onDrawerClosed(View v){
                super.onDrawerClosed(v);
            }

            @Override
            public void onDrawerOpened(View v) {
                super.onDrawerOpened(v);
            }
        };
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }
    private void callSideBarApi()
    {

        System.out.println("Api called");
//        final ProgressDialog pDialog = new ProgressDialog(context);
//        pDialog.setMessage("Loading...");
//        pDialog.show();

        Map<String, String> params= new HashMap<>();


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                BASE_URL+"api/mobile/sidebar", new JSONObject(params),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        if(response!=null) {
                            Log.d("Response on sidebar", response.toString());
                        }
                        //pDialog.hide();

                    }
                }, new Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error", "Error: " + error.getMessage());
                String message=HomeScreenActivity.this.getString(R.string.network_error_message);
                System.out.println("Error is color "+error.getMessage());
                if (error instanceof NetworkError) {
                    message = HomeScreenActivity.this.getString(R.string.network_error_message);
                } else if (error instanceof ServerError) {
                    message = HomeScreenActivity.this.getString(R.string.server_error_message);
                }
                Toast.makeText(HomeScreenActivity.this,message,Toast.LENGTH_SHORT).show();

                //pDialog.hide();
            }

        }) {

            /**
             * Passing some request headers
             * */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Client-Id", "1");
                headers.put("Authorization","eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoxMiwiaWF0IjoxNDg2NjI3MDU1LCJleHAiOjE0OTE4MTEwNTV9.xd8kX4gZP__RzVPvL3q57KbOHLwYOYFkphcBT8JWn6w");
                //Client-Id
                return headers;
            }


            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {


                return super.parseNetworkError(volleyError);
            }

            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {


                if(response!=null){

                    try {
                        String jsonString = new String(response.data, "UTF-8");
                        System.out.println("status code is " + response.statusCode+" and response is "+jsonString);
                        if(response.statusCode==200){




                            Gson gson=new Gson();



                            final MobileSideBarPojo mobileSideBarPojo=gson.fromJson(jsonString, MobileSideBarPojo.class);



                            if(mobileSideBarPojo!=null){

                                if(mobileSideBarPojo.getMobileSideBarDataResponsePojoArrayList().size()!=0){

                                    finalMobileSideBarDataResponsePojoList.clear();
                                    finalMobileSideBarDataResponsePojoList=mobileSideBarPojo.getMobileSideBarDataResponsePojoArrayList();
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            MobileSideBarDataResponsePojo.deleteAll(MobileSideBarDataResponsePojo.class);
                                            Collections.sort(mobileSideBarPojo.getMobileSideBarDataResponsePojoArrayList());
                                            for(int i=0;i<mobileSideBarPojo.getMobileSideBarDataResponsePojoArrayList().size();i++) {

                                                mobileSideBarPojo.getMobileSideBarDataResponsePojoArrayList().get(i).save();
                                                int position=mobileSideBarPojo.getMobileSideBarDataResponsePojoArrayList().get(i).getSort();
                                                navView.getMenu().add(0,position,i,mobileSideBarPojo.getMobileSideBarDataResponsePojoArrayList().get(i).getDisplay_name()).setIcon(R.drawable.home);

                                            }
                                        }
                                    });
                                }
                            }


                        }
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }


                }


                return super.parseNetworkResponse(response);


            }
        };


        // Adding request to request queue


        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        jsonObjReq.setShouldCache(false);
        singleton.addToRequestQueue(jsonObjReq, "side bar api calling");

    }

}
//{"data":[{"action":"link","display_name":"LINK","sort":1},{"action":"document","display_name":"DOCUMENT","sort":2},{"action":"album","display_name":"ALBUM","sort":3},{"action":"messages","display_name":"MESSAGES","sort":4},{"action":"video","display_name":"VIDEOS","sort":5},{"action":"calendar","display_name":"CALENDAR","sort":6},{"action":"setting","display_name":"SETTING","sort":7},{"action":"institute_info","display_name":"INSTITUTE INFO","sort":8},{"action":"sign_out","display_name":"SIGN OUT","sort":9},{"action":"home","display_name":"HOME","sort":0}]}