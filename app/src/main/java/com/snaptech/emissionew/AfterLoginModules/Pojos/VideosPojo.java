package com.snaptech.emissionew.AfterLoginModules.Pojos;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by vikas on 01/11/16.
 */

public class VideosPojo {


    @SerializedName("data")
    private ArrayList<VideosResponsePojo> videosResponsePojoArrayList;

    public ArrayList<VideosResponsePojo> getVideosResponsePojoArrayList() {
        return videosResponsePojoArrayList;
    }

    public void setVideosResponsePojoArrayList(ArrayList<VideosResponsePojo> videosResponsePojoArrayList) {
        this.videosResponsePojoArrayList = videosResponsePojoArrayList;
    }
}
