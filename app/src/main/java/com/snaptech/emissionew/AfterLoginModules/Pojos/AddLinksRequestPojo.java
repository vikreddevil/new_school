package com.snaptech.emissionew.AfterLoginModules.Pojos;

import java.util.ArrayList;

/**
 * Created by vikas on 16/03/17.
 */

public class AddLinksRequestPojo {

    private String link_name;
    private String link_desc;
    private String link_url;
    private ArrayList<String> group_add_id;

    public String getLink_name() {
        return link_name;
    }

    public void setLink_name(String link_name) {
        this.link_name = link_name;
    }

    public String getLink_desc() {
        return link_desc;
    }

    public void setLink_desc(String link_desc) {
        this.link_desc = link_desc;
    }

    public String getLink_url() {
        return link_url;
    }

    public void setLink_url(String link_url) {
        this.link_url = link_url;
    }

    public ArrayList<String> getGroup_add_id() {
        return group_add_id;
    }

    public void setGroup_add_id(ArrayList<String> group_add_id) {
        this.group_add_id = group_add_id;
    }

    @Override
    public String toString() {
        return "AddLinksRequestPojo{" +
                "link_name='" + link_name + '\'' +
                ", link_desc='" + link_desc + '\'' +
                ", link_url='" + link_url + '\'' +
                ", group_add_id=" + group_add_id +
                '}';
    }
}
