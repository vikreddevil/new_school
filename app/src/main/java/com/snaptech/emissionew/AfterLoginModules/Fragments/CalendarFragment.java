package com.snaptech.emissionew.AfterLoginModules.Fragments;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.jmavarez.materialcalendar.CalendarView;
import com.jmavarez.materialcalendar.Interface.OnDateChangedListener;
import com.jmavarez.materialcalendar.Interface.OnMonthChangedListener;
import com.jmavarez.materialcalendar.Util.CalendarDay;
import com.snaptech.emissionew.AfterLoginModules.Adapters.CalendarListAdapter;
import com.snaptech.emissionew.AfterLoginModules.AddEventsActivity;
import com.snaptech.emissionew.AfterLoginModules.AddLinksActivity;
import com.snaptech.emissionew.AfterLoginModules.HomeScreenActivity;
import com.snaptech.emissionew.AfterLoginModules.Pojos.CalendarMonthwiseDataResponsePojo;
import com.snaptech.emissionew.AfterLoginModules.Pojos.CalendarMonthwisePojo;

import com.snaptech.emissionew.AfterLoginModules.TodaysEventActivity;
import com.snaptech.emissionew.R;
import com.snaptech.emissionew.Utils.Constants;
import com.snaptech.emissionew.Utils.Singleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import es.dmoral.toasty.Toasty;

import static com.snaptech.emissionew.Utils.Constants.BASE_URL;

//documents, awas yojna, loan part dad
/**
 * Created by vikas on 14/03/17.
 */

public class CalendarFragment extends Fragment{

   // private ImageView iv_add;
private SharedPreferences preferences;
    private static final String KEY_LAYOUT_MANAGER = "layoutManager";
    private RecyclerView mRecyclerView;
    private Activity mActivity;
    protected RecyclerView.LayoutManager mLayoutManager;
    private Singleton singleton;
    private String api_token;
    private TextView tv_no_events;
    private ImageView iv_add;
    private HashSet<CalendarDay> calendarDays;
    private ArrayList<CalendarMonthwiseDataResponsePojo> calendarMonthwiseDataResponsePojoArrayList;
    public static ArrayList<CalendarMonthwiseDataResponsePojo> calendarMonthwiseDataResponsePojoArrayList_selected_date=new ArrayList<>();
    private SharedPreferences sharedPreferences;
    private enum LayoutManagerType {
        GRID_LAYOUT_MANAGER,
        LINEAR_LAYOUT_MANAGER
    }
    CalendarView calendarView;
    protected CalendarFragment.LayoutManagerType mCurrentLayoutManagerType;

    CalendarListAdapter calendarListAdapter;

    //private CompactCalendarView compactCalendarView;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View view;
        view=inflater.inflate(R.layout.fragment_calendar,null);


        getRef(view);
        calendarMonthwiseDataResponsePojoArrayList_selected_date.clear();
        Toolbar toolbar = (Toolbar) mActivity.findViewById(R.id.toolbar);
        iv_add= (ImageView) toolbar.findViewById(R.id.iv_add);
        iv_add.setVisibility(View.VISIBLE);
        calendarView.setIndicatorsVisibility(true);

        calendarDays = new HashSet<>();
        mCurrentLayoutManagerType = CalendarFragment.LayoutManagerType.LINEAR_LAYOUT_MANAGER;

        if (savedInstanceState != null) {
            // Restore saved layout manager type.
            mCurrentLayoutManagerType = (CalendarFragment.LayoutManagerType) savedInstanceState
                    .getSerializable(KEY_LAYOUT_MANAGER);
        }
        setRecyclerViewLayoutManager(mCurrentLayoutManagerType);
        sharedPreferences=mActivity.getSharedPreferences(Constants.LOGIN_PREFS_NAME,Context.MODE_PRIVATE);
        api_token=sharedPreferences.getString(Constants.ACCESS_TOKEN_PREFS_KEY,null);

     //set Layout manager to recycler view


        Calendar calendar=Calendar.getInstance();
        ((HomeScreenActivity)mActivity).getSupportActionBar().setTitle(new SimpleDateFormat("MMMM").format(calendar.getTime())+" "+calendar.get(Calendar.YEAR));
        callMonthwiseEventsApi(mActivity,calendar.get(Calendar.MONTH)+"",calendar.get(Calendar.YEAR)+"");

        calendarView.setOnDateChangedListener(new OnDateChangedListener() {
            @Override
            public void onDateChanged(Date date) {


                System.out.println("Date selected is "+date);


                String str_date="",str_date_to_search="";
                Calendar calendar1=Calendar.getInstance();
                calendar1.setTime(date);
                String sMonth = "";
                int month=calendar1.get(Calendar.MONTH)+1;
                if (month < 10) {
                    sMonth = "0"+String.valueOf(month);
                } else {
                    sMonth = String.valueOf(month);
                }
                str_date=calendar1.get(Calendar.DAY_OF_MONTH)+"/"+sMonth+"/"+calendar1.get(Calendar.YEAR);

                str_date_to_search=calendar1.get(Calendar.YEAR)+"-"+sMonth+"-"+calendar1.get(Calendar.DAY_OF_MONTH);

                if(calendarMonthwiseDataResponsePojoArrayList!=null)
                for(CalendarMonthwiseDataResponsePojo c : calendarMonthwiseDataResponsePojoArrayList){

                    if(c.getStart_date()!=null&&c.getStart_date().contains(str_date_to_search)){

                        calendarMonthwiseDataResponsePojoArrayList_selected_date.add(c);
                    }
                }
                Intent intent=new Intent(mActivity, TodaysEventActivity.class);
                intent.putExtra("date",str_date);
                startActivity(intent);
            }
        });
        iv_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent=new Intent(mActivity, AddEventsActivity.class);
                startActivity(intent);

            }
        });
        calendarView.setOnMonthChangedListener(new OnMonthChangedListener() {
            @Override
            public void onMonthChanged(Date date) {

                Calendar calendar=Calendar.getInstance();
                calendar.setTime(date);
                ((HomeScreenActivity)mActivity).getSupportActionBar().setTitle(new SimpleDateFormat("MMMM").format(calendar.getTime())+" "+calendar.get(Calendar.YEAR));
                System.out.println("After month changed "+calendar.get(Calendar.MONTH));
                callMonthwiseEventsApi(mActivity,calendar.get(Calendar.MONTH)+"",calendar.get(Calendar.YEAR)+"");
            }
        });
        return view;
    }
    private void setRecyclerViewLayoutManager(CalendarFragment.LayoutManagerType layoutManagerType) {
        int scrollPosition = 0;

        // If a layout manager has already been set, get current scroll position.
        if (mRecyclerView.getLayoutManager() != null) {
            scrollPosition = ((LinearLayoutManager) mRecyclerView.getLayoutManager())
                    .findFirstCompletelyVisibleItemPosition();
        }

        switch (layoutManagerType) {
            case GRID_LAYOUT_MANAGER:
                mLayoutManager = new GridLayoutManager(mActivity, 2);
                mCurrentLayoutManagerType = CalendarFragment.LayoutManagerType.GRID_LAYOUT_MANAGER;
                break;
            case LINEAR_LAYOUT_MANAGER:
                mLayoutManager = new LinearLayoutManager(mActivity);
                mCurrentLayoutManagerType = CalendarFragment.LayoutManagerType.LINEAR_LAYOUT_MANAGER;
                break;
            default:
                mLayoutManager = new LinearLayoutManager(mActivity);
                mCurrentLayoutManagerType = CalendarFragment.LayoutManagerType.LINEAR_LAYOUT_MANAGER;
        }

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.scrollToPosition(scrollPosition);
    }
    private void getRef(View view){


        calendarMonthwiseDataResponsePojoArrayList=new ArrayList<>();
        calendarView=(CalendarView)view.findViewById(R.id.calendarView);
        tv_no_events=(TextView)view.findViewById(R.id.tv_no_events);
        singleton=Singleton.getInstance();
        calendarListAdapter=new CalendarListAdapter(calendarMonthwiseDataResponsePojoArrayList);
        mRecyclerView=(RecyclerView)view.findViewById(R.id.recycler_view);
        mRecyclerView.setAdapter(calendarListAdapter);
       // compactCalendarView=(CompactCalendarView)view.findViewById(R.id.compactcalendar_view);
        preferences=mActivity.getSharedPreferences(Constants.LOGIN_PREFS_NAME, Context.MODE_PRIVATE);
        String color_prmary=preferences.getString(Constants.PRIMARY_COLOR_PREFS_KEY,null);
        if(color_prmary!=null){
        char[] chars=color_prmary.toCharArray();
        String actual_color="";
        for (int i=0;i<color_prmary.length();i++){

            if(i!=0)
            actual_color=actual_color+chars[i];
        }
       // compactCalendarView.setCalendarBackgroundColor(Color.parseColor(color_prmary));


    }
    }
    @TargetApi(23)
    @Override public void onAttach(Context context) {
        //This method avoid to call super.onAttach(context) if I'm not using api 23 or more
        //if (Build.VERSION.SDK_INT >= 23) {
        super.onAttach(context);
        onAttachToContext(context);
        //}
    }

    /*
     * Deprecated on API 23
     * Use onAttachToContext instead
     */
    @SuppressWarnings("deprecation")
    @Override public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (Build.VERSION.SDK_INT < 23) {
            onAttachToContext(activity);
        }
    }
    protected void onAttachToContext(Context context){

        mActivity=(Activity)context;
    }
    private void callMonthwiseEventsApi(final Context context,String month,String year)
    {
//        swipeRefreshLayout.setRefreshing(true);

        Map<String, String> params= new HashMap<String, String>();
        params.put("year", year);
        params.put("month",month);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                BASE_URL+"api/event/monthwise", new JSONObject(params),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        if(response!=null)
                            Log.d("monthwise events", response.toString());


                        //Toast.makeText(mActivity,"Login Successful",Toast.LENGTH_SHORT).show();
                        //swipeRefreshLayout.setRefreshing(false);
                    }
                }, new Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
                mRecyclerView.setVisibility(View.GONE);
               // tv_no_events.setVisibility(View.VISIBLE);
               // tv_no_events.setText("Sorry, no events found");
//                if(swipeRefreshLayout.isRefreshing()){
//                    swipeRefreshLayout.setRefreshing(false);
//                }
                //Constants.notification_read=true;
                //VolleyLog.d("Error", "Error: " + error.getMessage());
               // swipeRefreshLayout.setRefreshing(false);
                String message="";
                if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }


                if(!message.equalsIgnoreCase(""))
                    Toasty.error(mActivity, message, Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {

                if(response!=null){
                    System.out.println("Response code from parse Network response is "+response.statusCode+" and data is "+response.toString()+" and data is "+response.data.toString());
                    try {
                        String jsonString = new String(response.data, "UTF-8");
                        System.out.println("Response from parse Network response is "+jsonString);

                        Gson gson = new Gson();
                        // Reader reader = new InputStreamReader();
                        final CalendarMonthwisePojo calendarMonthwisePojo = gson.fromJson(jsonString,CalendarMonthwisePojo.class); // (reader, Login_Response_Pojo.class);


                        if(response.statusCode==200){
                            // Constants.notification_read=true;

                            if(calendarMonthwisePojo.getCalendarMonthwiseDataResponsePojoArrayList().size()!=0) {

                                mActivity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        //Toast.makeText(mActivity,"Login Successful",Toast.LENGTH_SHORT).show();
                                        tv_no_events.setVisibility(View.GONE);
                                        mRecyclerView.setVisibility(View.VISIBLE);

                                        calendarMonthwiseDataResponsePojoArrayList.clear();
                                        calendarDays.clear();
                                        for (int i=0;i<calendarMonthwisePojo.getCalendarMonthwiseDataResponsePojoArrayList().size();i++){

                                            calendarMonthwiseDataResponsePojoArrayList.add(i,calendarMonthwisePojo.getCalendarMonthwiseDataResponsePojoArrayList().get(i));
                                            CalendarDay calendarDay = CalendarDay.from(new Date());
                                            System.out.println(calendarMonthwisePojo.getCalendarMonthwiseDataResponsePojoArrayList().get(i).getStart_date()+" date "+i);
                                            Date date1=null;
                                            try {
                                                date1=new SimpleDateFormat("yyyy-MM-dd").parse(calendarMonthwisePojo.getCalendarMonthwiseDataResponsePojoArrayList().get(i).getStart_date());
                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                            }
                                            if(!calendarMonthwisePojo.getCalendarMonthwiseDataResponsePojoArrayList().get(i).getStart_date().equals(calendarMonthwisePojo.getCalendarMonthwiseDataResponsePojoArrayList().get(i).getEnd_date())){

                                                ArrayList<Date> between_dates_array_list=getDates(calendarMonthwisePojo.getCalendarMonthwiseDataResponsePojoArrayList().get(i).getStart_date(),calendarMonthwisePojo.getCalendarMonthwiseDataResponsePojoArrayList().get(i).getEnd_date());
                                                for(int j=0;j<between_dates_array_list.size();j++) {
//                                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//                                                    String stringDate = sdf.format(between_dates_array_list.get(i));
                                                    Calendar calendar=Calendar.getInstance();
                                                    calendar.setTime(between_dates_array_list.get(j));
                                                    CalendarDay day = CalendarDay.from(calendar.get(Calendar.DAY_OF_MONTH), calendar.get(Calendar.MONTH)+1, calendar.get(Calendar.YEAR));
                                                    calendarDays.add(day);

                                                }
                                            }
                                            Calendar calendar=Calendar.getInstance();
                                            calendar.setTime(date1);
                                            CalendarDay day = CalendarDay.from(calendar.get(Calendar.DAY_OF_MONTH), calendar.get(Calendar.MONTH)+1, calendar.get(Calendar.YEAR));
                                            System.out.println("Date mapped is "+calendar.get(Calendar.DAY_OF_MONTH)+"-"+calendar.get(Calendar.MONTH)+"-"+calendar.get(Calendar.YEAR));
                                            calendarDays.add(day);
                                        }
                                        calendarListAdapter.notifyDataSetChanged();

                                        calendarView.addEvents(calendarDays);
                                        //notificationPojoList.clear();
//                                        for (int i = 0; i < notificationPojo.getNotificationResponsePojoArrayList().size(); i++) {
//                                         ''   notificationPojoList.add(i, notificationPojo.getNotificationResponsePojoArrayList().get(i));
//                                        }

//                                        notificationAdapter.notifyDataSetChanged();
//                                        if (swipeRefreshLayout.isRefreshing()) {
//                                            swipeRefreshLayout.setRefreshing(false);
//                                        }
                                        //callNotificationReadApi(context);
                                    }
                                });

                            }
                            else{

                                ((HomeScreenActivity) context).runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {

                                        mRecyclerView.setVisibility(View.GONE);
                                        tv_no_events.setVisibility(View.VISIBLE);
//                                        tv_no_events.setText("Sorry, no notifications found");
                                    }
                                });

                            }
                        }
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
                return super.parseNetworkResponse(response);
            }


            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {

                NetworkResponse networkResponse=volleyError.networkResponse;
                if(networkResponse!=null){

                    String jsonString="";
                    try {
                        jsonString = new String(networkResponse.data, "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                    System.out.println("Network response is "+networkResponse.statusCode+jsonString);
                    try {
                        JSONObject jsonObject=new JSONObject(jsonString);
                        JSONArray jsonArray=jsonObject.getJSONArray("error");
                        final String errorString=jsonArray.getString(0);
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toasty.error(mActivity,errorString,Toast.LENGTH_SHORT).show();
                            }
                        });


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
                return super.parseNetworkError(volleyError);
            }

            /**
             * Passing some request headers
             * */

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Authorization",api_token);
                headers.put("Client-Id","1");
                return headers;
            }



        };

        // Adding request to request queue


        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        singleton.addToRequestQueue(jsonObjReq, "Calendar month wise api calling");

    }
    private static ArrayList<Date> getDates(String dateString1, String dateString2)
    {
        ArrayList<Date> dates = new ArrayList<Date>();
        DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");

        Date date1 = null;
        Date date2 = null;

        try {
            date1 = df1 .parse(dateString1);
            date2 = df1 .parse(dateString2);
            System.out.println("Inside start date "+date1.getTime()+" and outside is "+date2.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);


        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);

        while(!cal1.after(cal2))
        {
            dates.add(cal1.getTime());
            cal1.add(Calendar.DATE, 1);
        }
        return dates;
    }
}
