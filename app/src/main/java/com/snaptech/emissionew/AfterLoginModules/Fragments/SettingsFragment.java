package com.snaptech.emissionew.AfterLoginModules.Fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;

import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.snaptech.emissionew.AfterLoginModules.Pojos.UserDetailsDataResponsePojo;
import com.snaptech.emissionew.AfterLoginModules.Pojos.UserDetailsPojo;
import com.snaptech.emissionew.R;
import com.snaptech.emissionew.Utils.AppHelper;
import com.snaptech.emissionew.Utils.Constants;
import com.snaptech.emissionew.Utils.MultipartRequest;
import com.snaptech.emissionew.Utils.Singleton;
import com.snaptech.emissionew.Utils.VolleyMultipartRequest;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import es.dmoral.toasty.Toasty;

import static android.content.ContentValues.TAG;
import static android.content.Context.MODE_PRIVATE;
import static com.snaptech.emissionew.Utils.Constants.BASE_URL;


/**
 * Created by vikas on 21/11/16.
 */

// SettingsFragment is for changing user profile settings.

public class SettingsFragment extends Fragment {

    private Activity mActivity;
    private SharedPreferences.Editor editor;
    private EditText et_email;
    private EditText et_mobile;

    private EditText et_fname;
    private EditText et_lname;
    private EditText et_mname;
    private EditText et_dob;
    private Singleton singleton;
    private SharedPreferences prefs;
    private String api_token;
    private String user_id,email,mobile,gender,fname,lname,mname,dob;
    private DatePickerDialog datePickerDialog;
    private Button btn_update;
    private ProgressDialog progressDialog;
    private int year,month,day;
    private String selectDateString;
    private ImageView img_profile;
    private static int PICK_IMAGE_REQUEST=1;
    private Bitmap bitmap;
    private String image_string="";
    private String image_url="";
    private boolean flagpermission_phone=true;

    private String message="";
    private Uri mCropImageUri=null;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=null;

        view=inflater.inflate(R.layout.fragment_settings,null);
        getRef(view);

        callUserDetailsApi(mActivity);
        //checking for external storage permission

        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.M){

            if(!checkWriteExternal()) {
                flagpermission_phone=false;
                ContextCompat.checkSelfPermission(mActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }
            else
                flagpermission_phone=true;

        }
        if(Build.VERSION.SDK_INT<Build.VERSION_CODES.M){

            flagpermission_phone=true;
        }




        //loading profile image if it exists


        setData();


        //on click of imageview calling image pick image chooser intent

        img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(Build.VERSION.SDK_INT<Build.VERSION_CODES.M){

                    flagpermission_phone=true;
                }

                if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.M){

                    if(!checkWriteExternal()) {
                        flagpermission_phone=false;
                        ContextCompat.checkSelfPermission(mActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
                    }
                    else
                        flagpermission_phone=true;

                }
                if(flagpermission_phone){
                    //showFileChooser();


                    //Intent.createChooser(CropImage.getPickImageChooserIntent(mActivity),"Pick");
                    startActivityForResult(CropImage.getPickImageChooserIntent(mActivity),200);
                    //CropImage.startPickImageActivity(mActivity);
                }
                else{

                    Toasty.error(mActivity, "Please give External Storage permission", Toast.LENGTH_LONG).show();
                }

            }
        });


        //setting meal type based on users choice




        et_email.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;


                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (et_email.getRight() - et_email.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here

                        et_email.setText("");
                        et_email.requestFocus();
                        return true;
                    }
                }
                return false;
            }
        });
        et_fname.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;


                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (et_fname.getRight() - et_fname.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here

                        et_fname.setText("");
                        et_fname.requestFocus();
                        return true;
                    }
                }
                return false;
            }
        });
        et_mname.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;


                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (et_mname.getRight() - et_mname.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here

                        et_mname.setText("");
                        et_mname.requestFocus();
                        return true;
                    }
                }
                return false;
            }
        });
        et_lname.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;


                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (et_lname.getRight() - et_lname.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here

                        et_lname.setText("");
                        et_lname.requestFocus();
                        return true;
                    }
                }
                return false;
            }
        });
        et_dob.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;


                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (et_dob.getRight() - et_dob.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here

                        et_dob.setText("");
                        et_dob.requestFocus();
                        return true;
                    }
                }
                return false;
            }
        });
        et_mobile.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {

                final int DRAWABLE_RIGHT = 2;


                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (et_mobile.getRight() - et_mobile.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here

                        et_mobile.setText("");
                        et_mobile.requestFocus();
                        return true;
                    }
                }
                return false;
            }
        });


        btn_update.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {
                email = et_email.getText().toString();
                mobile = et_mobile.getText().toString();

                dob=et_dob.getText().toString();
                fname=et_fname.getText().toString();
                lname=et_lname.getText().toString();
                mname=et_mname.getText().toString();

                boolean flag = validate(mobile, email, mActivity);

                if (flag) {
                    // call settings api

                    new AlertDialog.Builder(mActivity)
                            .setTitle("Update")
                            .setMessage("Are you sure you want to update your details ?")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                    //callSetingsApi(mActivity);
                                    callMultipart();
                                   // callMultipartRequestNew();
                                }
                            })
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing
                                }
                            })
                            .setIcon(R.mipmap.ic_launcher)
                            .show();
                }
            }
        });

        if(progressDialog.isShowing())
            progressDialog.dismiss();
        return view;
    }
    private void getRef(View view){

        progressDialog=new ProgressDialog(mActivity);
        progressDialog.setMessage("Loading");
        progressDialog.show();


        img_profile=(ImageView)view.findViewById(R.id.img_profile);
        et_email= (EditText) view.findViewById(R.id.et_email_settings);
        et_mobile=(EditText)view.findViewById(R.id.et_mobile_settings);

        et_dob=(EditText)view.findViewById(R.id.et_dob_settings);
        et_fname=(EditText)view.findViewById(R.id.et_first_name_settings);
        et_lname=(EditText)view.findViewById(R.id.et_last_name_settings);
        btn_update=(Button)view.findViewById(R.id.btn_update_settings);
        et_mname=(EditText)view.findViewById(R.id.et_middle_name_settings);

        prefs= mActivity.getSharedPreferences(Constants.LOGIN_PREFS_NAME, MODE_PRIVATE);

        singleton=Singleton.getInstance();
//        et_password=(EditText)view.findViewById(R.id.et_password_settings);
//        et_confirm_password=(EditText)view.findViewById(R.id.et_confirmpassword_settings);

        //String initialization


        email=prefs.getString(Constants.EMAIL_ID_PREFS_KEY,null);
        mobile=prefs.getString(Constants.MOBILE_PREFS_KEY,null);
        gender=prefs.getString(Constants.GENDER_PREFS_KEY,null);
        dob=prefs.getString(Constants.DOB_PREFS_KEY,null);
        fname=prefs.getString(Constants.FIRST_NAME_PREFS_KEY,null);
        lname=prefs.getString(Constants.LAST_NAME_PREFS_KEY,null);
        mname=prefs.getString(Constants.MIDDLE_NAME_PREFS_KEY,null);
        api_token=prefs.getString(Constants.ACCESS_TOKEN_PREFS_KEY,null);
        user_id=prefs.getString(Constants.USER_ID_PREFS_KEY,null);
        image_url=prefs.getString(Constants.PROFILE_IMAGE_URL,null);




    }
    private void setData()
    {

        if(gender!=null) {
            if (gender.trim().equalsIgnoreCase("female")) {
                img_profile.setBackground(ContextCompat.getDrawable(mActivity, R.drawable.femaleavatar));
            } else if (gender.trim().equalsIgnoreCase("male")) {

                img_profile.setBackground(ContextCompat.getDrawable(mActivity, R.drawable.maleavatar));

            } else {
                img_profile.setBackground(ContextCompat.getDrawable(mActivity, R.drawable.ic_others));
            }
        }
        else{
            img_profile.setBackground(ContextCompat.getDrawable(mActivity, R.drawable.ic_others));

        }

        et_email.setText(email);
        et_mobile.setText(mobile);



        if(image_url!=null)
        {
            if(!image_url.trim().equals("")){

                Picasso.with(mActivity).load(image_url).fit().centerCrop()
                        .memoryPolicy(MemoryPolicy.NO_CACHE)
                        .error(R.drawable.maleavatar)
                        .into(img_profile);
            }
        }


    }

    //validation

    private boolean validate(String mobile,String email,Context context){


        if(email.trim().equalsIgnoreCase("")){

            Toasty.error(context,"Please enter email id",Toast.LENGTH_SHORT).show();
            et_email.requestFocus();
            return false;
        }
        if(!isValidEmail(email)){
            Toasty.error(context,"Please enter valid email id",Toast.LENGTH_SHORT).show();
            et_email.requestFocus();
            return false;

        }
        if(mobile.trim().equalsIgnoreCase("")){

            Toasty.error(context,"Please enter mobile number",Toast.LENGTH_SHORT).show();
            et_mobile.requestFocus();
            return false;
        }
        if(!isValidMobile(mobile)){

            Toasty.error(context,"Please enter a 10 digit mobile number",Toast.LENGTH_SHORT).show();
            et_mobile.requestFocus();
            return false;
        }

//        if(passport_no.trim().equalsIgnoreCase("")){
//            Toast.makeText(context,"Please enter passport no",Toast.LENGTH_SHORT).show();
//            et_passport_no.requestFocus();
//            return false;
//        }
//        if(passport_issue_date.trim().equalsIgnoreCase("")){
//
//            Toast.makeText(context,"Please enter issue date",Toast.LENGTH_SHORT).show();
//            et_passport_issue_date.requestFocus();
//            return false;
//        }
//        if(passport_expiry_date.trim().equalsIgnoreCase("")){
//            Toast.makeText(context,"Please enter expiry date",Toast.LENGTH_SHORT).show();
//            et_passport_issue_date.requestFocus();
//            return false;
//
//        }
//        if(passport_place_of_issue.trim().equalsIgnoreCase("")){
//            Toast.makeText(context,"Please enter passport place of issue",Toast.LENGTH_SHORT).show();
//            et_passport_issue_date.requestFocus();
//            return false;
//
//        }

        return true;
    }
    private boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }
    private boolean isValidMobile(String mobile){

        if(mobile.length()!=10){
            return false;
        }
        return true;
    }

    //settings api called
    private void callSetingsApi(final Context context)
    {


        final ProgressDialog pDialog = new ProgressDialog(context,R.style.MyTheme);
        pDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        pDialog.setCancelable(false);
        pDialog.show();


        Map<String, String> params= new HashMap<String, String>();

        params.put("user_id", user_id);
        params.put("email_id", email);
        params.put("mobile", mobile);

        params.put("middle_name",mname);
        params.put("first_name",fname);
        params.put("last_name",lname);
        params.put("gender",gender);
        params.put("profile_image",image_string);
        params.put("dob",dob);


        System.out.println("image sent is "+image_string);



        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                BASE_URL+"api/user/update", new JSONObject(params),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        if(response!=null)
                            Log.d("Response", response.toString());


                        //Toast.makeText(mActivity,"Login Successful",Toast.LENGTH_SHORT).show();
                        pDialog.dismiss();
                    }
                }, new Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {

                //VolleyLog.d("Error", "Error: " + error.getMessage());
                pDialog.dismiss();

                if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }

                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(message!=null){
                            if(!message.trim().equals(""))
                                Toasty.error(mActivity, message, Toast.LENGTH_SHORT).show();

                        }
                    }
                });


//
//                if(networkResponse!=null) {
//
//                    if (networkResponse.statusCode == 404) {
//                        Toast.makeText(mActivity, "Something went wrong, Please try again", Toast.LENGTH_SHORT).show();
//                    }
//                }

            }
        }) {

            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {

                if(response!=null) {
                    System.out.println("Response code from parse Network response is " + response.statusCode + " and data is " + response.toString() + " and data is " + response.data.toString());
                    pDialog.dismiss();
                    try {
                        final String jsonString = new String(response.data, "UTF-8");
                        System.out.println("Response from parse Network response is " + jsonString);


                        // Reader reader = new InputStreamReader();
                        //final LinksPojo linksPojo = gson.fromJson(jsonString,LinksPojo.class); // (reader, Login_Response_Pojo.class);

                        //  System.out.println("Response pojo is "+linksPojo.getLinksDataResponsePojo().get(0).getUrl());
                        if (response.statusCode == 200) {


                            try {
                                JSONObject jsonObject = new JSONObject(jsonString);
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                if (jsonArray.length() != 0) {
                                    image_url = jsonArray.getJSONObject(0).getString("image");
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            editor = mActivity.getSharedPreferences(Constants.LOGIN_PREFS_NAME, MODE_PRIVATE).edit();
                            //editor = loginSharedPrefs.edit();
                            editor.putString(Constants.EMAIL_ID_PREFS_KEY, email);
                            editor.putString(Constants.MOBILE_PREFS_KEY, mobile);
                            editor.putString(Constants.PROFILE_IMAGE_URL,image_url);

                            if (image_url != null) {
                                if (!image_url.trim().equals("")) {

                                    editor.putString(Constants.PROFILE_IMAGE_URL, image_url);
                                }
                            }
                            editor.commit();
                            mActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    Toasty.success(mActivity, "Profile updated successfully", Toast.LENGTH_SHORT).show();
                                    //loginSharedPrefs = context.getSharedPreferences(Constants.LOGIN_PREFS_NAME, MODE_PRIVATE);


                                }
                            });


                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                return super.parseNetworkResponse(response);
            }


            @Override

            protected VolleyError parseNetworkError(VolleyError volleyError) {

                pDialog.dismiss();
                NetworkResponse networkResponse=volleyError.networkResponse;
                if(networkResponse!=null){

                    String jsonString="";
                    try {
                        jsonString = new String(networkResponse.data, "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                    System.out.println("Network response is "+networkResponse.statusCode+jsonString);
                    try {
                        JSONObject jsonObject=new JSONObject(jsonString);
                        final String errorString=jsonObject.getString("message");
                        //final String errorString=jsonArray.getString(0);
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toasty.error(mActivity,errorString,Toast.LENGTH_SHORT).show();
                            }
                        });


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
                return super.parseNetworkError(volleyError);
            }

            /**
             * Passing some request headers
             * */

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Authorization",api_token);
                headers.put("Client-Id","1");
                return headers;
            }



        };


        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        singleton.addToRequestQueue(jsonObjReq, "Settings with profile image upload api calling");

    }
    private void callUserDetailsApi(final Context context)
    {


        final ProgressDialog pDialog = new ProgressDialog(context,R.style.MyTheme);
        pDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        pDialog.setCancelable(false);
        pDialog.show();


        Map<String, String> params= new HashMap<String, String>();

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                BASE_URL+"api/user/parent/details", new JSONObject(params),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        if(response!=null)
                            Log.d("user details", response.toString());


                        //Toast.makeText(mActivity,"Login Successful",Toast.LENGTH_SHORT).show();
                        pDialog.dismiss();
                    }
                }, new Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {

                //VolleyLog.d("Error", "Error: " + error.getMessage());
                pDialog.dismiss();

                if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }

                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(message!=null){
                            if(!message.trim().equals(""))
                                Toasty.error(mActivity, message, Toast.LENGTH_SHORT).show();

                        }
                    }
                });


//
//                if(networkResponse!=null) {
//
//                    if (networkResponse.statusCode == 404) {
//                        Toast.makeText(mActivity, "Something went wrong, Please try again", Toast.LENGTH_SHORT).show();
//                    }
//                }

            }
        }) {

            @Override
            protected Response<JSONObject> parseNetworkResponse(final NetworkResponse response) {

                if (response != null) {
                    System.out.println("Response code from parse Network response is " + response.statusCode + " and data is " + response.toString() + " and data is " + response.data.toString());
                    pDialog.dismiss();
                    try {
                        final String jsonString = new String(response.data, "UTF-8");
                        System.out.println("Response from parse Network response is " + jsonString);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    // Reader reader = new InputStreamReader();
                    //final LinksPojo linksPojo = gson.fromJson(jsonString,LinksPojo.class); // (reader, Login_Response_Pojo.class);

                    //  System.out.println("Response pojo is "+linksPojo.getLinksDataResponsePojo().get(0).getUrl());
                    if (response.statusCode == 200) {


                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                try {

                                    final String jsonString = new String(response.data, "UTF-8");

                                    Gson gson=new Gson();
                                    UserDetailsPojo userDetailsPojo=gson.fromJson(jsonString, UserDetailsPojo.class);

                                    if(userDetailsPojo!=null) {
                                        if (userDetailsPojo.getUserDetailsDataResponsePojoArrayList().size() != 0) {
                                            UserDetailsDataResponsePojo userDetailsDataResponsePojo = userDetailsPojo.getUserDetailsDataResponsePojoArrayList().get(0);

                                            email=userDetailsDataResponsePojo.getEmail();
                                            mobile=userDetailsDataResponsePojo.getMobile();


                                            fname=userDetailsDataResponsePojo.getParent_first_name();
                                            lname=userDetailsDataResponsePojo.getParent_last_name();
                                            dob=userDetailsDataResponsePojo.getStudent_dob();
                                            user_id=userDetailsDataResponsePojo.getUser_id()+"";

                                            gender=userDetailsDataResponsePojo.getStudent_gender();





                                            setData();
                                        }
                                    }


                                }
                                catch (Exception e){
                                    e.printStackTrace();
                                }
                            }
                        });

                        //{"data":[{"document_type_id":1,"insurance_nominee_name":null,"meal_type":"veg","is_domestic":0,"emergency_contact_no":null,"image":null,"place_of_issue":null,"passport_no":null,"trip_id":"ABBO22-02","document_type_no":null,"id":8,"first_name":"AVI","insurance_nominee_mob_no":null,"itenary_no":1,"passport_expiry":null,"dob":"1994-08-07","document_type_name":"Passport","last_name":"JAIN","gender":"male","tour_id":1,"email_id":"avi@snaptech.in","passport_issue":null,"mobile":"9819749732"}],"message":"success"}
//
//                            try {
//                                JSONObject jsonObject = new JSONObject(jsonString);
//                                JSONArray jsonArray = jsonObject.getJSONArray("data");
//                                if (jsonArray.length() != 0) {
//                                    image_url = jsonArray.getJSONObject(0).getString("image");
//                                    is_domestic = jsonArray.getJSONObject(0).getString("is_domestic");
//                                    if (jsonArray.getJSONObject(0).getString("trip_id") != null) {
//
//                                        if (!jsonArray.getJSONObject(0).getString("trip_id").trim().equals("")) {
//
//                                            trip_id = jsonArray.getJSONObject(0).getString("trip_id");
//
//                                        }
//                                    }
//
//                                    tour_id = jsonArray.getJSONObject(0).getInt("tour_id") + "";
//
//                                }
//
//
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }


                    }
                }
                return super.parseNetworkResponse(response);
            }

            @Override

            protected VolleyError parseNetworkError(VolleyError volleyError) {

                pDialog.dismiss();
                NetworkResponse networkResponse=volleyError.networkResponse;
                if(networkResponse!=null){

                    String jsonString="";
                    try {
                        jsonString = new String(networkResponse.data, "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                    System.out.println("Network response is "+networkResponse.statusCode+jsonString);
                    try {
                        JSONObject jsonObject=new JSONObject(jsonString);
                        final String errorString=jsonObject.getString("message");
                        //final String errorString=jsonArray.getString(0);
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toasty.error(mActivity,errorString,Toast.LENGTH_SHORT).show();
                            }
                        });


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
                return super.parseNetworkError(volleyError);
            }

            /**
             * Passing some request headers
             * */

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Authorization",api_token);
                headers.put("Client-Id","1");
                return headers;
            }



        };


        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        singleton.addToRequestQueue(jsonObjReq, "User details api calling");

    }
    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }


    //
    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0,source.getWidth() , source.getHeight(),
                matrix, true);
    }
    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 50, baos);       //this is commented because of double compress
        byte[] imageBytes = baos.toByteArray();
        return Base64.encodeToString(imageBytes, Base64.DEFAULT);
    }
    private boolean checkWriteExternal()
    {
        if (Build.VERSION.SDK_INT >= 23) {
            if (mActivity.checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG,"Permission is granted");
                return true;
            } else {

                Log.v(TAG,"Permission is revoked");
                ActivityCompat.requestPermissions(mActivity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG,"Permission is granted");
            return true;
        }
    }
    @Override
    @SuppressLint("NewApi")
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // handle result of pick image chooser

//        Bundle extras = data.getExtras();
//        if(extras != null ) {
//            Bitmap photo = extras.getParcelable("data");
//            ByteArrayOutputStream stream = new ByteArrayOutputStream();
//            photo.compress(Bitmap.CompressFormat.JPEG, 75, stream);
//          //  / / The stream to write to a file or directly using the
//        }
        System.out.println("Activity result called request code "+requestCode+" result code "+resultCode);
        Context context=mActivity;
        // Uri imageUri = data.getData();


        //System.out.println("image uri is "+imageUri);

        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri2 = CropImage.getPickImageResultUri(context, data);
            mCropImageUri = imageUri2;

            System.out.println("inside first if");
            // For API >= 23 we need to check specifically that we have permissions to read external storage.
            if (CropImage.isReadExternalStoragePermissionsRequired(context, imageUri2)) {
                if(mCropImageUri!=null)
                {


                    cropImage();


                }
                // request permissions and handle the result in onRequestPermissionsResult()
                System.out.println("inside second if");

                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},   CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE);
            } else {
                cropImage();
                // no permissions required or already grunted, can start crop image activity
                //startCropImageActivity(imageUri2);
            }
        }
        else if (requestCode == 100 && resultCode == Activity.RESULT_OK)
        {
            System.out.println("inside else if");
            try {
                mCropImageUri = data.getData();
                // System.out.println("data returned is "+data.getStringExtra("data"));
                if(mCropImageUri!=null) {
                    bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), mCropImageUri);
                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 30, out);
                    Bitmap decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));
                    img_profile.setImageBitmap(decoded);
                    byte[] imageBytes = out.toByteArray();
                    String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
                    image_string=encodedImage;
                    System.out.println("Image string updated");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }


        }
    }

    private void startCropImageActivity(Uri imageUri) {
        System.out.println("Start crop activity called");
        CropImage.activity(imageUri)
                .start(getContext(),this);
    }
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE) {
            if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // required permissions granted, start crop image activity
                startCropImageActivity(mCropImageUri);
            } else {
                Toasty.error(mActivity, "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
            }
        }
    }
    @SuppressLint("NewApi")
    public void onSelectImageClick(View view) {
        if (CropImage.isExplicitCameraPermissionRequired(mActivity)) {
            requestPermissions(new String[]{Manifest.permission.CAMERA}, CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE);
        } else {
            CropImage.startPickImageActivity(mActivity);
        }
    }
    private void cropImage() {

        final File imagePath = new File(mActivity.getFilesDir(), "images");
        final File imageFile = new File(imagePath, "sample.jpg");
        String IMAGE_FILE_LOCATION = "file:///sdcard/temp.jpg";//temp file
        Uri tmpUri = Uri.parse(IMAGE_FILE_LOCATION);
        // Provider authority string must match the one declared in AndroidManifest.xml
        // final Uri providedUri = FileProvider.getUriForFile(
        //       mActivity, "com.example.test.fileprovider", mCropImageUri.toString());
        // Use existing crop activity.
        try {
            Intent intent = new Intent("com.android.camera.action.CROP");
            intent.setDataAndType(mCropImageUri, "image/*");
            // Specify image size
            intent.putExtra("outputX", 150);
            intent.putExtra("outputY", 150);

            // Specify aspect ratio, 1:1
            intent.putExtra("aspectX", 1);
            intent.putExtra("aspectY", 1);
            intent.putExtra("scale", true);
            intent.putExtra("return-data", true);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, tmpUri);
            intent.putExtra("noFaceDetection", true);
            // REQUEST_CODE_CROP_PHOTO is an integer tag you defined to
            // identify the activity in onActivityResult() when it returns
            startActivityForResult(intent, 100);
        }catch (Exception e){

            try {
                bitmap = MediaStore.Images.Media.getBitmap(mActivity.getContentResolver(), mCropImageUri);
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 30, out);
                Bitmap decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));
                img_profile.setImageBitmap(decoded);
                byte[] imageBytes = out.toByteArray();
                image_string= Base64.encodeToString(imageBytes, Base64.DEFAULT);
                System.out.println("Image string updated");
                e.printStackTrace();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            catch (Exception e2){

                e2.printStackTrace();
            }

        }
    }
    @TargetApi(23)
    @Override public void onAttach(Context context) {
        //This method avoid to call super.onAttach(context) if I'm not using api 23 or more
        //if (Build.VERSION.SDK_INT >= 23) {
        super.onAttach(context);
        onAttachToContext(context);
        //}
    }

    /*
     * Deprecated on API 23
     * Use onAttachToContext instead
     */
    @SuppressWarnings("deprecation")
    @Override public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (Build.VERSION.SDK_INT < 23) {
            onAttachToContext(activity);
        }
    }
    protected void onAttachToContext(Context context){

        mActivity=(Activity)context;
    }
    private void callMultipart(){
        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, BASE_URL+"api/user/update", new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                String resultResponse = new String(response.data);
                // parse success output
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }

        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", user_id);
                params.put("email_id", email);
                params.put("mobile", mobile);
                params.put("middle_name",mname);
                params.put("first_name",fname);
                params.put("last_name",lname);
                params.put("gender",gender);
//                params.put("profile_image",image_string);
                params.put("dob",dob);
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                // file name could found file base or direct access from real path
                // for now just get bitmap data from ImageView
                params.put("profile_image", new DataPart("file_avatar.jpeg", AppHelper.getFileDataFromDrawable(mActivity, img_profile.getDrawable()), "image/jpeg"));
                //params.put("cover", new DataPart("file_cover.jpg", AppHelper.getFileDataFromDrawable(mActivity, img_profile.getDrawable()), "image/jpeg"));

                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "multipart/form-data");
                headers.put("Authorization",api_token);
                headers.put("Client-Id","1");
//                headers.put("form-data","multipart");
                return headers;
            }
        };



        singleton.addToRequestQueue(multipartRequest,"multipart request");

    }
//    private void callMultipartRequestNew(){
//
//        img_profile.buildDrawingCache();
//        Bitmap bmap = img_profile.getDrawingCache();
//
//
//        File f = new File(mActivity.getCacheDir(), "test");
//        ByteArrayOutputStream bos = new ByteArrayOutputStream();
//        bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
//        byte[] bitmapdata = bos.toByteArray();
//
////write the bytes in file
//        FileOutputStream fos = null;
//        try {
//            fos = new FileOutputStream(f);
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
//        try {
//            fos.write(bitmapdata);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        try {
//            fos.flush();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        try {
//            fos.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        try {
//            f.createNewFile();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        MultipartRequest multipartRequest=new MultipartRequest(BASE_URL + "api/user/update", new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//
//                System.out.println("error is "+error.networkResponse.statusCode+" "+error.networkResponse.data);
//            }
//
//        }, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//
//            }
//        }
//                ,f,"",api_token);
//
//        singleton.addToRequestQueue(multipartRequest,"new multi part request");
//    }
}
