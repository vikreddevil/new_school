package com.snaptech.emissionew.AfterLoginModules.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;


import com.snaptech.emissionew.AfterLoginModules.DocumentsFolderActivity;
import com.snaptech.emissionew.AfterLoginModules.PDFWebViewer;
import com.snaptech.emissionew.AfterLoginModules.Pojos.DocumentsFolderResponsePojo;
import com.snaptech.emissionew.AfterLoginModules.Pojos.DocumentsResponsePojo;
import com.snaptech.emissionew.R;

import java.text.DateFormat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


/**
 * Created by vikas on 01/11/16.
 */

// This adapter is to Show the list of Documents

public class DocumentsAdapter extends RecyclerView.Adapter<DocumentsAdapter.CustomViewHolder> {

    public static ArrayList<DocumentsResponsePojo> DocumentsResponsePjoList;
    public  static ArrayList<DocumentsFolderResponsePojo> documentsFolderResponsePojoArrayList=new ArrayList<>();
    private Context mContext;


    public DocumentsAdapter(Context context, ArrayList<DocumentsResponsePojo> DocumentsResponsePjoList) {
        this.DocumentsResponsePjoList = DocumentsResponsePjoList;
        this.mContext = context;
    }

    @Override
    public DocumentsAdapter.CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.documents_row, viewGroup,false);
        DocumentsAdapter.CustomViewHolder viewHolder = new DocumentsAdapter.CustomViewHolder(view);
        viewHolder.cardView.setPreventCornerOverlap(false);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CustomViewHolder customViewHolder, final int i) {

        customViewHolder.tv_Title.setText(DocumentsResponsePjoList.get(i).getName());
        customViewHolder.tv_description.setText(DocumentsResponsePjoList.get(i).getDescription());
        DateFormat fromFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        fromFormat.setLenient(false);
        DateFormat toFormat = new SimpleDateFormat("EEE dd-MM-yyyy");
        toFormat.setLenient(false);
        String dateStr = DocumentsResponsePjoList.get(i).getCreated_at();

        final String folder_name=DocumentsResponsePjoList.get(i).getFolder_name();

        if(folder_name!=null){

            customViewHolder.tv_Title.setText(DocumentsResponsePjoList.get(i).getFolder_name());
//            customViewHolder.tv_description.setText(DocumentsResponsePjoList.get(i).getF());

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                customViewHolder.ib_icon.setBackground(ContextCompat.getDrawable(mContext, R.drawable.ic_folder));
            }

        }
        else{

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                customViewHolder.ib_icon.setBackground(ContextCompat.getDrawable(mContext, R.drawable.pdf_icon));
            }
        }
//temporary comment


        if(folder_name==null) {
            Date date = null;
            try {
                date = fromFormat.parse(dateStr);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            System.out.println(toFormat.format(date));
            customViewHolder.tv_date.setText(toFormat.format(date));
        }

        //This is to call the PDF webview on click of a list item
//        customViewHolder.cardView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                if(folder_name!=null) {
//
//
//                    //                    Intent intent=new Intent(mContext, DocumentsFolderActivity.class);
////                    intent.putExtra("folder_name",folder_name);
////                    mContext.startActivity(intent);
//                }
//            }
//        });

    }

    @Override
    public int getItemCount() {
        return (DocumentsResponsePjoList.size());
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {

        protected TextView tv_Title;
        private CardView cardView;
        protected TextView tv_description;
        protected TextView tv_date;
        private ImageButton ib_icon;

        public CustomViewHolder(View view) {
            super(view);

            this.tv_Title=(TextView)view.findViewById(R.id.tv_title_documents);
            this.cardView=(CardView)view.findViewById(R.id.cardview_notification_row);
            this.tv_description=(TextView)view.findViewById(R.id.tv_description_documents);
            this.tv_date=(TextView)view.findViewById(R.id.tv_date_documents);
            this.ib_icon=(ImageButton)view.findViewById(R.id.ib_documents_icon);

        }
    }

}
