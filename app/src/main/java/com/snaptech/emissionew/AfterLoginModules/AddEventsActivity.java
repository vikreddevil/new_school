package com.snaptech.emissionew.AfterLoginModules;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.snaptech.emissionew.AfterLoginModules.Pojos.AddEventsRequestPojo;
import com.snaptech.emissionew.R;
import com.snaptech.emissionew.Utils.Constants;
import com.snaptech.emissionew.Utils.Singleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import es.dmoral.toasty.Toasty;

import static com.snaptech.emissionew.Utils.Constants.BASE_URL;
import static com.snaptech.emissionew.Utils.Constants.LOGIN_PREFS_NAME;

public class AddEventsActivity extends AppCompatActivity {


    private EditText et_title;
    private Singleton singleton;
    private TextView tv_location;
    private EditText et_description;
    private EditText et_type;
    private EditText et_start_date;
    private DatePickerDialog datePickerDialog;
    private TimePickerDialog timePickerDialog;
    private EditText et_end_date;
    private EditText et_start_time;
    private EditText et_end_time;
    private EditText et_location;
    private EditText et_groups;
    private Button btn_add_event;
    private int year,month,day,hours,minutes;
    private String date_String;
    private String time_String;
    private String end_time_String;
    private String end_date_String;
    private SharedPreferences preferences;
    private String api_token;
    private AddEventsRequestPojo addEventsRequestPojo=new AddEventsRequestPojo();
    private ArrayList<String> group_id_array;
    private String description,title,type_name="",type_id="",type_code="",location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_events);

        getRef();



        et_groups.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(AddEventsActivity.this,GroupsPopupActivity.class);
                startActivityForResult(intent,1);
            }
        });
        et_groups.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;


                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (et_groups.getRight() - et_groups.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here

                        et_groups.setText("");
                        et_groups.requestFocus();
                        return true;
                    }
                }
                return false;
            }
        });
        et_title.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;


                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (et_title.getRight() - et_title.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here

                        et_title.setText("");
                        et_title.requestFocus();
                        return true;
                    }
                }
                return false;
            }
        });
        et_description.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;


                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (et_description.getRight() - et_description.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here

                        et_description.setText("");
                        et_description.requestFocus();
                        return true;
                    }
                }
                return false;
            }
        });
        et_type.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;


                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (et_type.getRight() - et_type.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here

                        et_type.setText("");
                        et_type.requestFocus();
                        return true;
                    }
                }
                return false;
            }
        });
//        et_start_date.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent event) {
//                final int DRAWABLE_RIGHT = 2;
//
//
//                if(event.getAction() == MotionEvent.ACTION_UP) {
//                    if(event.getRawX() >= (et_start_date.getRight() - et_start_date.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
//                        // your action here
//
//                        et_start_date.setText("");
//                        et_start_date.requestFocus();
//                        return true;
//                    }
//                }
//                return false;
//            }
//        });
//        et_start_time.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent event) {
//                final int DRAWABLE_RIGHT = 2;
//
//
//                if(event.getAction() == MotionEvent.ACTION_UP) {
//                    if(event.getRawX() >= (et_start_time.getRight() - et_start_time.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
//                        // your action here
//
//                        et_start_time.setText("");
//                        et_start_time.requestFocus();
//                        return true;
//                    }
//                }
//                return false;
//            }
//        });
//        et_end_time.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent event) {
//                final int DRAWABLE_RIGHT = 2;
//
//
//                if(event.getAction() == MotionEvent.ACTION_UP) {
//                    if(event.getRawX() >= (et_end_time.getRight() - et_end_time.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
//                        // your action here
//
//                        et_end_time.setText("");
//                        et_end_time.requestFocus();
//                        return true;
//                    }
//                }
//                return false;
//            }
//        });
        et_location.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;


                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (et_location.getRight() - et_location.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here

                        et_location.setText("");
                        et_location.requestFocus();
                        return true;
                    }
                }
                return false;
            }
        });

//        et_end_date.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent event) {
//                final int DRAWABLE_RIGHT = 2;
//
//
//                if(event.getAction() == MotionEvent.ACTION_UP) {
//                    if(event.getRawX() >= (et_end_date.getRight() - et_end_date.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
//                        // your action here
//
//                        et_end_date.setText("");
//                        et_end_date.requestFocus();
//                        return true;
//                    }
//                }
//                return false;
//            }
//        });

        et_start_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar myCalendar=Calendar.getInstance();
                Calendar c = Calendar.getInstance();
                year = c.get(Calendar.YEAR);
                month = c.get(Calendar.MONTH);

                day = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog.OnDateSetListener dateSetListener =new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                        myCalendar.set(Calendar.YEAR, i);
                        myCalendar.set(Calendar.MONTH, i1);
                        myCalendar.set(Calendar.DAY_OF_MONTH, i2);

                        String myFormat = "yyyy-MM-dd"; //In which you need put here
                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                        date_String = sdf.format(myCalendar.getTime());
                        et_start_date.setText(date_String);
                    }
                };


                datePickerDialog = new android.app.DatePickerDialog(AddEventsActivity.this, dateSetListener, year, month, day);
                datePickerDialog.show();
            }
        });

        et_end_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar myCalendar=Calendar.getInstance();
                Calendar c = Calendar.getInstance();
                year = c.get(Calendar.YEAR);
                month = c.get(Calendar.MONTH);

                day = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog.OnDateSetListener dateSetListener =new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                        myCalendar.set(Calendar.YEAR, i);
                        myCalendar.set(Calendar.MONTH, i1);
                        myCalendar.set(Calendar.DAY_OF_MONTH, i2);

                        String myFormat = "yyyy-MM-dd"; //In which you need put here
                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                        end_date_String = sdf.format(myCalendar.getTime());
                        et_end_date.setText(end_date_String);
                    }
                };

                datePickerDialog = new android.app.DatePickerDialog(AddEventsActivity.this, dateSetListener, year, month, day);
                datePickerDialog.show();
            }
        });
        et_start_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar myCalendar=Calendar.getInstance();
                Calendar calendar=Calendar.getInstance();
                hours=calendar.get(Calendar.HOUR_OF_DAY);
                minutes=calendar.get(Calendar.MINUTE);
                TimePickerDialog.OnTimeSetListener timeSetListener=new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                        myCalendar.set(Calendar.HOUR_OF_DAY,hourOfDay);
                        myCalendar.set(Calendar.MINUTE,minute);
                        String myFormat = "hh:mm a"; //In which you need put here
                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);


                        time_String = sdf.format(myCalendar.getTime());
                        et_start_time.setText(time_String);

                    }
                };
                timePickerDialog=new android.app.TimePickerDialog(AddEventsActivity.this,timeSetListener,hours,minutes,false);
                timePickerDialog.show();
            }
        });
        et_end_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar myCalendar=Calendar.getInstance();
                Calendar calendar=Calendar.getInstance();
                hours=calendar.get(Calendar.HOUR_OF_DAY);
                minutes=calendar.get(Calendar.MINUTE);
                TimePickerDialog.OnTimeSetListener timeSetListener=new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                        myCalendar.set(Calendar.HOUR_OF_DAY,hourOfDay);
                        myCalendar.set(Calendar.MINUTE,minute);
                        String myFormat = "hh:mm a"; //In which you need put here
                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                        end_time_String = sdf.format(myCalendar.getTime());
                        et_end_time.setText(end_time_String);

                    }
                };
                timePickerDialog=new android.app.TimePickerDialog(AddEventsActivity.this,timeSetListener,hours,minutes,false);
                timePickerDialog.show();
            }
        });

        et_type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(AddEventsActivity.this,EventTypePopupActivity.class);
                startActivityForResult(intent,2);
            }
        });

        btn_add_event.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                title=et_title.getText().toString();
                description=et_description.getText().toString();

                if(type_code==null)
                location=et_location.getText().toString();
                callAddEventsApi(AddEventsActivity.this);
            }
        });
    }
    private void getRef(){

        tv_location=(TextView)findViewById(R.id.tv_location);
        preferences=this.getSharedPreferences(LOGIN_PREFS_NAME,MODE_PRIVATE);
        api_token=preferences.getString(Constants.ACCESS_TOKEN_PREFS_KEY,null);
        et_description=(EditText)findViewById(R.id.et_description_add_events);
        et_title=(EditText)findViewById(R.id.et_title_add_events);
        et_type=(EditText)findViewById(R.id.et_type_add_events);
        et_start_date=(EditText)findViewById(R.id.et_start_date_add_events);
        et_end_date=(EditText)findViewById(R.id.et_end_date_add_events);
        et_end_time=(EditText)findViewById(R.id.et_end_time_add_events);
        et_start_time=(EditText)findViewById(R.id.et_start_time_add_events);
        et_location=(EditText)findViewById(R.id.et_location_add_events);
        et_groups=(EditText)findViewById(R.id.et_groups_add_events);
        btn_add_event=(Button)findViewById(R.id.btn_add_event);
        singleton=Singleton.getInstance();
    }
    private void callAddEventsApi(final Context context)
    {
        //swipeRefreshLayout.setRefreshing(true);

        Map<String, String> params= new HashMap<String, String>();





        if(type_code==null) {
            String start_time="";
            try {

                Date date1 = new SimpleDateFormat("hh:mm a").parse(time_String);

//            String string = sdf.format(date);
                String myFormat = "HH:mm"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                start_time = sdf.format(date1);

            } catch (ParseException e) {
                e.printStackTrace();
            }
            String end_time = "";
            try {
                Date date1 = new SimpleDateFormat("hh:mm a").parse(end_time_String);

//            String string = sdf.format(date);
                String myFormat = "HH:mm"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                end_time = sdf.format(date1);

            } catch (ParseException e) {
                e.printStackTrace();
            }
            addEventsRequestPojo.setEnd_time(end_time);
            addEventsRequestPojo.setStart_time(start_time);
        }
        addEventsRequestPojo.setGroup_add_id(group_id_array);
        addEventsRequestPojo.setDesc(description);
        addEventsRequestPojo.setTitle(title);
        addEventsRequestPojo.setStart_date(date_String);
        addEventsRequestPojo.setEnd_date(end_date_String);
        addEventsRequestPojo.setEvent_type_id(type_id);

//addEventsRequestPojo.setEvent_type_id();
        Gson gson=new Gson();
        JSONObject jsonObject=null;
        try {
            jsonObject=new JSONObject(gson.toJson(addEventsRequestPojo));
        } catch (JSONException e) {
            e.printStackTrace();
        }


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                BASE_URL+"api/event/add", jsonObject,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        if(response!=null)
                            Log.d("Response", response.toString());


                        //Toast.makeText(mActivity,"Login Successful",Toast.LENGTH_SHORT).show();
                        //swipeRefreshLayout.setRefreshing(false);
                    }
                }, new Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
//                if(swipeRefreshLayout.isRefreshing()){
//                    swipeRefreshLayout.setRefreshing(false);
//                }

                //VolleyLog.d("Error", "Error: " + error.getMessage());
                String message="";
                if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }


                if (!message.equalsIgnoreCase(""))
                    Toasty.error(context, message, Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {

                if(response!=null) {
                    System.out.println("Response code from parse Network response is " + response.statusCode + " and data is " + response.toString() + " and data is " + response.data.toString());
                    try {
                        String jsonString = new String(response.data, "UTF-8");
                        System.out.println("Response from parse Network response is " + jsonString);

                        //Gson gson = new Gson();
                        //final LinksPojo linksPojo = gson.fromJson(jsonString, LinksPojo.class);

                        if (response.statusCode == 200) {

                            //Toast.makeText(mActivity,"Login Successful",Toast.LENGTH_SHORT).show();







                            ((AddEventsActivity) context).runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    Toast.makeText(AddEventsActivity.this,"Event added successfully",Toast.LENGTH_SHORT).show();

                                    finish();

                                }
                            });



                        }
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
                return super.parseNetworkResponse(response);
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {

                NetworkResponse networkResponse=volleyError.networkResponse;
                if(networkResponse!=null){

                    String jsonString="";
                    try {
                        jsonString = new String(networkResponse.data, "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                    System.out.println("Network response is "+networkResponse.statusCode+jsonString);
                    try {
                        JSONObject jsonObject=new JSONObject(jsonString);

                        final String errorString=jsonObject.getString("error");
                        ((AddEventsActivity)context).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toasty.error(context,errorString,Toast.LENGTH_SHORT).show();
                            }
                        });


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
                return super.parseNetworkError(volleyError);
            }

            /**
             * Passing some request headers
             * */

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Authorization",api_token);
                headers.put("Client-Id","1");
                return headers;
            }

        };

        singleton.addToRequestQueue(jsonObjReq, "Add Event api calling");

    }
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if(resultCode == RESULT_OK){
                Bundle bundle=data.getExtras();
                group_id_array=bundle.getStringArrayList("group_ids");
                //group_name_array=bundle.getStringArrayList("group_names");

                if(group_id_array!=null){
                    String groups=group_id_array.size()+" groups selected";
                    et_groups.setText(groups);
                }
//                for (int i=0;i<group_name_array.size();i++){
//
//                    if(i!=group_name_array.size()-1)
//                    groups=groups+group_name_array.get(i)+" ,";
//                    else
//                        groups=groups+group_name_array.get(i);
//                }

            }
        }
        if(requestCode==2){

            if(resultCode==RESULT_OK){
            Bundle bundle=data.getExtras();
            type_name=bundle.getString("type_name");
                type_code=bundle.getString("type_code");
                type_id=bundle.getString("type_id");

            //group_name_array=bundle.getStringArrayList("group_names");

            if(type_name!=null){

                et_type.setText(type_name);
            }
                if(type_code!=null){

                    et_start_time.setVisibility(View.GONE);
                    et_end_time.setVisibility(View.GONE);
                    et_location.setVisibility(View.GONE);
                    tv_location.setVisibility(View.GONE);
                }
                else{
                    tv_location.setVisibility(View.VISIBLE);
                    et_start_time.setVisibility(View.VISIBLE);
                    et_end_time.setVisibility(View.VISIBLE);
                    et_location.setVisibility(View.VISIBLE);

                }

            }
        }
    }
}
