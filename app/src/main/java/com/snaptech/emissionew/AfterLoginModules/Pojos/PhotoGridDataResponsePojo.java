package com.snaptech.emissionew.AfterLoginModules.Pojos;

/**
 * Created by vikas on 14/11/16.
 */

public class PhotoGridDataResponsePojo {

    private String image_name;
    private String thumbnail_image;
    private String large_image;
    private String album_id;

    public String getImage_name() {
        return image_name;
    }

    public void setImage_name(String image_name) {
        this.image_name = image_name;
    }

    public String getThumbnail_image() {
        return thumbnail_image;
    }

    public void setThumbnail_image(String thumbnail_image) {
        this.thumbnail_image = thumbnail_image;
    }

    public String getLarge_image() {
        return large_image;
    }

    public void setLarge_image(String large_image) {
        this.large_image = large_image;
    }

    public String getAlbum_id() {
        return album_id;
    }

    public void setAlbum_id(String album_id) {
        this.album_id = album_id;
    }
}
