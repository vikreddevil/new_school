package com.snaptech.emissionew.AfterLoginModules.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubeStandalonePlayer;
import com.snaptech.emissionew.AfterLoginModules.Pojos.VideosResponsePojo;
import com.snaptech.emissionew.R;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import es.dmoral.toasty.Toasty;

/**
 * Created by vikas on 01/11/16.
 */


//this adapter is to show the videos list


public class VideosAdapter extends RecyclerView.Adapter<VideosAdapter.CustomViewHolder> {

    private ArrayList<VideosResponsePojo> videosResponsePojoList;
    private Context mContext;

    public VideosAdapter(Context context, ArrayList<VideosResponsePojo> videosResponsePojoList) {
        this.videosResponsePojoList = videosResponsePojoList;
        this.mContext = context;
    }

    @Override
    public VideosAdapter.CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.videos_row, viewGroup,false);
        VideosAdapter.CustomViewHolder viewHolder = new VideosAdapter.CustomViewHolder(view);
        viewHolder.cardView.setPreventCornerOverlap(false);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(VideosAdapter.CustomViewHolder customViewHolder, final int i) {

        String pattern = "(?<=watch\\?v=|/videos/|embed\\/)[^#\\&\\?]*";
        String id="";
        Pattern compiledPattern = Pattern.compile(pattern);
        Matcher matcher = compiledPattern.matcher(videosResponsePojoList.get(i).getVideo_url());

        if(matcher.find()){
            id= matcher.group();
        }

        // loading video image icon


        Picasso.with(mContext).load("https://img.youtube.com/vi/"+id+"/hqdefault.jpg")
        .error(R.drawable.placeholder)
        .placeholder(R.drawable.placeholder)
        .into(customViewHolder.imageView);

        customViewHolder.tv_name.setText(videosResponsePojoList.get(i).getVideo_name());



        DateFormat fromFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        fromFormat.setLenient(false);
        DateFormat toFormat = new SimpleDateFormat("EEE dd-MM-yyyy");
        toFormat.setLenient(false);
        String dateStr = videosResponsePojoList.get(i).getCreated_at();
        Date date = null;
        try {
            date = fromFormat.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        customViewHolder.tv_date.setText(toFormat.format(date));
        DateFormat fromFormat2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        fromFormat2.setLenient(false);
        DateFormat toFormat2 = new SimpleDateFormat("hh:mm aa");
        toFormat2.setLenient(false);

        Date date2 = null;
        try {
            date2 = fromFormat2.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        customViewHolder.tv_time.setText(toFormat2.format(date2));
        final String finalId = id;

        //opening youtube app on click of list item of videos list

        customViewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

              if(!finalId.equalsIgnoreCase("")){
                Intent intent = YouTubeStandalonePlayer.createVideoIntent(
                        (Activity)mContext, "AIzaSyB3XgbXLS5a1ynmUSNaDx6U49ljCZXxEBc", finalId, 0, false, false);

                if (intent != null) {

                    if(canResolveIntent(intent))
                    ((Activity)mContext).startActivityForResult(intent, 1);

                    else {
                        YouTubeInitializationResult.SERVICE_MISSING
                                .getErrorDialog((Activity) mContext, 2).show();
                    }

                }
              }
                else {

                  Toasty.error(mContext,"Invalid video link.",Toast.LENGTH_SHORT).show();
              }
            }
        });
    }

    @Override
    public int getItemCount() {
        return (videosResponsePojoList.size());
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {
        protected ImageView imageView;
        protected TextView tv_name;
        protected TextView tv_date;
        protected TextView tv_time;
        private CardView cardView;

        public CustomViewHolder(View view) {
            super(view);

            this.cardView=(CardView)view.findViewById(R.id.cardview_notification_row);
            tv_name=(TextView)view.findViewById(R.id.tv_video_name);
            tv_date=(TextView)view.findViewById(R.id.tv_video_date);
            tv_time=(TextView)view.findViewById(R.id.tv_video_time);
        this.imageView = (ImageView) view.findViewById(R.id.MediaPreview);
        }
    }
    private boolean canResolveIntent(Intent intent) {
        List<ResolveInfo> resolveInfo = mContext.getPackageManager().queryIntentActivities(intent, 0);
        return resolveInfo != null && !resolveInfo.isEmpty();
    }
}
