package com.snaptech.emissionew.AfterLoginModules.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;


import com.snaptech.emissionew.AfterLoginModules.PhotoGridActivity;
import com.snaptech.emissionew.AfterLoginModules.Pojos.PhotosDataResponsePojo;
import com.snaptech.emissionew.R;
import com.squareup.picasso.Picasso;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by vikas on 01/11/16.
 */

//this adapter is for photos list view

public class PhotosAdapter extends RecyclerView.Adapter<PhotosAdapter.CustomViewHolder> {

    private ArrayList<PhotosDataResponsePojo> photosPojoList;
    private Context mContext;

    public PhotosAdapter(Context context, ArrayList<PhotosDataResponsePojo> photosPojoList) {
        this.photosPojoList = photosPojoList;
        this.mContext = context;
    }

    @Override
    public PhotosAdapter.CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.photos_row, viewGroup,false);
        PhotosAdapter.CustomViewHolder viewHolder = new PhotosAdapter.CustomViewHolder(view);
        viewHolder.cardView.setPreventCornerOverlap(false);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(PhotosAdapter.CustomViewHolder customViewHolder, final int i) {

        URL sourceUrl=null;
        String temp=photosPojoList.get(i).getAlbum_image();
        temp = temp.replaceAll(" ", "%20");
        try {
            sourceUrl = new URL(temp);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }


        System.out.println("Url in album "+i+sourceUrl);
        //loading photo as the album main image

        Picasso.with(mContext).load(sourceUrl.toString()).fit().centerCrop()
        .error(R.drawable.placeholder)
        .placeholder(R.drawable.placeholder)
        .into(customViewHolder.imageButton);

        customViewHolder.tv_title.setText(photosPojoList.get(i).getAlbum_name());
        DateFormat fromFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        fromFormat.setLenient(false);
        DateFormat toFormat = new SimpleDateFormat("EEE dd-MM-yyyy");
        toFormat.setLenient(false);
        String dateStr = photosPojoList.get(i).getCreated_at();
        Date date = null;
        try {
            date = fromFormat.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        customViewHolder.tv_date.setText(toFormat.format(date));
        DateFormat fromFormat2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        fromFormat2.setLenient(false);
        DateFormat toFormat2 = new SimpleDateFormat("hh:mm aa");
        toFormat2.setLenient(false);
        Date date2 = null;
        try {
            date2 = fromFormat2.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if(photosPojoList.get(i).getTotal_image()==1)
        customViewHolder.tv_count.setText(photosPojoList.get(i).getTotal_image()+" Photo");
        else
            customViewHolder.tv_count.setText(photosPojoList.get(i).getTotal_image()+" Photos");
        customViewHolder.tv_time.setText(toFormat2.format(date2));


        //Opening Album photos on click of the list view item

        customViewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(mContext, PhotoGridActivity.class);
                intent.putExtra("id",photosPojoList.get(i).getId());
                mContext.startActivity(intent);

            }
        });
        customViewHolder.imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(mContext, PhotoGridActivity.class);
                intent.putExtra("id",photosPojoList.get(i).getId());
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return (photosPojoList.size());
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {
        protected TextView tv_title;
        private CardView cardView;
        private ImageButton imageButton;
        private TextView tv_date;
        private TextView tv_time;
        private TextView tv_count;

        public CustomViewHolder(View view) {
            super(view);

            tv_title=(TextView)view.findViewById(R.id.tv_photos_title);
            imageButton=(ImageButton)view.findViewById(R.id.imgbtn_photos_row_button);
            this.cardView=(CardView)view.findViewById(R.id.cardview_notification_row);
            this.tv_date=(TextView)view.findViewById(R.id.tv_photos_date);
            this.tv_time=(TextView)view.findViewById(R.id.tv_photos_time);
            this.tv_count=(TextView)view.findViewById(R.id.tv_photos_count);
        }
    }
}