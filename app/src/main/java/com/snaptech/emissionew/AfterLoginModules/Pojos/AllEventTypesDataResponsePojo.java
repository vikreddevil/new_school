package com.snaptech.emissionew.AfterLoginModules.Pojos;

/**
 * Created by vikas on 27/03/17.
 */

public class AllEventTypesDataResponsePojo {

    //{"data":[{"event_type_id":1,"event_code":"PH","event_type_name":"PH"},{"event_type_id":2,"event_code":null,"event_type_name":"SH"},{"event_type_id":3,"event_code":"H","event_type_name":"HOLIDAY"}]}
    private int event_type_id;
    private String event_type_name;
    private String event_code;

    public int getEvent_type_id() {
        return event_type_id;
    }

    public void setEvent_type_id(int event_type_id) {
        this.event_type_id = event_type_id;
    }

    public String getEvent_type_name() {
        return event_type_name;
    }

    public void setEvent_type_name(String event_type_name) {
        this.event_type_name = event_type_name;
    }

    public String getEvent_code() {
        return event_code;
    }

    public void setEvent_code(String event_code) {
        this.event_code = event_code;
    }
}
