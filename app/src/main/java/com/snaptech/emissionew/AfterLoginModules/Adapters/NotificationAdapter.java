package com.snaptech.emissionew.AfterLoginModules.Adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.alexandrius.accordionswipelayout.library.SwipeLayout;
import com.snaptech.emissionew.AfterLoginModules.Pojos.NotificationResponsePojo;
import com.snaptech.emissionew.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by vikas on 28/10/16.
 */


//this adapter is for displaying notification list


public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.CustomViewHolder> {

private List<NotificationResponsePojo> notificationPojoList;


public NotificationAdapter(Context context, List<NotificationResponsePojo> notificationPojoList) {
        this.notificationPojoList = notificationPojoList;
        }

@Override
public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.notification_row, viewGroup,false);
        CustomViewHolder viewHolder = new CustomViewHolder(view);

        return viewHolder;
        }

@Override
public void onBindViewHolder(CustomViewHolder viewHolder, int i) {

    viewHolder.cardView.setPreventCornerOverlap(false);
    DateFormat fromFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    fromFormat.setLenient(false);
    DateFormat toFormat = new SimpleDateFormat("dd-MM-yyyy");
    toFormat.setLenient(false);
    String dateStr = notificationPojoList.get(i).getCreated_at();
    Date date = null;
    try {
        date = fromFormat.parse(dateStr);
    } catch (ParseException e) {
        e.printStackTrace();
    }

    //date formatting

    viewHolder.tv_date.setText(toFormat.format(date));
    DateFormat fromFormat2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    fromFormat2.setLenient(false);
    DateFormat toFormat2 = new SimpleDateFormat("hh:mm aa");
    toFormat2.setLenient(false);
    Date date2 = null;
    try {
        date2 = fromFormat2.parse(dateStr);
    } catch (ParseException e) {
        e.printStackTrace();
    }

    //setting data in the views

    viewHolder.tv_time.setText(toFormat2.format(date2));
    viewHolder.tv_desc.setText(notificationPojoList.get(i).getDescription());
    viewHolder.tv_title.setText(notificationPojoList.get(i).getTitle());

    viewHolder.swipeLayout.setOnSwipeItemClickListener(new SwipeLayout.OnSwipeItemClickListener() {
        @Override
        public void onSwipeItemClick(boolean left, int index) {
            if (left) {
                switch (index) {
                    case 0:
                        break;
                }
            } else {
                switch (index) {
                    case 0:
                        break;
                }
            }
        }
    });

        }

@Override
public int getItemCount() {
        return notificationPojoList.size();
        }

class CustomViewHolder extends RecyclerView.ViewHolder {
    protected TextView tv_title;
    protected TextView tv_desc;
    protected TextView tv_date;
    protected TextView tv_time;
    private CardView cardView;
    private SwipeLayout swipeLayout;

    public CustomViewHolder(View view) {
        super(view);
         this.swipeLayout = (SwipeLayout) view.findViewById(R.id.swipe_layout);
        this.tv_title=(TextView)view.findViewById(R.id.tv_title_notification);
        this.tv_desc=(TextView)view.findViewById(R.id.tv_description_notification);
        this.cardView=(CardView)view.findViewById(R.id.cardview_notification_row);
        this.tv_date=(TextView)view.findViewById(R.id.tv_date_notifications);
        this.tv_time=(TextView)view.findViewById(R.id.tv_time_notification);

    }
}
}


