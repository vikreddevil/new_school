package com.snaptech.emissionew.AfterLoginModules.Fragments;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.snaptech.emissionew.AfterLoginModules.HomeScreenActivity;
import com.snaptech.emissionew.AfterLoginModules.Pojos.InstituteInfoDataResponsePojo;
import com.snaptech.emissionew.AfterLoginModules.Pojos.InstituteInfoResponsePojo;
import com.snaptech.emissionew.AfterLoginModules.Pojos.NotificationPojo;
import com.snaptech.emissionew.R;
import com.snaptech.emissionew.Utils.Constants;
import com.snaptech.emissionew.Utils.Singleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.dmoral.toasty.Toasty;

import static com.snaptech.emissionew.Utils.Constants.BASE_URL;

/**
 * Created by vikas on 23/03/17.
 */

public class InstituteInfoFragment extends Fragment {

    private ImageButton imageButton_youtube;
    private ImageButton imageButton_facebook;
    private ImageButton imageButton_instagram;
    private ImageButton imageButton_twitter;
    private TextView tv_title;
    private TextView tv_address;
    private TextView tv_website;
    private TextView tv_email;
    private TextView tv_contact;

    private String youtube_url="";
    private String facebook_url="";
    private String twitter_url="";
    private String insta_url="";

    private Activity mActivity;
    private SharedPreferences sharedPreferences;
    private Singleton singleton;
    private String api_token;
    public InstituteInfoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view;
        singleton=Singleton.getInstance();
        view=inflater.inflate(R.layout.fragment_institute_info,null);

        getRef(view);


        callInstituteInfoApi(mActivity);

        imageButton_facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    mActivity.getPackageManager().getPackageInfo("com.facebook.katana", 0);
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(facebook_url)));
                    // startActivity(new Intent("com.facebook.katana", Uri.parse("")));
                }catch (Exception e){

                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(facebook_url)));
                }
            }
        });
        imageButton_instagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    Uri uri = Uri.parse(insta_url);
                    Intent insta = new Intent(Intent.ACTION_VIEW, uri);
                    insta.setPackage("com.instagram.android");

                    if (isIntentAvailable(mActivity, insta)){
                        startActivity(insta);
                    } else{
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(insta_url)));
                    }
                    //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://instagram.com/kipling_morelia/")).setPackage("com.instagram.android"));
                }catch (Exception e){
                    Toast.makeText(mActivity,"Please ",Toast.LENGTH_SHORT).show();
                    //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.instagram.com/kipling_morelia/")));

                }
            }
        });
        imageButton_youtube.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {


                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/channel/UCQQnqJpgn3k25AO3CCN59Bw")));
                }catch (Exception e){

                    Toast.makeText(mActivity,"Su teléfono no es compatible con esta función",Toast.LENGTH_SHORT).show();
                }
            }
        });
imageButton_twitter.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {

        Intent intent = null;
        try {
            // get the Twitter app if possible
            mActivity.getPackageManager().getPackageInfo("com.twitter.android", 0);
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(twitter_url));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        } catch (Exception e) {
            // no Twitter app, revert to browser
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(twitter_url));
        }
        startActivity(intent);

    }
});
        return view;
    }
    private void getRef(View view){

        imageButton_youtube=(ImageButton)view.findViewById(R.id.btn_youtube);
        imageButton_facebook=(ImageButton)view.findViewById(R.id.btn_facebook);
        imageButton_instagram=(ImageButton)view.findViewById(R.id.btn_instagram);
        imageButton_twitter=(ImageButton) view.findViewById(R.id.btn_twitter);

        tv_address=(TextView)view.findViewById(R.id.tv_address_institute);
        tv_contact=(TextView)view.findViewById(R.id.tv_contact_institute);
        tv_email=(TextView)view.findViewById(R.id.tv_email_institute);
        tv_title=(TextView)view.findViewById(R.id.tv_title_institute);
        tv_website=(TextView)view.findViewById(R.id.tv_web_institute);

        sharedPreferences=mActivity.getSharedPreferences(Constants.LOGIN_PREFS_NAME,Context.MODE_PRIVATE);
        api_token=sharedPreferences.getString(Constants.ACCESS_TOKEN_PREFS_KEY,null);
    }
    private boolean isIntentAvailable(Context ctx, Intent intent) {
        final PackageManager packageManager = ctx.getPackageManager();
        List<ResolveInfo> list = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        return list.size() > 0;
    }

    @TargetApi(23)
    @Override public void onAttach(Context context) {
        //This method avoid to call super.onAttach(context) if I'm not using api 23 or more
        //if (Build.VERSION.SDK_INT >= 23) {
        super.onAttach(context);
        onAttachToContext(context);
        //}
    }

    /*
     * Deprecated on API 23
     * Use onAttachToContext instead
     */
    @SuppressWarnings("deprecation")
    @Override public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (Build.VERSION.SDK_INT < 23) {
            onAttachToContext(activity);
        }
    }
    protected void onAttachToContext(Context context){

        mActivity=(Activity)context;
    }
    private void callInstituteInfoApi(final Context context)
    {
//        swipeRefreshLayout.setRefreshing(true);

        Map<String, String> params= new HashMap<String, String>();
//        params.put("limit", "10");
//        params.put("offset","0");

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                BASE_URL+"api/client/details", new JSONObject(params),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        if(response!=null)
                            Log.d("Response inst info", response.toString());


                        //Toast.makeText(mActivity,"Login Successful",Toast.LENGTH_SHORT).show();
                        //swipeRefreshLayout.setRefreshing(false);
                    }
                }, new Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
//                mRecyclerView.setVisibility(View.GONE);
                // tv_no_events.setVisibility(View.VISIBLE);
                //tv_no_events.setText("Sorry, no notifications found");
//                if(swipeRefreshLayout.isRefreshing()){
//                    swipeRefreshLayout.setRefreshing(false);
//                }
                //Constants.notification_read=true;
                //VolleyLog.d("Error", "Error: " + error.getMessage());
                // swipeRefreshLayout.setRefreshing(false);



                String message="";
                if (error instanceof NoConnectionError) {
                    message = mActivity.getString(R.string.network_error_message);
                } else if (error instanceof TimeoutError) {
                    message = mActivity.getString(R.string.network_error_message);
                }


                if(!message.equalsIgnoreCase(""))
                    Toasty.error(mActivity, message, Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {

                if(response!=null){
                    System.out.println("Response code from parse Network response is "+response.statusCode+" and data is "+response.toString()+" and data is "+response.data.toString());
                    try {
                        String jsonString = new String(response.data, "UTF-8");
                        System.out.println("Response from parse Network response is "+jsonString);

                        Gson gson = new Gson();
                        // Reader reader = new InputStreamReader();
                        final InstituteInfoResponsePojo instituteInfoResponsePojo = gson.fromJson(jsonString,InstituteInfoResponsePojo.class); // (reader, Login_Response_Pojo.class);


                        if(response.statusCode==200){
                            // Constants.notification_read=true;

                            if(instituteInfoResponsePojo.getInstituteInfoDataResponsePojoArrayList().size()!=0) {

                                mActivity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {


                                        InstituteInfoDataResponsePojo instituteInfoDataResponsePojo=instituteInfoResponsePojo.getInstituteInfoDataResponsePojoArrayList().get(0);

                                        if(instituteInfoDataResponsePojo.getName()!=null){

                                            tv_title.setVisibility(View.VISIBLE);
                                            tv_title.setText(instituteInfoDataResponsePojo.getName());

                                        }
                                        else{

                                            tv_title.setVisibility(View.GONE);
                                        }


                                        if(instituteInfoDataResponsePojo.getAddress()!=null){

                                            tv_address.setVisibility(View.VISIBLE);
                                            tv_address.setText(instituteInfoDataResponsePojo.getAddress());

                                        }
                                        else{

                                            tv_address.setVisibility(View.GONE);
                                        }


                                        if(instituteInfoDataResponsePojo.getEmail_1()!=null||instituteInfoDataResponsePojo.getEmail_2()!=null){


                                            tv_email.setVisibility(View.VISIBLE);
                                            if(instituteInfoDataResponsePojo.getEmail_1()!=null&&instituteInfoDataResponsePojo.getEmail_2()!=null) {

                                                tv_email.setText(instituteInfoDataResponsePojo.getEmail_1()+", "+instituteInfoDataResponsePojo.getEmail_2());


                                            }
                                            else{
                                                tv_email.setText(instituteInfoDataResponsePojo.getEmail_1());

                                            }

                                        }
                                        else{

                                            tv_email.setVisibility(View.GONE);
                                        }

                                        if(instituteInfoDataResponsePojo.getPhone_no_1()!=null||instituteInfoDataResponsePojo.getPhone_no_2()!=null){


                                            tv_contact.setVisibility(View.VISIBLE);
                                            if(instituteInfoDataResponsePojo.getPhone_no_1()!=null&&instituteInfoDataResponsePojo.getPhone_no_2()!=null) {

                                                tv_contact.setText(instituteInfoDataResponsePojo.getPhone_no_1()+", "+instituteInfoDataResponsePojo.getPhone_no_2());


                                            }
                                            else{
                                                tv_contact.setText(instituteInfoDataResponsePojo.getPhone_no_1());

                                            }

                                        }
                                        else{

                                            tv_contact.setVisibility(View.GONE);
                                        }

                                        if(instituteInfoDataResponsePojo.getFb_link()!=null){

                                            imageButton_facebook.setVisibility(View.VISIBLE);
                                            facebook_url=instituteInfoDataResponsePojo.getFb_link();

                                        }
                                        else{

                                            imageButton_facebook.setVisibility(View.GONE);
                                        }
                                        if(instituteInfoDataResponsePojo.getInstagram_link()!=null){

                                            imageButton_instagram.setVisibility(View.VISIBLE);
                                            insta_url=instituteInfoDataResponsePojo.getInstagram_link();

                                        }
                                        else{

                                            imageButton_instagram.setVisibility(View.GONE);
                                        }
                                        if(instituteInfoDataResponsePojo.getTwitter_link()!=null){

                                            imageButton_twitter.setVisibility(View.VISIBLE);
                                            twitter_url=instituteInfoDataResponsePojo.getTwitter_link();

                                        }
                                        else{

                                            imageButton_twitter.setVisibility(View.GONE);
                                        }
                                        if(instituteInfoDataResponsePojo.getYoutube_link()!=null){

                                            imageButton_youtube.setVisibility(View.VISIBLE);
                                            youtube_url=instituteInfoDataResponsePojo.getYoutube_link();

                                        }
                                        else{

                                            imageButton_youtube.setVisibility(View.GONE);
                                        }
                                    }
                                });

                            }

                        }
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
                return super.parseNetworkResponse(response);
            }


            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {

                NetworkResponse networkResponse=volleyError.networkResponse;
                if(networkResponse!=null){

                    String jsonString="";
                    try {
                        jsonString = new String(networkResponse.data, "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                    System.out.println("Network response is "+networkResponse.statusCode+jsonString);
                    try {
                        JSONObject jsonObject=new JSONObject(jsonString);
                        JSONArray jsonArray=jsonObject.getJSONArray("error");
                        final String errorString=jsonArray.getString(0);
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toasty.error(mActivity,errorString,Toast.LENGTH_SHORT).show();
                            }
                        });


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
                return super.parseNetworkError(volleyError);
            }

            /**
             * Passing some request headers
             * */

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Authorization",api_token);
                headers.put("Client-Id","1");
                return headers;
            }



        };

        // Adding request to request queue


        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        singleton.addToRequestQueue(jsonObjReq, "Institute info api calling");

    }
}


