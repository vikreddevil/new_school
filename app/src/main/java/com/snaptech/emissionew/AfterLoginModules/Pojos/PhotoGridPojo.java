package com.snaptech.emissionew.AfterLoginModules.Pojos;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by vikas on 14/11/16.
 */

public class PhotoGridPojo {

    @SerializedName("data")
    private ArrayList<PhotoGridDataResponsePojo> photoGridDataResponsePojoArrayList;

    public ArrayList<PhotoGridDataResponsePojo> getPhotoGridDataResponsePojoArrayList() {
        return photoGridDataResponsePojoArrayList;
    }

    public void setPhotoGridDataResponsePojoArrayList(ArrayList<PhotoGridDataResponsePojo> photoGridDataResponsePojoArrayList) {
        this.photoGridDataResponsePojoArrayList = photoGridDataResponsePojoArrayList;
    }
}
