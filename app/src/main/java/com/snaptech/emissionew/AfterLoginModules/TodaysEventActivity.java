package com.snaptech.emissionew.AfterLoginModules;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.bumptech.glide.request.target.Target;
import com.google.gson.Gson;
import com.jmavarez.materialcalendar.Util.CalendarDay;
import com.snaptech.emissionew.AfterLoginModules.Adapters.CalendarListAdapter;
import com.snaptech.emissionew.AfterLoginModules.Adapters.SelectedDayEventListAdapter;
import com.snaptech.emissionew.AfterLoginModules.Fragments.CalendarFragment;
import com.snaptech.emissionew.AfterLoginModules.Pojos.CalendarMonthwiseDataResponsePojo;
import com.snaptech.emissionew.AfterLoginModules.Pojos.CalendarMonthwisePojo;
import com.snaptech.emissionew.R;
import com.snaptech.emissionew.Utils.Constants;
import com.snaptech.emissionew.Utils.Singleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import es.dmoral.toasty.Toasty;
import in.myinnos.gifimages.gif.GifView;

import static com.snaptech.emissionew.AfterLoginModules.Fragments.CalendarFragment.calendarMonthwiseDataResponsePojoArrayList_selected_date;
import static com.snaptech.emissionew.Utils.Constants.BASE_URL;


public class TodaysEventActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    protected RecyclerView.LayoutManager mLayoutManager;
    private Singleton singleton;
    private String api_token;
    private static final String KEY_LAYOUT_MANAGER = "layoutManager";
    private SharedPreferences sharedPreferences;
    private ImageView img_todays_event;
    private TextView tv_date;
    private TextView tv_no_events;
    private String selected_date="";
    private ArrayList<CalendarMonthwiseDataResponsePojo> calendarMonthwiseDataResponsePojoArrayList;

    private GifView gifView;
    private enum LayoutManagerType {
        GRID_LAYOUT_MANAGER,
        LINEAR_LAYOUT_MANAGER
    }

    protected TodaysEventActivity.LayoutManagerType mCurrentLayoutManagerType;

    //CalendarListAdapter calendarListAdapter;
    SelectedDayEventListAdapter selectedDayEventListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todays_event);
        getRef();
        Intent intent=getIntent();
        if(intent!=null){

            if(intent.getStringExtra("date")!=null){
                selected_date=intent.getStringExtra("date");
                tv_date.setText(intent.getStringExtra("date"));
            }
        }
        img_todays_event=(ImageView)findViewById(R.id.img_todays_event);
//        gifView=(GifView)findViewById(R.id.gif_view);
//        Picasso.with(this).load("").into(gifView);
//        Glide.with(this).load("http://static1.squarespace.com/static/552a5cc4e4b059a56a050501/565f6b57e4b0d9b44ab87107/566024f5e4b0354e5b79dd24/1449141991793/NYCGifathon12.gif").asGif()
//                .placeholder(R.drawable.calendar_icon).centerCrop().into(img_todays_event);
        GlideDrawableImageViewTarget imageViewPreview = new GlideDrawableImageViewTarget(img_todays_event);
        Glide
                .with(this)
                .load("https://media.giphy.com/media/3og0IAcRL1z9Z6bazS/giphy.gif")
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        return false;
                    }
                })
                .into(imageViewPreview);

        mCurrentLayoutManagerType = TodaysEventActivity.LayoutManagerType.LINEAR_LAYOUT_MANAGER;

        if (savedInstanceState != null) {
            // Restore saved layout manager type.
            mCurrentLayoutManagerType = (TodaysEventActivity.LayoutManagerType) savedInstanceState
                    .getSerializable(KEY_LAYOUT_MANAGER);
        }
        setRecyclerViewLayoutManager(mCurrentLayoutManagerType);
    }

    private void setRecyclerViewLayoutManager(TodaysEventActivity.LayoutManagerType layoutManagerType) {
        int scrollPosition = 0;

        // If a layout manager has already been set, get current scroll position.
        if (mRecyclerView.getLayoutManager() != null) {
            scrollPosition = ((LinearLayoutManager) mRecyclerView.getLayoutManager())
                    .findFirstCompletelyVisibleItemPosition();
        }

        switch (layoutManagerType) {
            case GRID_LAYOUT_MANAGER:
                mLayoutManager = new GridLayoutManager(TodaysEventActivity.this, 2);
                mCurrentLayoutManagerType = TodaysEventActivity.LayoutManagerType.GRID_LAYOUT_MANAGER;
                break;
            case LINEAR_LAYOUT_MANAGER:
                mLayoutManager = new LinearLayoutManager(this);
                mCurrentLayoutManagerType = TodaysEventActivity.LayoutManagerType.LINEAR_LAYOUT_MANAGER;
                break;
            default:
                mLayoutManager = new LinearLayoutManager(this);
                mCurrentLayoutManagerType = TodaysEventActivity.LayoutManagerType.LINEAR_LAYOUT_MANAGER;
        }

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.scrollToPosition(scrollPosition);
        callFilterWiseApi(TodaysEventActivity.this);

    }

    private void getRef() {


        sharedPreferences=TodaysEventActivity.this.getSharedPreferences(Constants.LOGIN_PREFS_NAME,MODE_PRIVATE);
        api_token=sharedPreferences.getString(Constants.ACCESS_TOKEN_PREFS_KEY,null);
        calendarMonthwiseDataResponsePojoArrayList=new ArrayList<>();
        tv_no_events=(TextView)findViewById(R.id.tv_no_events);
        tv_date=(TextView)findViewById(R.id.tv_date);
        singleton = Singleton.getInstance();
        selectedDayEventListAdapter = new SelectedDayEventListAdapter(calendarMonthwiseDataResponsePojoArrayList);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mRecyclerView.setAdapter(selectedDayEventListAdapter);


        if(calendarMonthwiseDataResponsePojoArrayList_selected_date.size()==0){

            mRecyclerView.setVisibility(View.GONE);
            tv_no_events.setVisibility(View.VISIBLE);


        }
        else{

            mRecyclerView.setVisibility(View.VISIBLE);
            tv_no_events.setVisibility(View.GONE);

        }
        // compactCalendarView=(CompactCalendarView)view.findViewById(R.id.compactcalendar_view);
        //preferences=this.getSharedPreferences(Constants.LOGIN_PREFS_NAME, Context.MODE_PRIVATE);
        //String color_prmary=preferences.getString(Constants.PRIMARY_COLOR_PREFS_KEY,null);
//        if(color_prmary!=null){
//            char[] chars=color_prmary.toCharArray();
//            String actual_color="";
//            for (int i=0;i<color_prmary.length();i++){
//
//                if(i!=0)
//                    actual_color=actual_color+chars[i];
//            }
//            // compactCalendarView.setCalendarBackgroundColor(Color.parseColor(color_prmary));
//
//
//        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        calendarMonthwiseDataResponsePojoArrayList_selected_date.clear();
        finish();
    }
    private void callFilterWiseApi(final Context context)
    {
//        swipeRefreshLayout.setRefreshing(true);

        Map<String, String> params= new HashMap<String, String>();
        //String sDate1="31/12/1998";
        Date date1=null;
        String actual_date="";
        Calendar calendar=Calendar.getInstance();

        try {
            date1=new SimpleDateFormat("dd/MM/yyyy").parse(selected_date);
            calendar.setTime(date1);
            actual_date=calendar.get(Calendar.YEAR)+"-"+(calendar.get(Calendar.MONTH)+1)+"-"+calendar.get(Calendar.DAY_OF_MONTH);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        params.put("from_date", actual_date);
        params.put("to_date",actual_date);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                BASE_URL+"api/event/daterange", new JSONObject(params),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        if(response!=null)
                            Log.d("monthwise events", response.toString());


                        //Toast.makeText(mActivity,"Login Successful",Toast.LENGTH_SHORT).show();
                        //swipeRefreshLayout.setRefreshing(false);
                    }
                }, new Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
                mRecyclerView.setVisibility(View.GONE);
                // tv_no_events.setVisibility(View.VISIBLE);
                // tv_no_events.setText("Sorry, no events found");
//                if(swipeRefreshLayout.isRefreshing()){
//                    swipeRefreshLayout.setRefreshing(false);
//                }
                //Constants.notification_read=true;
                //VolleyLog.d("Error", "Error: " + error.getMessage());
                // swipeRefreshLayout.setRefreshing(false);
                String message="";
                if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }


                if(!message.equalsIgnoreCase(""))
                    Toasty.error(TodaysEventActivity.this, message, Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {

                if(response!=null){
                    System.out.println("Response code from parse Network response is "+response.statusCode+" and data is "+response.toString()+" and data is "+response.data.toString());
                    try {
                        String jsonString = new String(response.data, "UTF-8");
                        System.out.println("Response from parse Network response is "+jsonString);

                        Gson gson = new Gson();
                        // Reader reader = new InputStreamReader();
                        final CalendarMonthwisePojo calendarMonthwisePojo = gson.fromJson(jsonString,CalendarMonthwisePojo.class); // (reader, Login_Response_Pojo.class);


                        if(response.statusCode==200){
                            // Constants.notification_read=true;

                            if(calendarMonthwisePojo.getCalendarMonthwiseDataResponsePojoArrayList().size()!=0) {

                                TodaysEventActivity.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        //Toast.makeText(mActivity,"Login Successful",Toast.LENGTH_SHORT).show();
                                        tv_no_events.setVisibility(View.GONE);
                                        mRecyclerView.setVisibility(View.VISIBLE);

                                        calendarMonthwiseDataResponsePojoArrayList.clear();
                                        //calendarDays.clear();
                                        for (int i=0;i<calendarMonthwisePojo.getCalendarMonthwiseDataResponsePojoArrayList().size();i++){

                                            calendarMonthwiseDataResponsePojoArrayList.add(i,calendarMonthwisePojo.getCalendarMonthwiseDataResponsePojoArrayList().get(i));
                                            //CalendarDay calendarDay = CalendarDay.from(new Date());
                                            System.out.println(calendarMonthwisePojo.getCalendarMonthwiseDataResponsePojoArrayList().get(i).getStart_date()+" date "+i);
                                            //Date date1=null;
//                                            try {
//                                                date1=new SimpleDateFormat("yyyy-MM-dd").parse(calendarMonthwisePojo.getCalendarMonthwiseDataResponsePojoArrayList().get(i).getStart_date());
//                                            } catch (ParseException e) {
//                                                e.printStackTrace();
//                                            }
//                                            if(!calendarMonthwisePojo.getCalendarMonthwiseDataResponsePojoArrayList().get(i).getStart_date().equals(calendarMonthwisePojo.getCalendarMonthwiseDataResponsePojoArrayList().get(i).getEnd_date())){
//
//                                                ArrayList<Date> between_dates_array_list=getDates(calendarMonthwisePojo.getCalendarMonthwiseDataResponsePojoArrayList().get(i).getStart_date(),calendarMonthwisePojo.getCalendarMonthwiseDataResponsePojoArrayList().get(i).getEnd_date());
//                                                for(int j=0;j<between_dates_array_list.size();j++) {
////                                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
////                                                    String stringDate = sdf.format(between_dates_array_list.get(i));
//                                                    Calendar calendar=Calendar.getInstance();
//                                                    calendar.setTime(between_dates_array_list.get(j));
//                                                    CalendarDay day = CalendarDay.from(calendar.get(Calendar.DAY_OF_MONTH), calendar.get(Calendar.MONTH)+1, calendar.get(Calendar.YEAR));
//                                                    calendarDays.add(day);
//
//                                                }
//                                            }
//                                            Calendar calendar=Calendar.getInstance();
//                                            calendar.setTime(date1);
//                                            CalendarDay day = CalendarDay.from(calendar.get(Calendar.DAY_OF_MONTH), calendar.get(Calendar.MONTH)+1, calendar.get(Calendar.YEAR));
//                                            System.out.println("Date mapped is "+calendar.get(Calendar.DAY_OF_MONTH)+"-"+calendar.get(Calendar.MONTH)+"-"+calendar.get(Calendar.YEAR));
//                                            calendarDays.add(day);
                                        }
                                       // calendarListAdapter.notifyDataSetChanged();

                                        selectedDayEventListAdapter.notifyDataSetChanged();
                                        //calendarView.addEvents(calendarDays);
                                        //notificationPojoList.clear();
//                                        for (int i = 0; i < notificationPojo.getNotificationResponsePojoArrayList().size(); i++) {
//                                         ''   notificationPojoList.add(i, notificationPojo.getNotificationResponsePojoArrayList().get(i));
//                                        }

//                                        notificationAdapter.notifyDataSetChanged();
//                                        if (swipeRefreshLayout.isRefreshing()) {
//                                            swipeRefreshLayout.setRefreshing(false);
//                                        }
                                        //callNotificationReadApi(context);
                                    }
                                });

                            }
                            else{

                                ((TodaysEventActivity) context).runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {

                                        mRecyclerView.setVisibility(View.GONE);
                                        tv_no_events.setVisibility(View.VISIBLE);
//                                        tv_no_events.setText("Sorry, no notifications found");
                                    }
                                });

                            }
                        }
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
                return super.parseNetworkResponse(response);
            }


            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {

                NetworkResponse networkResponse=volleyError.networkResponse;
                if(networkResponse!=null){

                    String jsonString="";
                    try {
                        jsonString = new String(networkResponse.data, "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                    System.out.println("Network response is "+networkResponse.statusCode+jsonString);
                    try {
                        JSONObject jsonObject=new JSONObject(jsonString);
                        JSONArray jsonArray=jsonObject.getJSONArray("error");
                        final String errorString=jsonArray.getString(0);
                        TodaysEventActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toasty.error(TodaysEventActivity.this,errorString,Toast.LENGTH_SHORT).show();
                            }
                        });


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
                return super.parseNetworkError(volleyError);
            }

            /**
             * Passing some request headers
             * */

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Authorization",api_token);
                headers.put("Client-Id","1");
                return headers;
            }



        };

        // Adding request to request queue


        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        singleton.addToRequestQueue(jsonObjReq, "Calendar filter wise api calling");

    }
}