package com.snaptech.emissionew.AfterLoginModules.Pojos;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by vikas on 23/03/17.
 */

public class InstituteInfoResponsePojo {


    @SerializedName("data")
    private ArrayList<InstituteInfoDataResponsePojo> instituteInfoDataResponsePojoArrayList;

    public ArrayList<InstituteInfoDataResponsePojo> getInstituteInfoDataResponsePojoArrayList() {
        return instituteInfoDataResponsePojoArrayList;
    }

    public void setInstituteInfoDataResponsePojoArrayList(ArrayList<InstituteInfoDataResponsePojo> instituteInfoDataResponsePojoArrayList) {
        this.instituteInfoDataResponsePojoArrayList = instituteInfoDataResponsePojoArrayList;
    }
}
