package com.snaptech.emissionew.AfterLoginModules;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;


import com.snaptech.emissionew.AfterLoginModules.Adapters.AlbumImagePagerAdapter;
import com.snaptech.emissionew.R;
import com.snaptech.emissionew.Utils.Constants;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;

import es.dmoral.toasty.Toasty;


//this activity is the Gallery of Images

public class FullScreenViewActivity extends AppCompatActivity {

    private ViewPager imageViewPager;
    private AlbumImagePagerAdapter albumImagePagerAdapter;
    private ProgressDialog mProgressDialog;
    private TextView countLabel;
    private TextView title;
    private ImageButton back_button;
    private ImageButton download_button;
    private String caption="image";
    int position,position_actual;
    private boolean flag_download=true;
    private boolean flag_permission_ex_storage=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_full_screen_view);
        imageViewPager = (ViewPager)findViewById(R.id.pager);

        albumImagePagerAdapter = new AlbumImagePagerAdapter(this);
        imageViewPager.setAdapter(albumImagePagerAdapter);
        position = getIntent().getIntExtra("position",0);
        imageViewPager.setCurrentItem(position);
        title=(TextView)findViewById(R.id.tv_title_full_screen_image);
        countLabel = (TextView)findViewById(R.id.page_count);
        back_button=(ImageButton)findViewById(R.id.ib_toolbar_back);
        download_button=(ImageButton)findViewById(R.id.ib_download);
        download_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!checkExternalStoragePermission()) {
                    flag_permission_ex_storage=false;
                    ContextCompat.checkSelfPermission(FullScreenViewActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
                }
                else
                    flag_permission_ex_storage=true;


                if(flag_permission_ex_storage) {
                    position_actual = imageViewPager.getCurrentItem();
                    String imageUrl = "";
                    imageUrl = Constants.photoGridDataResponsePojos.get(position_actual).getLarge_image();
                    new ImageDownloader().execute(imageUrl, Constants.photoGridDataResponsePojos.get(position_actual).getImage_name());
                }
            }
        });

        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


//        getSupportActionBar().setTitle("");


        imageViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                countLabel.setText((position+1)+"/"+ Constants.photoGridDataResponsePojos.size());

                String name=Constants.photoGridDataResponsePojos.get(position).getImage_name();
                if (name.indexOf(".") > 0)
                    name = name.substring(0, name.lastIndexOf("."));
                title.setText(name);
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
       // getMenuInflater().inflate(R.menu.menu_album_full_screen, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        // Activate the navigation drawer toggle
//        if (item.getItemId() == R.id.action_download) {
//            //TODO: download
//            int position = imageViewPager.getCurrentItem();
//       //     String imageUrl = albumDetailsList.get(position).getFilePath();
//            new ImageDownloader().execute(imageUrl);
//            return true;
//        }if (item.getItemId() == android.R.id.home){
//            finish();
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public class ImageDownloader extends AsyncTask<String, Void, Bitmap> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(FullScreenViewActivity.this);

            mProgressDialog.setMessage("Downloading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            String imageURL = params[0];
            caption=params[1];

            Bitmap bitmap = null;
            try {
                // Download Image from URL
                URI uri = new URI(imageURL.replace(" ", "%20"));
                System.out.println("Image url hit is "+uri.toString());
                InputStream input = new java.net.URL(uri.toString()).openStream();
                // Decode Bitmap
                bitmap = BitmapFactory.decodeStream(input);
            } catch (Exception e) {
                flag_download=false;
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            mProgressDialog.dismiss();



            // save image to gallery
            storeImage(bitmap);
            if(flag_download)
                Toasty.success(FullScreenViewActivity.this,"Image downloaded successfully",Toast.LENGTH_SHORT).show();
            else {
                Toasty.error(FullScreenViewActivity.this, "Something went wrong, please check your internet connection", Toast.LENGTH_SHORT).show();
                flag_download=true;
            }
        }


        public void storeImage(Bitmap bitmap) {
            //get path to external storage (SD card)
            String iconsStoragePath = Constants.PHOTO_SAVE_GALLERY_DIR_IMAGE_PATH + caption;
            File sdIconStorageDir = new File(iconsStoragePath);

            //create storage directories, if they don't exist
            if (!sdIconStorageDir.mkdir()) {
                sdIconStorageDir.mkdirs();
            }

            try {
                String filePath = sdIconStorageDir.toString() + "/image-" + position + ".jpeg";
                FileOutputStream fileOutputStream = new FileOutputStream(filePath);

                BufferedOutputStream bos = new BufferedOutputStream(fileOutputStream);

                //choose another format if PNG doesn't suit you
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);

                addImageToGallery(filePath);

                bos.flush();
                bos.close();

            } catch (FileNotFoundException e) {
                Log.w("TAG", "Error saving image file: " + e.getMessage());
                flag_download=false;

            } catch (IOException e) {
                Log.w("TAG", "Error saving image file: " + e.getMessage());
                flag_download=false;

            } catch(NullPointerException e){
                Log.w("TAG", "Error saving image file: " + e.getMessage());
                flag_download=false;
        }
            catch (Exception e){

                flag_download=false;

            }
        }


        public  void addImageToGallery(final String filePath) {

            ContentValues values = new ContentValues();

            values.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis());
            values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg");
            values.put(MediaStore.MediaColumns.DATA, filePath);

            getApplicationContext().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

    }private boolean checkExternalStoragePermission()
    {

        if (Build.VERSION.SDK_INT >= 23) {
            if (FullScreenViewActivity.this.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
//                Log.v(TAG,"Permission is granted");
                System.out.println("First condition");
                return true;
            } else {

                System.out.println("Second condition");
                //Log.v(TAG,"Permission is revoked");
                ActivityCompat.requestPermissions(FullScreenViewActivity.this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            // Log.v(TAG,"Permission is granted");
            System.out.println("Third condition");
            return true;
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        System.out.println("Called request permission");
        switch (requestCode) {


            case 1: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                    System.out.println("Inside case 1");
                    String imageUrl="";
                    imageUrl=Constants.photoGridDataResponsePojos.get(position_actual).getLarge_image();
                    if(imageUrl!=null)
                    new ImageDownloader().execute(imageUrl,Constants.photoGridDataResponsePojos.get(position_actual).getImage_name());

                } else {

                    Toasty.error(FullScreenViewActivity.this,"Please give storage permission to download image.",Toast.LENGTH_SHORT).show();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'switch' lines to check for other
            // permissions this app might request
        }
    }
}
