package com.snaptech.emissionew.AfterLoginModules;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.joanzapata.pdfview.PDFView;
import com.joanzapata.pdfview.listener.OnPageChangeListener;
import com.snaptech.emissionew.AfterLoginModules.Pojos.DocumentsPojo;
import com.snaptech.emissionew.R;
import com.snaptech.emissionew.Utils.Constants;
import com.snaptech.emissionew.Utils.FileDownloader;
import com.snaptech.emissionew.Utils.Singleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

import es.dmoral.toasty.Toasty;

import static com.snaptech.emissionew.Utils.Constants.BASE_URL;

//webview for opening pdfs
public class PDFWebViewer extends AppCompatActivity implements OnPageChangeListener {

    private ProgressDialog pDialog;
    PDFView pdfView;

    TextView pdfPageCount;
    private TextView tv_title;
    private Toolbar toolbar;
    private ImageView img_email_me;
    private Intent intent=null;
    private String doc_id="";
    private Singleton singleton;
    private SharedPreferences prefs;
    private String api_token;
    private String url;
    private String url_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_pdfweb_viewer);
        pdfPageCount = (TextView)findViewById(R.id.pdfPageCount);

        pdfView = (PDFView) findViewById(R.id.pdfview);
        tv_title=(TextView)findViewById(R.id.tv_toolbar_title_pdfwv);
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        img_email_me=(ImageView)toolbar.findViewById(R.id.img_email_me);
        prefs= PDFWebViewer.this.getSharedPreferences(Constants.LOGIN_PREFS_NAME, MODE_PRIVATE);
        api_token = prefs.getString(Constants.ACCESS_TOKEN_PREFS_KEY, null);
        singleton=Singleton.getInstance();
        intent=getIntent();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        img_email_me.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                //call Email me api


                new AlertDialog.Builder(PDFWebViewer.this)
                        .setTitle("Email Document")
                        .setMessage("Are you sure you want to send the document to your registered email address ?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue with sending
                                callDocEmailmeApi(PDFWebViewer.this);
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();


            }
        });

        if(intent!=null){

            if(intent.getStringExtra("id")!=null){

                doc_id=intent.getStringExtra("id");

            }
            if(intent.getStringExtra("url")!=null){

                url=intent.getStringExtra("url");

            }
            if(intent.getStringExtra("urltitle")!=null){

                url_title=intent.getStringExtra("urltitle");

            }
        }
        String title = "";


//        WebView webView = (WebView) findViewById(R.id.webview);

        pDialog= new ProgressDialog(this, R.style.MyTheme);
        pDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        pDialog.setCancelable(false);
        //pDialog.show();



        String url = "";

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            url = extras.getString("url");
            title = extras.getString("urltitle");
        }

        tv_title.setText(title);
        System.out.println("Actual URL is "+url);
        // webView = (WebView) findViewById(R.id.webview_compontent);

        String afterDecode = null;
        if(url!=null) {
            try {
                afterDecode = URLDecoder.decode(url, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        view(afterDecode,title);

//        webView.getSettings().setJavaScriptEnabled(true);
//        webView.getSettings().setLoadWithOverviewMode(true);
//        webView.getSettings().setUseWideViewPort(true);
//        webView.getSettings().setBuiltInZoomControls(true);
//        webView.getSettings().setSupportZoom(true);
//        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
//        webView.getSettings().setAllowFileAccess(true);
//        webView.getSettings().setDomStorageEnabled(true);
//        webView.setWebViewClient(new WebViewClient(){
//
//            @Override
//            public boolean shouldOverrideUrlLoading(WebView view, String url) {
//                pDialog.show();
//                view.loadUrl(url);
//
//                return true;
//            }
//            @Override
//            public void onPageFinished(WebView view, final String url) {
//                pDialog.dismiss();
//            }
//
//            @Override
//            public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
//                final AlertDialog.Builder builder = new AlertDialog.Builder(PDFWebViewer.this);
//                builder.setMessage("Do you want to continue?");
//                builder.setPositiveButton("continue", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        handler.proceed();
//                    }
//                });
//                builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        handler.cancel();
//                    }
//                });
//                final AlertDialog dialog = builder.create();
//                dialog.show();// Ignore SSL certificate errors
//            }
//
//        });
//
//
//
//        webView.loadUrl("http://drive.google.com/viewerng/viewer?embedded=true&url="+url);
//
//        //  browser.loadUrl("");
    }


    private void callDocEmailmeApi(final Context context)
    {
        final ProgressDialog pDialog = new ProgressDialog(context,R.style.MyTheme);
        pDialog.setCancelable(false);
        pDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        pDialog.show();


        Map<String, String> params= new HashMap<String, String>();
        //params.put("first_name", "Vikas");
        params.put("document_id", doc_id);
        params.put("document_name",url_title);
        params.put("document_url",url);

//

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                BASE_URL+"api/send/document", new JSONObject(params),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Response", response.toString());


                        //Toast.makeText(getActivity(),"Login Successful",Toast.LENGTH_SHORT).show();
                        pDialog.dismiss();
                    }
                }, new Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error", "Error: " + error.getMessage());
                pDialog.dismiss();
                String message="";
                if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }

                if(!message.equalsIgnoreCase(""))
                    Toasty.error(PDFWebViewer.this,message,Toast.LENGTH_SHORT).show();


            }
        }) {

            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                System.out.println("Response code from parse Network response is "+response.statusCode+" and data is "+response.toString()+" and data is "+response.data.toString());
                try {
                    String jsonString = new String(response.data, "UTF-8");
                    System.out.println("Response from parse Network response is "+jsonString);

                    Gson gson = new Gson();
                    // Reader reader = new InputStreamReader();
                    final DocumentsPojo documentsPojo = gson.fromJson(jsonString,DocumentsPojo.class); // (reader, Login_Response_Pojo.class);

                    // System.out.println("Response pojo is "+documentsPojo.getDocumentsResponsePojoArrayList().get(0).getUrl());
                    if(response.statusCode==200){
                        PDFWebViewer.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                Toasty.success(PDFWebViewer.this,"The document has been sent to your registered email address",Toast.LENGTH_SHORT).show();
                            }
                        });


                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.parseNetworkResponse(response);
            }


            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {

                NetworkResponse networkResponse=volleyError.networkResponse;
                if(networkResponse!=null){

                    String jsonString="";
                    try {
                        jsonString = new String(networkResponse.data, "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                    System.out.println("Network response is "+networkResponse.statusCode+jsonString);
                    try {
                        JSONObject jsonObject=new JSONObject(jsonString);
                        JSONArray jsonArray=jsonObject.getJSONArray("error");
                        final String errorString=jsonArray.getString(0);
                        PDFWebViewer.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toasty.error(PDFWebViewer.this,errorString,Toast.LENGTH_SHORT).show();
                            }
                        });


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
                return super.parseNetworkError(volleyError);
            }

            /**
             * Passing some request headers
             * */

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Authorization",api_token);
                return headers;
            }



        };


//// Adding request to request queue
        singleton.addToRequestQueue(jsonObjReq, "Document sending api calling");

    }

    @Override
    public void onPageChanged(int page, int pageCount) {
        pdfPageCount.setText(page+"/"+pageCount);
    }

    private class DownloadFile extends AsyncTask<String, Void, Void> {

        String fileName;

        @Override
        protected Void doInBackground(String... strings) {

            //  Toast.makeText(MainActivity.this,"File download started", Toast.LENGTH_SHORT).show();

            String fileUrl = strings[0];   // -> http://maven.apache.org/maven-1.x/maven.pdf
            fileName = strings[1];  // -> maven.pdf
            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
            File folder = new File(extStorageDirectory, "Sircle");
            folder.mkdir();

            File pdfFile = new File(folder, fileName);

            try{
                pdfFile.createNewFile();
                FileDownloader.downloadFile(fileUrl, pdfFile);
            }catch (IOException e){

                PDFWebViewer.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        Toasty.error(PDFWebViewer.this, "Please give storage permission to view pdf", Toast.LENGTH_SHORT).show();
                    }
                });

                e.printStackTrace();
            }catch (Exception e){
                //Toast.makeText(PDFViewer.this, Constants.NO_NET_CONNECTIVITY_MESSAGE, Toast.LENGTH_SHORT).show();
            }
            try {
                Class.forName("android.os.AsyncTask");
            } catch (ClassNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog.show();

        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
//                        if (aVoid == null) {
//
//            }
//            else
//            {
            pDialog.dismiss();

            File pdfFile = new File(Environment.getExternalStorageDirectory() + "/Sircle/" + fileName);  // -> filename
            if(pdfFile.exists() && pdfFile.length() != 0) {
                // pdfView.fromFile(pdfFile).defaultPage(1).load();
                pdfView.fromFile(pdfFile).defaultPage(1).onPageChange(PDFWebViewer.this).load();
                pdfPageCount.setText(pdfView.getCurrentPage() + "/" + pdfView.getPageCount());
            }

            //   }
            // return;

        }
    }
    public void view(String url,String filename)
    {
        if(url!=null) {
            Uri uri = Uri.parse(url);
            //   String fileName = uri.getLastPathSegment();

            String fileName = filename;

            File pdfFile = new File(Environment.getExternalStorageDirectory() + "/Sircle/" + fileName);  // -> filename
            long len = pdfFile.length();
            if (pdfFile.exists() && pdfFile.canRead() && pdfFile.length() != 0) {
                pdfView.fromFile(pdfFile).defaultPage(1).onPageChange(this).load();
                pdfPageCount.setText(pdfView.getCurrentPage() + "/" + pdfView.getPageCount());
            } else {
                download(url, fileName);
            }
        }
    }
    public void download(String url,String fileName)
    {
        Uri uri = Uri.parse(url);
        //String fileName = uri.getLastPathSegment();
        new DownloadFile().execute(url, fileName);
    }

}
