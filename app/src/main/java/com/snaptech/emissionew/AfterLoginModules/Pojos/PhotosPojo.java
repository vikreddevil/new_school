package com.snaptech.emissionew.AfterLoginModules.Pojos;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by vikas on 01/11/16.
 */

public class PhotosPojo {

    @SerializedName("data")
    private ArrayList<PhotosDataResponsePojo> photosDataResponsePojoArrayList;

    public ArrayList<PhotosDataResponsePojo> getPhotosDataResponsePojoArrayList() {
        return photosDataResponsePojoArrayList;
    }

    public void setPhotosDataResponsePojoArrayList(ArrayList<PhotosDataResponsePojo> photosDataResponsePojoArrayList) {
        this.photosDataResponsePojoArrayList = photosDataResponsePojoArrayList;
    }
}
