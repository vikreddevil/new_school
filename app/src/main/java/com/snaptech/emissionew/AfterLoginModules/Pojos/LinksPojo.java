package com.snaptech.emissionew.AfterLoginModules.Pojos;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vikas on 30/10/16.
 */

public class LinksPojo {

    @SerializedName("data")
   private ArrayList<LinksDataResponsePojo> linksDataResponsePojoArrayList;

    public List<LinksDataResponsePojo> getLinksDataResponsePojo() {
        return linksDataResponsePojoArrayList;
    }

    public void setLinksDataResponsePojo(ArrayList<LinksDataResponsePojo> linksDataResponsePojoArrayList) {
        this.linksDataResponsePojoArrayList = linksDataResponsePojoArrayList;
    }
}
