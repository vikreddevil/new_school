package com.snaptech.emissionew.AfterLoginModules.Pojos;

/**
 * Created by vikas on 23/03/17.
 */

public class InstituteInfoDataResponsePojo {



    //{"data":[{"instagram_link":null,"twitter_link":null,"state":null,"fb_link":null,"email_1":null,"email_2":null,"country":null,"city":null,"youtube_link":null,"address":null,"pin":null,"phone_no_1":null,"phone_no_2":null,"name":null}]}

    private String name;
    private String phone_no_1;
    private String phone_no_2;
    private String email_1;
    private String email_2;
    private String city;
    private String state;
    private String country;
    private long pin;
    private String address;
    private String fb_link;
    private String twitter_link;
    private String instagram_link;
    private String youtube_link;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone_no_1() {
        return phone_no_1;
    }

    public void setPhone_no_1(String phone_no_1) {
        this.phone_no_1 = phone_no_1;
    }

    public String getPhone_no_2() {
        return phone_no_2;
    }

    public void setPhone_no_2(String phone_no_2) {
        this.phone_no_2 = phone_no_2;
    }

    public String getEmail_1() {
        return email_1;
    }

    public void setEmail_1(String email_1) {
        this.email_1 = email_1;
    }

    public String getEmail_2() {
        return email_2;
    }

    public void setEmail_2(String email_2) {
        this.email_2 = email_2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public long getPin() {
        return pin;
    }

    public void setPin(long pin) {
        this.pin = pin;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getFb_link() {
        return fb_link;
    }

    public void setFb_link(String fb_link) {
        this.fb_link = fb_link;
    }

    public String getTwitter_link() {
        return twitter_link;
    }

    public void setTwitter_link(String twitter_link) {
        this.twitter_link = twitter_link;
    }

    public String getInstagram_link() {
        return instagram_link;
    }

    public void setInstagram_link(String instagram_link) {
        this.instagram_link = instagram_link;
    }

    public String getYoutube_link() {
        return youtube_link;
    }

    public void setYoutube_link(String youtube_link) {
        this.youtube_link = youtube_link;
    }
}
