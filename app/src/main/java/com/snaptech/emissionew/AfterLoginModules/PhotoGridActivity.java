package com.snaptech.emissionew.AfterLoginModules;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import com.etiennelawlor.imagegallery.library.activities.FullScreenImageGalleryActivity;
import com.etiennelawlor.imagegallery.library.adapters.FullScreenImageGalleryAdapter;
import com.etiennelawlor.imagegallery.library.adapters.ImageGalleryAdapter;
import com.google.gson.Gson;
import com.snaptech.emissionew.AfterLoginModules.Adapters.ImageAdapter;
import com.snaptech.emissionew.AfterLoginModules.Pojos.PhotoGridDataResponsePojo;
import com.snaptech.emissionew.AfterLoginModules.Pojos.PhotoGridPojo;

import com.snaptech.emissionew.R;
import com.snaptech.emissionew.Utils.Constants;
import com.snaptech.emissionew.Utils.Singleton;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import es.dmoral.toasty.Toasty;

import static com.snaptech.emissionew.Utils.Constants.BASE_URL;


//list of photos inside an album

public class PhotoGridActivity extends AppCompatActivity implements ImageGalleryAdapter.ImageThumbnailLoader,FullScreenImageGalleryAdapter.FullScreenImageLoader{

    private Toolbar toolbar;
    private SharedPreferences prefs;
    private String api_token;
    private SwipeRefreshLayout swipeRefreshLayout;
    private Singleton singleton;
    private ImageAdapter imageAdapter;
    private String id="1";
    private ArrayList<PhotoGridDataResponsePojo> photoGridDataResponsePojoArrayList;
    private GridView gridview;// = (GridView) findViewById(R.id.gridview);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_grid);
        getRef();

        Intent intent=getIntent();

       // System.out.println("Intent is "+intent+" intent.getInt is "+intent.getIntExtra("id",123));
        if(intent!=null){
                id=intent.getIntExtra("id",1)+"";
         //       System.out.println("Id got is "+id);
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        FullScreenImageGalleryActivity.setFullScreenImageLoader(this);


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
//                Toast.makeText(PhotoGridActivity.this, "" + position,
//                        Toast.LENGTH_SHORT).show();

            }
        });
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                callAlbumPhotosApi();
            }
        });

        callAlbumPhotosApi();
    }
    private void getRef(){

        swipeRefreshLayout=(SwipeRefreshLayout)findViewById(R.id.swiperefresh_photo_grid);
        gridview = (GridView) findViewById(R.id.gridview);
        photoGridDataResponsePojoArrayList=new ArrayList<>();
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        singleton=Singleton.getInstance();
        imageAdapter=new ImageAdapter(this,photoGridDataResponsePojoArrayList);
        gridview.setAdapter(imageAdapter);
        prefs= PhotoGridActivity.this.getSharedPreferences(Constants.LOGIN_PREFS_NAME, MODE_PRIVATE);
        api_token = prefs.getString(Constants.ACCESS_TOKEN_PREFS_KEY, null);
    }

    @Override
    public void loadFullScreenImage(ImageView iv, String imageUrl, int width, LinearLayout bglinearLayout) {

    }

    @Override
    public void loadImageThumbnail(ImageView iv, String imageUrl, int dimension) {

    }
    private void callAlbumPhotosApi()
    {
        swipeRefreshLayout.setRefreshing(true);
        Map<String, String> params= new HashMap<String, String>();

        System.out.println("Album Id is "+id);
        params.put("album_id", id);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                BASE_URL+"api/album_images", new JSONObject(params),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        if(response!=null)
                        Log.d("Response", response.toString());


                        //Toast.makeText(PhotoGridActivity.this,"Login Successful",Toast.LENGTH_SHORT).show();
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }, new Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
                //VolleyLog.d("Error", "Error: " + error.getMessage());

                if(swipeRefreshLayout.isRefreshing()){
                    swipeRefreshLayout.setRefreshing(false);
                }
                String message="";
                if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }

                if(!message.equalsIgnoreCase(""))
                    Toasty.error(PhotoGridActivity.this, message, Toast.LENGTH_SHORT).show();

            }
        }) {

            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                if(response!=null) {
                    System.out.println("Response code from parse Network response is " + response.statusCode + " and data is " + response.toString() + " and data is " + response.data.toString());
                    try {
                        String jsonString = new String(response.data, "UTF-8");
                        System.out.println("Response from parse Network response is " + jsonString);

                        Gson gson = new Gson();
                        final PhotoGridPojo photoGridPojo = gson.fromJson(jsonString, PhotoGridPojo.class); // (reader, Login_Response_Pojo.class);

                        // System.out.println("Response pojo is "+videosPojo.getVideosResponsePojoArrayList().get(0).getUrl());
                        if (response.statusCode == 200) {

                            //Toast.makeText(PhotoGridActivity.this,"Login Successful",Toast.LENGTH_SHORT).show();
                            photoGridDataResponsePojoArrayList.clear();
                            Constants.photoGridDataResponsePojos.clear();
                            for (int i = 0; i < photoGridPojo.getPhotoGridDataResponsePojoArrayList().size(); i++) {
                                photoGridDataResponsePojoArrayList.add(i, photoGridPojo.getPhotoGridDataResponsePojoArrayList().get(i));
                                Constants.photoGridDataResponsePojos.add(i, photoGridPojo.getPhotoGridDataResponsePojoArrayList().get(i));
                            }

                            PhotoGridActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    imageAdapter.notifyDataSetChanged();
                                    if (swipeRefreshLayout.isRefreshing()) {
                                        swipeRefreshLayout.setRefreshing(false);
                                    }
                                }
                            });

                        }
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
                return super.parseNetworkResponse(response);
            }


            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {

                NetworkResponse networkResponse=volleyError.networkResponse;
                if(networkResponse!=null){

                    String jsonString="";
                    try {
                        jsonString = new String(networkResponse.data, "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                    System.out.println("Network response is "+networkResponse.statusCode+jsonString);
                    try {
                        JSONObject jsonObject=new JSONObject(jsonString);
                        JSONArray jsonArray=jsonObject.getJSONArray("error");
                        final String errorString=jsonArray.getString(0);
                        PhotoGridActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toasty.error(PhotoGridActivity.this,errorString,Toast.LENGTH_SHORT).show();
                            }
                        });


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
                return super.parseNetworkError(volleyError);
            }

            /**
             * Passing some request headers
             * */

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Authorization",api_token);
                headers.put("Client-Id","1");
                return headers;
            }
        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        singleton.addToRequestQueue(jsonObjReq, "Photos Grid images api calling");

    }
}
