package com.snaptech.emissionew.AfterLoginModules;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;

import com.snaptech.emissionew.AfterLoginModules.Pojos.AddMessageRequestPojo;
import com.snaptech.emissionew.R;
import com.snaptech.emissionew.Utils.Constants;
import com.snaptech.emissionew.Utils.Singleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import es.dmoral.toasty.Toasty;

import static com.snaptech.emissionew.Utils.Constants.BASE_URL;

public class AddNotificationActivity extends AppCompatActivity {

    private EditText et_title;
    private Singleton singleton;
    private EditText et_description;

    private EditText et_groups;
    private Button btn_add_url;
    private LinearLayout ll_add_links;
    private SwipeRefreshLayout swipeRefreshLayout;
    private SharedPreferences prefs;
    private String api_token;
    private ArrayList<String> group_id_array=new ArrayList<>();
    private AddMessageRequestPojo addMessageRequestPojo;
    private String title="",group="",description="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_notification);

        getRef();

        et_groups.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;


                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (et_groups.getRight() - et_groups.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here

                        et_groups.setText("");
                        et_groups.requestFocus();
                        return true;
                    }
                }
                return false;
            }
        });

        et_description.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;


                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (et_description.getRight() - et_description.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here

                        et_description.setText("");
                        et_description.requestFocus();
                        return true;
                    }
                }
                return false;
            }
        });
        et_title.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;


                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (et_title.getRight() - et_title.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here

                        et_title.setText("");
                        et_title.requestFocus();
                        return true;
                    }
                }
                return false;
            }
        });

        btn_add_url.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                title=et_title.getText().toString();
                description=et_description.getText().toString();
                et_title.setText("");
                et_description.setText("");
                et_groups.setText("");
                callAddMessageApi(AddNotificationActivity.this);

            }
        });

        et_groups.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                Intent intent=new Intent(AddNotificationActivity.this,GroupsPopupActivity.class);
                startActivityForResult(intent,1);

            }
        });
    }
    private void getRef(){

        addMessageRequestPojo=new AddMessageRequestPojo();
        swipeRefreshLayout=(SwipeRefreshLayout)findViewById(R.id.swiperefresh_links);
        ll_add_links=(LinearLayout)findViewById(R.id.ll_add_links);
        btn_add_url=(Button)findViewById(R.id.btn_add_url_links);
        et_title=(EditText)findViewById(R.id.et_title_add_links);
        et_description=(EditText)findViewById(R.id.et_description_add_links);
        et_groups=(EditText)findViewById(R.id.et_groups_add_links);
        singleton=Singleton.getInstance();
        prefs= AddNotificationActivity.this.getSharedPreferences(Constants.LOGIN_PREFS_NAME, MODE_PRIVATE);
        // tour_id=prefs.getString(TOUR_ID,null);
        api_token = prefs.getString(Constants.ACCESS_TOKEN_PREFS_KEY, null);
        swipeRefreshLayout.setEnabled(false);
    }
    private void callAddMessageApi(final Context context)
    {
        swipeRefreshLayout.setRefreshing(true);

        Map<String, String> params= new HashMap<String, String>();



        addMessageRequestPojo.setGroup_add_id(group_id_array);
        addMessageRequestPojo.setDesc(description);
        addMessageRequestPojo.setTitle(title);

        Gson gson=new Gson();
        JSONObject jsonObject=null;
        try {
            jsonObject=new JSONObject(gson.toJson(addMessageRequestPojo));
        } catch (JSONException e) {
            e.printStackTrace();
        }


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                BASE_URL+"api/send/message", jsonObject,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        if(response!=null)
                            Log.d("add message ", response.toString());


                        //Toast.makeText(mActivity,"Login Successful",Toast.LENGTH_SHORT).show();
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }, new Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
                if(swipeRefreshLayout.isRefreshing()){
                    swipeRefreshLayout.setRefreshing(false);
                }

                //VolleyLog.d("Error", "Error: " + error.getMessage());
                String message="";
                if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }


                if (!message.equalsIgnoreCase(""))
                    Toasty.error(context, message, Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {

                if(response!=null) {
                    System.out.println("Response code from parse Network response is " + response.statusCode + " and data is " + response.toString() + " and data is " + response.data.toString());
                    try {
                        String jsonString = new String(response.data, "UTF-8");
                        System.out.println("Response from parse Network response is " + jsonString);

                        //Gson gson = new Gson();
                        //final LinksPojo linksPojo = gson.fromJson(jsonString, LinksPojo.class);

                        if (response.statusCode == 200) {

                            //Toast.makeText(mActivity,"Login Successful",Toast.LENGTH_SHORT).show();







                            ((AddNotificationActivity) context).runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    Toast.makeText(AddNotificationActivity.this,"Message added Successfully",Toast.LENGTH_SHORT).show();

                                }
                            });



                        }
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
                return super.parseNetworkResponse(response);
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {

                NetworkResponse networkResponse=volleyError.networkResponse;
                if(networkResponse!=null){

                    String jsonString="";
                    try {
                        jsonString = new String(networkResponse.data, "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                    System.out.println("Network response is "+networkResponse.statusCode+jsonString);
                    try {
                        JSONObject jsonObject=new JSONObject(jsonString);
                        JSONArray jsonArray=jsonObject.getJSONArray("error");
                        final String errorString=jsonArray.getString(0);
                        ((HomeScreenActivity)context).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toasty.error(context,errorString,Toast.LENGTH_SHORT).show();
                            }
                        });


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
                return super.parseNetworkError(volleyError);
            }

            /**
             * Passing some request headers
             * */

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Authorization",api_token);
                headers.put("Client-Id","1");
                return headers;
            }

        };

        singleton.addToRequestQueue(jsonObjReq, "Add Message api calling");

    }
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if(resultCode == RESULT_OK){
                Bundle bundle=data.getExtras();
                group_id_array=bundle.getStringArrayList("group_ids");
               // group_name_array=bundle.getStringArrayList("group_names");

                if(group_id_array!=null) {
                    String groups = group_id_array.size() + " groups selected";
//                for (int i=0;i<group_name_array.size();i++){
//
//                    if(i!=group_name_array.size()-1)
//                    groups=groups+group_name_array.get(i)+" ,";
//                    else
//                        groups=groups+group_name_array.get(i);
//                }
                    et_groups.setText(groups);
                }
            }
        }
    }
}
