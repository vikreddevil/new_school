package com.snaptech.emissionew.AfterLoginModules.Pojos;

import com.orm.SugarRecord;

/**
 * Created by vikas on 28/02/17.
 */

public class MobileSideBarDataResponsePojo extends SugarRecord implements Comparable{


    public MobileSideBarDataResponsePojo(){


    }

    private String display_name;
    private String action;
    private int sort;


    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }


    @Override
    public String toString() {
        return "MobileSideBarDataResponsePojo{" +
                "display_name='" + display_name + '\'' +
                ", action='" + action + '\'' +
                ", sort=" + sort +
                '}';
    }

    @Override
    public int compareTo(Object mobileSideBarDataResponsePojo) {
        int compareQuantity = ((MobileSideBarDataResponsePojo) mobileSideBarDataResponsePojo).getSort();

        //ascending order
        return this.sort - compareQuantity;
    }
}
