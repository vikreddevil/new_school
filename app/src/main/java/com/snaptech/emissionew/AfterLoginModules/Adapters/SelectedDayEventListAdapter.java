package com.snaptech.emissionew.AfterLoginModules.Adapters;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.snaptech.emissionew.AfterLoginModules.Pojos.CalendarMonthwiseDataResponsePojo;
import com.snaptech.emissionew.R;

import java.util.ArrayList;

/**
 * Created by vikas on 29/03/17.
 */

public class SelectedDayEventListAdapter extends RecyclerView.Adapter<SelectedDayEventListAdapter.CustomViewHolder>{

    public ArrayList<CalendarMonthwiseDataResponsePojo> calendarMonthwiseDataResponsePojoArrayList=new ArrayList<>();

    public SelectedDayEventListAdapter(ArrayList<CalendarMonthwiseDataResponsePojo> calendarMonthwiseDataResponsePojoArrayList){

        this.calendarMonthwiseDataResponsePojoArrayList=calendarMonthwiseDataResponsePojoArrayList;
    }
    @Override
    public SelectedDayEventListAdapter.CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.calendar_list_row, parent,false);
        SelectedDayEventListAdapter.CustomViewHolder viewHolder = new SelectedDayEventListAdapter.CustomViewHolder(view);
        viewHolder.cardView.setPreventCornerOverlap(false);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(SelectedDayEventListAdapter.CustomViewHolder holder, int position) {


        holder.tv_title.setText(calendarMonthwiseDataResponsePojoArrayList.get(position).getTitle());

        holder.tv_event_type.setText(calendarMonthwiseDataResponsePojoArrayList.get(position).getEvent_type_name());
        holder.tv_start_date.setText(calendarMonthwiseDataResponsePojoArrayList.get(position).getStart_date());


        if(calendarMonthwiseDataResponsePojoArrayList.get(position).getEvent_code()==null) {
            holder.tv_end_time.setText(calendarMonthwiseDataResponsePojoArrayList.get(position).getEnd_time());
            holder.tv_start_time.setText(calendarMonthwiseDataResponsePojoArrayList.get(position).getStart_time());
        }


    }

    @Override
    public int getItemCount() {
        return calendarMonthwiseDataResponsePojoArrayList.size();
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {

        //        protected TextView tv_Title;
        private CardView cardView;
        private TextView tv_title;
        private TextView tv_start_date;
        private TextView tv_event_type;
        private TextView tv_end_time;
        private TextView tv_start_time;

//        protected TextView tv_description;
//        protected TextView tv_date;
//        private ImageButton ib_icon;

        public CustomViewHolder(View view) {
            super(view);

//            this.tv_Title=(TextView)view.findViewById(R.id.tv_title_documents);
            this.cardView=(CardView)view.findViewById(R.id.cardview_notification_row);
            this.tv_start_date=(TextView)view.findViewById(R.id.tv_start_date);
            this.tv_start_time=(TextView)view.findViewById(R.id.tv_start_time);
            this.tv_event_type=(TextView)view.findViewById(R.id.tv_event_type);
            this.tv_title=(TextView)view.findViewById(R.id.tv_title_event);
            this.tv_end_time=(TextView)view.findViewById(R.id.tv_event_end_time);
//            this.tv_description=(TextView)view.findViewById(R.id.tv_description_documents);
//            this.tv_date=(TextView)view.findViewById(R.id.tv_date_documents);
//            this.ib_icon=(ImageButton)view.findViewById(R.id.ib_documents_icon);

        }
    }
}
