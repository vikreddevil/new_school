package com.snaptech.emissionew.AfterLoginModules.Pojos;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by vikas on 22/03/17.
 */

public class UserEventResponsePojo {

    @SerializedName("data")
    private ArrayList<UserEventDataResponsePojo> userEventDataResponsePojoArrayList;

    public ArrayList<UserEventDataResponsePojo> getUserEventDataResponsePojoArrayList() {
        return userEventDataResponsePojoArrayList;
    }

    public void setUserEventDataResponsePojoArrayList(ArrayList<UserEventDataResponsePojo> userEventDataResponsePojoArrayList) {
        this.userEventDataResponsePojoArrayList = userEventDataResponsePojoArrayList;
    }
}
