package com.snaptech.emissionew.Utils;

import android.os.Environment;

import com.snaptech.emissionew.AfterLoginModules.Pojos.PhotoGridDataResponsePojo;

import java.util.ArrayList;

/**
 * Created by vikas on 14/02/17.
 */

public class Constants {

    public static String album_name="School";
    public static ArrayList<PhotoGridDataResponsePojo> photoGridDataResponsePojos=new ArrayList<>();
    public static final String USER_ID_PREFS_KEY ="user_id" ;
    public static final String PROFILE_IMAGE_URL = "image_url";
    public static boolean flag_close_app=true;
    public static final String ACCESS_TOKEN_PREFS_KEY = "accessToken";
    public static final String LOGIN_PREFS_NAME = "LOGIN_PREFS";
    public static final String BASE_URL="http://52.33.148.194:4000/";
    public static final String PRIMARY_COLOR_PREFS_KEY="primary_color";
    public static final String SECONDARY_COLOR_PREFS_KEY="secondary_color";
    public static final String COLOR_VERSION_PREFS_KEY="color_version";
    public static final String SIDE_BAR_VERSION_PREFS_KEY="side_bar_version";
    public static final String FIRST_NAME_PREFS_KEY="first_name";
    public static final String LAST_NAME_PREFS_KEY="last_name";
    public static final String MIDDLE_NAME_PREFS_KEY="middle_name";
    public static final String DOB_PREFS_KEY="dob";
    public static final String PROFILE_PREFS_KEY="profile";
    public static final String GENDER_PREFS_KEY="gender";
    public static final String EMAIL_ID_PREFS_KEY="email_id";
    public static final String MOBILE_PREFS_KEY="mobile";
    public static final String ROLE_ID="role_id";
    public static final String PHOTO_SAVE_GALLERY_DIR_IMAGE_PATH = Environment.getExternalStorageDirectory() + "/School/images/";
    public static boolean flag_side_bar_updated=false;
}
