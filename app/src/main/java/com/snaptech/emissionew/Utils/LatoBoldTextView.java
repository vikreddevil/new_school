package com.snaptech.emissionew.Utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import java.util.jar.Attributes;

/**
 * Created by vikas on 22/03/17.
 */

public class LatoBoldTextView extends TextView {


    public LatoBoldTextView(Context context, AttributeSet attributeSet) {
        super(context,attributeSet);
        this.setTypeface(Typeface.createFromAsset(context.getAssets(),"fonts/Lato-Bold.ttf"));
    }
}
