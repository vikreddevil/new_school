package com.snaptech.emissionew.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.RadioButton;

/**
 * Created by vikas on 26/10/16.
 */

public class LatoRegularRadioButton extends RadioButton {

//    private SharedPreferences preferences;
//    private String color="#FFF";
    @Override
    public boolean isInEditMode() {
        return true;
    }

    public LatoRegularRadioButton(Context context, AttributeSet attrs)
    {


        super(context,attrs);
//        preferences=context.getSharedPreferences(Constants.LOGIN_PREFS_NAME,Context.MODE_PRIVATE);
//        color=preferences.getString(Constants.PRIMARY_COLOR_PREFS_KEY,"#FFF");

        this.setTypeface(Typeface.createFromAsset(context.getAssets(),"fonts/Lato-Regular.ttf"));

//        if(!color.trim().equals(""))
//        this.setBackgroundColor(Color.parseColor(color));

    }
}
