package com.snaptech.emissionew.Utils;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Created by vikas on 15/03/17.
 */

public class LatoBoldButtonNoBackgroundColor extends Button {

    @Override
    public boolean isInEditMode() {
        return true;
    }

    public LatoBoldButtonNoBackgroundColor(Context context, AttributeSet attrs)
    {
        super(context,attrs);
        this.setTypeface(Typeface.createFromAsset(context.getAssets(),"fonts/Lato-Bold.ttf"));

    }
}
