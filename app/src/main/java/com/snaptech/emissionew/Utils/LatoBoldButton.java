package com.snaptech.emissionew.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Created by vikas on 17/10/16.
 */

public class LatoBoldButton extends Button {

    private SharedPreferences preferences;
    private String color="#FFFFFF";
    @Override
    public boolean isInEditMode() {
        return true;
    }

    public LatoBoldButton(Context context, AttributeSet attrs)
    {
        super(context,attrs);
        this.setTypeface(Typeface.createFromAsset(context.getAssets(),"fonts/Lato-Bold.ttf"));
        preferences=context.getSharedPreferences(Constants.LOGIN_PREFS_NAME,Context.MODE_PRIVATE);
        color=preferences.getString(Constants.PRIMARY_COLOR_PREFS_KEY,"#FFFFFF");
        if(!color.trim().equals(""))
            this.setBackgroundColor(Color.parseColor(color));
    }
}
